﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows;
using NLog;
using EventLog = Dmc.Logging.EventLog;
using NatoliBlackBox.Windows;
using NatoliBlackBox.IoC;
using NatoliBlackBox.Services;

namespace NatoliBlackBox
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private const string COMPANY_NAME = "Natoli";
        private const string APPLICATION_NAME = "NatoliDAQAnalytical";
        private readonly string logPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), COMPANY_NAME, APPLICATION_NAME);


        public App()
        {
            AppDomain.CurrentDomain.UnhandledException += this.CurrentDomain_UnhandledException;
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            TimeSpan fadeoutDurration = new TimeSpan();

            SplashScreen splashScreen = new SplashScreen("/Resources/Icons/Natoli-Splash-Screen.jpg");
            splashScreen.Show(true);


            base.OnStartup(e);
            this.InitializeComponent();

            //Need to specify App Version to get file path to resolve as expected.            
            EventLog.Initialize(this.logPath, "0.0", LogLevel.Debug);
            EventLog.LogDebug("Application Started.");

            //IoC Container builds
            IoCContainer.Build();
            // Set Starting Window
            this.MainWindow = IoCContainer.Resolve<MainWindow>();
            splashScreen.Close(fadeoutDurration);
            this.MainWindow.ShowDialog();

        }

        // Log any unhandled exceptions to the log directory in a CrashDump file
        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            string message;

            if (e.ExceptionObject is Exception)
            {
                try
                {
                    string directory;

                    try
                    {
                        directory = this.logPath ?? ".";
                    }
                    catch
                    {
                        directory = ".";
                    }

                    File.WriteAllText
                    (
                        Path.Combine(directory, "CrashDump - " + DateTime.Now.ToString("yyyyMMdd - HHmmss") + ".txt"),
                        ((Exception)e.ExceptionObject).ToString()
                    );
                }
                finally
                {
                    message = ((Exception)e.ExceptionObject).ToString();
                }
            }
            else
            {
                message = "An unknown error occurred.";
            }

            Debug.Assert(false, message);
            Environment.Exit(-1);
        }
    }
}
