﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Dmc.Wpf;
using Tablet = NatoliAnalytical.Data.SQL.Entities.Tablet;
using NatoliAnalytical.Data.SQL.Entities;
using NatoliAnalytical.Data.SQL;

namespace NatoliBlackBox.Services
{
    public sealed class CalculationService : NotifyPropertyChanged
    {
        //
        // Service for calculated values 
        //
        private readonly NatoliDbContext _dbContext;
        //private readonly TabletService _tabletService;
        //private readonly AverageService _averageService;
        //private readonly StudyService _studyService;
        //private readonly SampleService _sampleService;
        //private readonly RunService _runService;


        #region Data Containers
        private ObservableCollection<Sample> _samples;
        public ObservableCollection<Sample> Samples
        {
            get { return _samples; }
            set { this.SetProperty(ref this._samples, value); }
        }

        //Tablet Data
        private ObservableCollection<Tablet> _tablets;
        public ObservableCollection<Tablet> Tablets
        {
            get { return _tablets; }
            set { this.SetProperty(ref this._tablets, value); }
        }

        //Project Selections
        private ObservableCollection<Project> _projectItems;
        public ObservableCollection<Project> ProjectItems
        {
            get { return _projectItems; }
            set { this.SetProperty(ref this._projectItems, value); }
        }

        private Project _selectedProject;
        public Project SelectedProject
        {
            get { return _selectedProject; }
            set { this.SetProperty(ref this._selectedProject, value); }
        }

        //Study Selections
        private ObservableCollection<Study> _studyItems;
        public ObservableCollection<Study> StudyItems
        {
            get { return _studyItems; }
            set { this.SetProperty(ref this._studyItems, value); }
        }

        private Study _selectedStudy;
        public Study SelectedStudy
        {
            get { return _selectedStudy; }
            set { this.SetProperty(ref this._selectedStudy, value); }
        }

        private StudyAverage _studyAverage;
        public StudyAverage StudyAverage
        {
            get { return _studyAverage; }
            set { this.SetProperty(ref this._studyAverage, value); }
        }
        #endregion

        public CalculationService(NatoliDbContext dbContext)
        {
            this._dbContext = dbContext;
            
            this._samples = new ObservableCollection<Sample>();
            this._tablets = new ObservableCollection<Tablet>();

            this._projectItems = new ObservableCollection<Project>();
            this._selectedProject = new Project();
            this._studyItems = new ObservableCollection<Study>();
            this._selectedStudy = new Study();

            this._studyAverage = new StudyAverage();
        }

        #region Compaction Pressure Calculations

        /// <summary>
        /// Compaction Pressure N/mm^2
        /// </summary>
        /// <param name="tool"></param>
        /// <param name="sample"></param>
        /// <returns></returns>
        public double CompactionPressCalc(Tool upperTool, Sample sample)
        {
            double sa = upperTool.SurfaceArea * upperTool.TipCount;
            double pf = sample.MaxMainCompForce;

            double compactionPressure = SigFig(pf / sa);
            return compactionPressure;
        }

        /// <summary>
        /// Calculates the Ejection Pressure
        /// Requires Belly band measurements
        /// As Such, can only be completed after post compaction data is entered
        /// </summary>
        /// <param name="lowerTool"></param>
        /// <param name="sample"></param>
        /// <returns></returns>
        public double EjectionPressCalc(Tool lowerTool, Tool upperTool, Sample sample, Tablet tablet)
        {
            // Ejection Pressure needs the area of the belly band

            double ejectionPressure = 0;


            if (tablet.Thickness != 0)
            {
                // determine the surface area of the belly band
                double bellybandArea;
                // tool is round
                if (lowerTool.ToolGeoTypeValue == 1)
                {
                    //double circ = (2 * Math.PI * (lowerTool.XAxis / 2));
                    double bellyBand = tablet.Thickness - (lowerTool.ZAxis + upperTool.ZAxis);

                    bellybandArea = bellyBand * lowerTool.Perimeter;

                    ejectionPressure = SigFig(sample.MaxEjectForce / (bellybandArea + lowerTool.SurfaceArea));

                    ejectionPressure *= lowerTool.TipCount;

                    return ejectionPressure;

                }
                // is capsule
                if (lowerTool.ToolGeoTypeValue == 2)
                {
                    // capsule end circumference
                    //double cEc = (2 * Math.PI * (lowerTool.YAxis / 2));
                    //double perimeter = (((lowerTool.XAxis - lowerTool.YAxis) * 2)) + cEc;

                    double bellyBand = tablet.Thickness - (lowerTool.ZAxis + upperTool.ZAxis);
                    double bellyBandArea = lowerTool.Perimeter * bellyBand;

                    ejectionPressure = SigFig(sample.MaxEjectForce / (bellyBandArea + lowerTool.SurfaceArea));

                    ejectionPressure *= lowerTool.TipCount;

                    return ejectionPressure;
                }
                // is oval
                if (lowerTool.ToolGeoTypeValue == 4)
                {
                    // circumference formula
                    // C = 2Pi * SqrRt((MajorAxis Sqrd + MinorAxis sqrd) / 2 )

                    //double axis = ((lowerTool.XAxis * lowerTool.XAxis) + (lowerTool.YAxis * lowerTool.YAxis) / 2);
                    //double circ = (2 * Math.PI) * Math.Sqrt(axis);



                    double bellyBand = tablet.Thickness - (lowerTool.ZAxis + upperTool.ZAxis);

                    bellybandArea = bellyBand * lowerTool.Perimeter;

                    ejectionPressure = SigFig(sample.MaxEjectForce / (bellybandArea + lowerTool.SurfaceArea));

                    ejectionPressure *= lowerTool.TipCount;

                    return ejectionPressure;
                }
                // is square
                if (lowerTool.ToolGeoTypeValue == 8)
                {
                    //double perimeter = (lowerTool.XAxis * 2) + (lowerTool.YAxis * 2);

                    double bellyBand = tablet.Thickness - (lowerTool.ZAxis + upperTool.ZAxis);

                    double bellyBandArea = lowerTool.Perimeter * bellyBand;

                    ejectionPressure = SigFig(sample.MaxEjectForce / (bellyBandArea + lowerTool.SurfaceArea));

                    ejectionPressure *= lowerTool.TipCount;

                    return ejectionPressure;
                }
                // is rectangle
                if (lowerTool.ToolGeoTypeValue == 10)
                {
                    //double perimeter = (lowerTool.XAxis * 2) + (lowerTool.YAxis * 2);
                    double bellyBand = tablet.Thickness - (lowerTool.ZAxis + upperTool.ZAxis);

                    double bellyBandArea = lowerTool.Perimeter * bellyBand;

                    ejectionPressure = SigFig(sample.MaxEjectForce / (bellyBandArea + lowerTool.SurfaceArea));

                    ejectionPressure *= lowerTool.TipCount;

                    return ejectionPressure;
                }

            }

            return ejectionPressure;
        }


        #endregion

        #region Newton/KiloNewton Converts

        public double ConvertNewtontoKiloNewton(double newtons)
        {
            newtons *= 1000;
            return newtons;
        }

        public double ConvertKiloNewtontoNewtons(double kilonewtons)
        {
            kilonewtons /= 1000;
            return kilonewtons;
        }

        #endregion


        #region Chart Scale Adjust
        /// <summary>
        /// Set chart max value to force set point + 10%
        /// </summary>
        /// <param name="maxValue"></param>
        /// <returns></returns>
        public double ChartScaler(double maxValue)
        {
            maxValue = (maxValue + (maxValue * 0.2));
            return maxValue;
        }

        #endregion

        #region SigFigLimiter

        /// <summary>
        /// Limit Sig Figs from output doubles
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public double SigFig(double input)
        {
            input = Math.Round(input, 2, MidpointRounding.AwayFromZero);
            return input;
        }

        public double SigFig3(double input)
        {
            input = Math.Round(input, 3, MidpointRounding.AwayFromZero);
            return input;
        }

        #endregion

        #region Deviation Calculations
        /// <summary>
        /// Calculate deviation from setpoint in %
        /// </summary>
        /// <param name="sample"></param>
        /// <returns></returns>
        //public double DevCalc(Sample sample)
        //{
        //    sample.DevPercent = ((Math.Abs(sample.ForceMax - sample.ForceSP) / sample.ForceSP) * 100);
        //    sample.DevPercent = SigFig(sample.DevPercent);
        //    return sample.DevPercent;
        //}
        #endregion

        #region Tool Calcs
        /// <summary>
        /// calculate dimensions of created tool based on partial data input
        /// </summary>
        /// <param name="tool"></param>
        public void CalculateTool(Tool tool)
        {
            // round Tool
            if (tool.ToolGeoTypeValue == 1)
            {
                // punch tip dia should never be zero here
                if (tool.XAxis != 0)
                {
                    // X and Y are the same
                    tool.YAxis = tool.XAxis;
                    // make easy r for furture calcs
                    double r = tool.XAxis / 2;

                    // Cross Section Not Entered - Calculate
                    if (tool.CrossSectionalArea == 0)
                    {
                        try
                        {
                            // calculate cross sectional area
                            //tool.CrossSectionalArea = Math.Round((.25 * Math.PI) * Math.Pow(tool.PunchTipDiameter, 2), 2, MidpointRounding.AwayFromZero);
                            tool.CrossSectionalArea = Math.Round((Math.PI) * (Math.Pow(r, 2)), 2, MidpointRounding.AwayFromZero);
                        }
                        catch
                        {

                        }
                    }

                    // Volume Not Entered - Calculate
                    if (tool.CupVolume == 0)
                    {
                        try
                        {
                            if (tool.ZAxis != 0)
                            {
                                // calculate Cup volume 
                                // get bigR
                                double R = ((Math.Pow(r, 2) + Math.Pow(tool.ZAxis, 2)) / 2 * (tool.ZAxis));
                                //
                                // V = (PI*h(3r^2 + h^2) / 6)
                                tool.CupVolume = SigFig(((Math.PI * tool.ZAxis) * ((3 * Math.Pow(r, 2)) + Math.Pow(tool.ZAxis, 2))) / 6);

                            }
                        }
                        catch
                        {

                        }
                    }
                    // Surface Area Not Entered - Calculate
                    if (tool.SurfaceArea == 0)
                    {
                        try
                        {
                            if (tool.ZAxis != 0)
                            {
                                // calculate surface area
                                // SA = pi(a^2 + h^2)
                                tool.SurfaceArea = SigFig(Math.PI * ((Math.Pow(r, 2))) + (Math.Pow(tool.ZAxis, 2)));
                            }
                            else
                            {
                                // surface area = cross section area
                                tool.SurfaceArea = tool.CrossSectionalArea;
                            }
                        }
                        catch
                        {

                        }
                    }
                    // Perimeter Not Entered - Calculate
                    if (tool.Perimeter == 0)
                    {
                        tool.Perimeter = SigFig(2 * Math.PI * r);
                    }
                }
            }
            else if (tool.ToolGeoTypeValue == 2) // Capsule type
            {
                // Cross Section Not Entered - Calculate
                if (tool.CrossSectionalArea == 0)
                {
                    // assumes capsule is set on the side
                    try
                    {
                        // get area of rounded endcaps Diameter will be Short Axis
                        double r = tool.YAxis / 2;
                        double endcapCircArea = Math.Round((Math.PI) * (Math.Pow(r, 2)), 2, MidpointRounding.AwayFromZero);

                        // get reamining rectagle area

                        double rectangleArea = (tool.XAxis - tool.YAxis) * tool.YAxis;


                        tool.CrossSectionalArea = SigFig(rectangleArea + endcapCircArea);
                    }
                    catch
                    {

                    }
                }
                // Volume Not Entered - Calculate
                if (tool.CupVolume == 0)
                {
                    try
                    {
                        if (tool.ZAxis != 0)
                        {

                            // Volume of Capsule math is
                            //
                            // V = (Pi * (r^2)) * ((4/3) * r + a )
                            //
                            // where a is length of the cyl segment
                            // where r is radius of capsule ends
                            // so cup vol will be (V / 2)
                            //
                            // we assume the minor axis is the diameter of the end cap circle
                            // and that the major axis is the length of the cyl + (minor axis * 2)
                            // 
                            // so this will assume the capsule is set on its side like:
                            // |___| - upper tool
                            // /___\
                            //|     | - Die
                            // \___/
                            // |   | - lower tool
                            double endcapRadius = tool.YAxis / 2;
                            double cylSegment = tool.XAxis - (tool.YAxis * 2);

                            tool.CupVolume = SigFig(((Math.PI * (Math.Pow(endcapRadius, 2))) * ((4 / 3) * endcapRadius + cylSegment)) / 2);



                        }
                        else
                        {
                            // surface area = cross section
                            tool.SurfaceArea = tool.CrossSectionalArea;
                        }

                    }
                    catch
                    {

                    }
                }
                // Surface Area Not Entered - Calculate
                if (tool.SurfaceArea == 0)
                {
                    try
                    {
                        if (tool.ZAxis != 0)
                        {
                            // calculate surface area
                            // S = 2pi R(2R + a)
                            // where
                            // r = end cap circle radius
                            // a = side length of cyl segment
                            // Div by 2 for tool surface area
                            double endcapRadius = tool.YAxis / 2;
                            double cylSegment = tool.XAxis - (tool.YAxis * 2);

                            tool.SurfaceArea = SigFig((((2 * Math.PI) * endcapRadius) * ((2 * endcapRadius) + cylSegment)) / 2);
                        }
                        else
                        {
                            tool.SurfaceArea = tool.CrossSectionalArea;
                        }
                    }
                    catch
                    {

                    }
                }
                if (tool.Perimeter == 0)
                {
                    // get permiter of capsule ends
                    double ends = (2 * Math.PI * (tool.YAxis / 2));
                    // get length of rectangle sides
                    double recLength = tool.XAxis = tool.YAxis;
                    // calculate perimeter
                    tool.Perimeter = SigFig(ends + recLength);
                }
            }
            else if (tool.ToolGeoTypeValue == 4) // Oval Type
            {
                // Cross Section Not Entered - Calculate
                if (tool.CrossSectionalArea == 0)
                {
                    try
                    {
                        // Cross Section of an ellipse:
                        //
                        // semiMajor axis * semiMinor axis * PI
                        tool.CrossSectionalArea = SigFig((tool.YAxis / 2) * (tool.XAxis / 2) * Math.PI);
                    }
                    catch
                    {

                    }
                }
                // Volume Not Entered - Calculate
                if (tool.CupVolume == 0)
                {
                    if (tool.ZAxis != 0)
                    {
                        try
                        {
                            // vol formula is
                            // V = ((4/3) * pi * MinorAxis * MajorAxis * CupDepth )
                            // div by 2 for cup vol
                            tool.CupVolume = SigFig(((4 / 3) * Math.PI * tool.YAxis * tool.XAxis * tool.ZAxis) / 2);
                        }
                        catch
                        {

                        }
                    }
                }
                // Surface Area not entered - calculate
                if (tool.SurfaceArea == 0)
                {
                    try
                    {
                        // is flat - use Cross Sectional Area
                        if (tool.ZAxis == 0)
                        {
                            tool.SurfaceArea = tool.CrossSectionalArea;
                        }
                        else
                        {
                            // SA = 4 ∙ π ∙ ((a^1.6075 * b^1.6075 + a^1.6075 *c^1.6075 + b^1.6075 * c^1.6075)/3)1/1.6075 
                            // aproximated based on Knud Thomsen's formulae
                            // (SA = 4*Pi(((a^pi * b^pi) + (a^pi * c^pi) + (b^pi * c^pi)) / 3) ^1/pi )
                            // will have ~1% error
                            // Where
                            // a = longaxis
                            // b = short axis
                            // c = cup depth

                            double ab = (Math.Pow(tool.XAxis, 1.6075) * (Math.Pow(tool.YAxis, 1.6075)));
                            double ac = (Math.Pow(tool.XAxis, 1.6075) * (Math.Pow(tool.ZAxis, 1.6075)));
                            double bc = (Math.Pow(tool.YAxis, 1.6075) * (Math.Pow(tool.ZAxis, 1.6075)));
                            double p = (1 / 1.6075);
                            double brackets = Math.Pow(((ab + ac + bc) / 3), p);

                            tool.SurfaceArea = SigFig(((4 * Math.PI) * brackets) / 2);

                        }
                    }
                    catch
                    {

                    }
                }
                if (tool.Perimeter == 0)
                {
                    // h = (a-b)^2/(a+b)^2
                    // ellipse perimeter approx pi(a+b) (1 + (1/4)h + (1/64)h^2 + (1/256)h^3 + ...)

                    // calculate h
                    double h = (Math.Pow((tool.XAxis - tool.YAxis), 2) / (Math.Pow((tool.XAxis + tool.YAxis), 2)));
                    double outside = Math.PI * (tool.XAxis + tool.YAxis);
                    double inside = (outside * (1 + ((1 / 4) * h) + ((1 / 64) * Math.Pow(h, 2)) + ((1 / 256) * Math.Pow(h, 3))));
                    tool.Perimeter = SigFig(inside);
                }
            }

            else if (tool.ToolGeoTypeValue == 6) // triangle - move to other (will require User Enter Volume/ Cross Section / Surface Area / Perimeter
            {
                if (tool.CrossSectionalArea == 0)
                {
                    try
                    {
                        // using Major Axis for Base
                        // using Minor Axis for Height
                        tool.CrossSectionalArea = SigFig(((0.5) * tool.YAxis * tool.XAxis));
                    }
                    catch
                    {

                    }
                }
                if (tool.CupVolume == 0)
                {
                    // this assumes this is just a triangle shapped tool with flat depth - NOT A THREE SIDED PYRAMID 
                    if (tool.ZAxis != 0)
                    {
                        try
                        {
                            tool.CupVolume = SigFig((tool.CrossSectionalArea * tool.ZAxis));
                        }
                        catch
                        {

                        }
                    }
                }
            }
            else if (tool.ToolGeoTypeValue == 8) // Square
            {
                tool.YAxis = tool.XAxis;

                if (tool.CrossSectionalArea == 0)
                {
                    try
                    {
                        // assumes length and width represented by punch tip dia
                        tool.CrossSectionalArea = SigFig(tool.XAxis * 2);
                    }
                    catch
                    {

                    }
                }
                if (tool.CupVolume == 0)
                {
                    if (tool.ZAxis != 0)
                    {
                        try
                        {
                            tool.CupVolume = SigFig(tool.XAxis * tool.XAxis * tool.ZAxis);
                        }
                        catch
                        {

                        }
                    }
                }
                // surface Area not entered - calculate
                if (tool.SurfaceArea == 0)
                {
                    try
                    {
                        if (tool.ZAxis != 0)
                        {
                            // find surface area of sides
                            double sideArea = (tool.ZAxis * tool.XAxis) * 4;
                            tool.SurfaceArea = SigFig(sideArea + tool.CrossSectionalArea);
                        }
                        else
                        {
                            tool.SurfaceArea = tool.CrossSectionalArea;
                        }
                    }
                    catch
                    {

                    }

                }
                if (tool.Perimeter == 0)
                {
                    tool.Perimeter = SigFig(tool.XAxis * 4);
                }
            }
            else if (tool.ToolGeoTypeValue == 10)// rectangle
            {
                if (tool.CrossSectionalArea == 0)
                {
                    //assumes Major / Minor axis fill in for side measurements
                    try
                    {
                        tool.CrossSectionalArea = SigFig(tool.XAxis * tool.YAxis);
                    }
                    catch
                    {

                    }
                }
                if (tool.CupVolume == 0)
                {
                    if (tool.ZAxis != 0)
                    {
                        try
                        {
                            tool.CupVolume = SigFig(tool.XAxis * tool.YAxis * tool.ZAxis);
                        }
                        catch
                        {

                        }
                    }
                }
                if (tool.SurfaceArea == 0)
                {
                    try
                    {
                        if (tool.ZAxis != 0)
                        {
                            double shortsides = (tool.ZAxis * tool.YAxis) * 2;
                            double longSides = (tool.ZAxis * tool.XAxis) * 2;
                            tool.SurfaceArea = SigFig(longSides + shortsides + tool.CrossSectionalArea);
                        }
                        else
                        {
                            tool.SurfaceArea = tool.CrossSectionalArea;
                        }
                    }
                    catch
                    {

                    }
                }
                if (tool.Perimeter == 0)
                {
                    tool.Perimeter = SigFig((tool.XAxis * 2) + (tool.YAxis * 2));
                }
            }
            // concate remaining feilds to 0000.000 ?
            tool.XAxis = SigFig3(tool.XAxis);
            tool.YAxis = SigFig3(tool.YAxis);
            tool.ZAxis = SigFig3(tool.ZAxis);
        }

        public Tool GenerateTool(Tool tool)
        {
            // round Tool
            if (tool.ToolGeoTypeValue == 1)
            {
                // punch tip dia should never be zero here
                if (tool.XAxis != 0)
                {
                    // X and Y are the same
                    tool.YAxis = tool.XAxis;
                    // make easy r for furture calcs
                    double r = tool.XAxis / 2;

                    // Cross Section Calculate
                    try
                    {
                        // calculate cross sectional area
                        //tool.CrossSectionalArea = Math.Round((.25 * Math.PI) * Math.Pow(tool.PunchTipDiameter, 2), 2, MidpointRounding.AwayFromZero);
                        tool.CrossSectionalArea = Math.Round((Math.PI) * (Math.Pow(r, 2)), 2, MidpointRounding.AwayFromZero);
                    }
                    catch
                    {
                        tool.CrossSectionalArea = 0;
                    }

                    // Volume  - Calculate
                    try
                    {
                        if (tool.ZAxis != 0)
                        {
                            // calculate Cup volume 
                            // get bigR
                            double R = ((Math.Pow(r, 2) + Math.Pow(tool.ZAxis, 2)) / 2 * (tool.ZAxis));
                            //
                            // V = (PI*h(3r^2 + h^2) / 6)
                            tool.CupVolume = SigFig(((Math.PI * tool.ZAxis) * ((3 * Math.Pow(r, 2)) + Math.Pow(tool.ZAxis, 2))) / 6);

                        }
                    }
                    catch
                    {
                        tool.CupVolume = 0;
                    }
                    // Surface Area  - Calculate
                    try
                    {
                        if (tool.ZAxis != 0)
                        {
                            // calculate surface area
                            // SA = pi(a^2 + h^2)
                            tool.SurfaceArea = SigFig(Math.PI * ((Math.Pow(r, 2))) + (Math.Pow(tool.ZAxis, 2)));
                        }
                        else
                        {
                            // surface area = cross section area
                            tool.SurfaceArea = tool.CrossSectionalArea;
                        }
                    }
                    catch
                    {
                        tool.SurfaceArea = 0;
                    }
                    // Perimeter  - Calculate
                    try
                    {
                        tool.Perimeter = SigFig(2 * Math.PI * r);
                    }
                    catch
                    {
                        tool.Perimeter = 0;
                    }
                }
            }
            else if (tool.ToolGeoTypeValue == 2) // Capsule type
            {
                // Cross Section  - Calculate
                // assumes capsule is set on the side
                try
                {
                    // get area of rounded endcaps Diameter will be Short Axis
                    double r = tool.YAxis / 2;
                    double endcapCircArea = Math.Round((Math.PI) * (Math.Pow(r, 2)), 2, MidpointRounding.AwayFromZero);

                    // get reamining rectagle area

                    double rectangleArea = (tool.XAxis - tool.YAxis) * tool.YAxis;


                    tool.CrossSectionalArea = SigFig(rectangleArea + endcapCircArea);
                }
                catch
                {
                    tool.CrossSectionalArea = 0;
                }
                // Volume - Calculate
                try
                {
                    if (tool.ZAxis != 0)
                    {

                        // Volume of Capsule math is
                        //
                        // V = (Pi * (r^2)) * ((4/3) * r + a )
                        //
                        // where a is length of the cyl segment
                        // where r is radius of capsule ends
                        // so cup vol will be (V / 2)
                        //
                        // we assume the minor axis is the diameter of the end cap circle
                        // and that the major axis is the length of the cyl + (minor axis * 2)
                        // 
                        // so this will assume the capsule is set on its side like:
                        // |___| - upper tool
                        // /___\
                        //|     | - Die
                        // \___/
                        // |   | - lower tool
                        double endcapRadius = tool.YAxis / 2;
                        double cylSegment = tool.XAxis - (tool.YAxis * 2);

                        tool.CupVolume = SigFig(((Math.PI * (Math.Pow(endcapRadius, 2))) * ((4 / 3) * endcapRadius + cylSegment)) / 2);

                    }
                    else
                    {
                        // is flat tool, volume is zero
                        tool.CupVolume = 0;
                        // surface area = cross section
                        tool.SurfaceArea = tool.CrossSectionalArea;
                    }

                }
                catch
                {
                    tool.CupVolume = 0;
                }
                // Surface Area  - Calculate
                try
                {
                    if (tool.ZAxis != 0)
                    {
                        // calculate surface area
                        // S = 2pi R(2R + a)
                        // where
                        // r = end cap circle radius
                        // a = side length of cyl segment
                        // Div by 2 for tool surface area
                        double endcapRadius = tool.YAxis / 2;
                        double cylSegment = tool.XAxis - (tool.YAxis * 2);

                        tool.SurfaceArea = SigFig((((2 * Math.PI) * endcapRadius) * ((2 * endcapRadius) + cylSegment)) / 2);
                    }
                    else
                    {
                        tool.SurfaceArea = tool.CrossSectionalArea;
                    }
                }
                catch
                {
                    tool.SurfaceArea = 0;
                }
                // perimeter
                try
                {
                    // get permiter of capsule ends
                    double ends = (2 * Math.PI * (tool.YAxis / 2));
                    // get length of rectangle sides
                    //double recLength = tool.XAxis * tool.YAxis;
                    double recLength = tool.XAxis - (tool.YAxis);
                    // calculate perimeter
                    tool.Perimeter = SigFig(ends + recLength);
                }
                catch
                {
                    tool.Perimeter = 0;
                }

            }
            else if (tool.ToolGeoTypeValue == 4) // Oval Type
            {
                // Cross Section Not Entered - Calculate
                try
                {
                    // Cross Section of an ellipse:
                    //
                    // semiMajor axis * semiMinor axis * PI
                    tool.CrossSectionalArea = SigFig((tool.YAxis / 2) * (tool.XAxis / 2) * Math.PI);
                }
                catch
                {
                    tool.CrossSectionalArea = 0;
                }
                // Volume Not Entered - Calculate
                if (tool.ZAxis != 0)
                {
                    try
                    {
                        // vol formula is
                        // V = ((4/3) * pi * MinorAxis * MajorAxis * CupDepth )
                        // div by 2 for cup vol
                        tool.CupVolume = SigFig(((4 / 3) * Math.PI * tool.YAxis * tool.XAxis * tool.ZAxis) / 2);
                    }
                    catch
                    {
                        tool.CupVolume = 0;
                    }
                }
                else
                {
                    tool.CupVolume = 0;
                }
                // Surface Area not entered - calculate
                try
                {
                    // is flat - use Cross Sectional Area
                    if (tool.ZAxis == 0)
                    {
                        tool.SurfaceArea = tool.CrossSectionalArea;
                    }
                    else
                    {
                        // SA = 4 ∙ π ∙ ((a^1.6075 * b^1.6075 + a^1.6075 *c^1.6075 + b^1.6075 * c^1.6075)/3)1/1.6075 
                        // aproximated based on Knud Thomsen's formulae
                        // (SA = 4*Pi(((a^pi * b^pi) + (a^pi * c^pi) + (b^pi * c^pi)) / 3) ^1/pi )
                        // will have ~1% error
                        // Where
                        // a = longaxis
                        // b = short axis
                        // c = cup depth

                        double ab = (Math.Pow(tool.XAxis, 1.6075) * (Math.Pow(tool.YAxis, 1.6075)));
                        double ac = (Math.Pow(tool.XAxis, 1.6075) * (Math.Pow(tool.ZAxis, 1.6075)));
                        double bc = (Math.Pow(tool.YAxis, 1.6075) * (Math.Pow(tool.ZAxis, 1.6075)));
                        double p = (1 / 1.6075);
                        double brackets = Math.Pow(((ab + ac + bc) / 3), p);

                        tool.SurfaceArea = SigFig(((4 * Math.PI) * brackets) / 2);

                    }
                }
                catch
                {
                    tool.SurfaceArea = 0;
                }
                // perimeter
                try
                {
                    // h = (a-b)^2/(a+b)^2
                    // ellipse perimeter approx pi(a+b) (1 + (1/4)h + (1/64)h^2 + (1/256)h^3 + ...)

                    // calculate h
                    double h = (Math.Pow((tool.XAxis - tool.YAxis), 2) / (Math.Pow((tool.XAxis + tool.YAxis), 2)));
                    double outside = Math.PI * (tool.XAxis + tool.YAxis);
                    double inside = (outside * (1 + ((1 / 4) * h) + ((1 / 64) * Math.Pow(h, 2)) + ((1 / 256) * Math.Pow(h, 3))));
                    tool.Perimeter = SigFig(inside);
                }
                catch
                {
                    tool.Perimeter = 0;
                }
            }

            else if (tool.ToolGeoTypeValue == 6) // triangle - move to other (will require User Enter Volume/ Cross Section / Surface Area / Perimeter
            {
                if (tool.CrossSectionalArea == 0)
                {
                    try
                    {
                        // using Major Axis for Base
                        // using Minor Axis for Height
                        tool.CrossSectionalArea = SigFig(((0.5) * tool.YAxis * tool.XAxis));
                    }
                    catch
                    {

                    }
                }
                if (tool.CupVolume == 0)
                {
                    // this assumes this is just a triangle shapped tool with flat depth - NOT A THREE SIDED PYRAMID 
                    if (tool.ZAxis != 0)
                    {
                        try
                        {
                            tool.CupVolume = SigFig((tool.CrossSectionalArea * tool.ZAxis));
                        }
                        catch
                        {

                        }
                    }
                }
            }
            else if (tool.ToolGeoTypeValue == 8) // Square
            {
                tool.YAxis = tool.XAxis;
                // CrossSectional Area
                try
                {
                    // assumes length and width represented by punch tip dia
                    tool.CrossSectionalArea = SigFig(tool.XAxis * 2);
                }
                catch
                {
                    tool.CrossSectionalArea = 0;
                }
                // Cup Volume
                if (tool.ZAxis != 0)
                {
                    try
                    {
                        tool.CupVolume = SigFig(tool.XAxis * tool.XAxis * tool.ZAxis);
                    }
                    catch
                    {
                        tool.CupVolume = 0;
                    }
                }
                else
                {
                    tool.CupVolume = 0;
                }
                // surface Area  - calculate
                try
                {
                    if (tool.ZAxis != 0)
                    {
                        // find surface area of sides
                        double sideArea = (tool.ZAxis * tool.XAxis) * 4;
                        tool.SurfaceArea = SigFig(sideArea + tool.CrossSectionalArea);
                    }
                    else
                    {
                        tool.SurfaceArea = tool.CrossSectionalArea;
                    }
                }
                catch
                {
                    tool.SurfaceArea = 0;
                }
                try
                {
                    tool.Perimeter = SigFig(tool.XAxis * 4);
                }
                catch
                {
                    tool.Perimeter = 0;
                }

            }
            else if (tool.ToolGeoTypeValue == 10)// rectangle
            {
                // Cross Sectional Area
                //assumes Major / Minor axis fill in for side measurements
                try
                {
                    tool.CrossSectionalArea = SigFig(tool.XAxis * tool.YAxis);
                }
                catch
                {
                    tool.CrossSectionalArea = 0;
                }
                // Cup Volume
                if (tool.ZAxis != 0)
                {
                    try
                    {
                        tool.CupVolume = SigFig(tool.XAxis * tool.YAxis * tool.ZAxis);
                    }
                    catch
                    {
                        tool.CupVolume = 0;
                    }
                }
                else
                {
                    tool.CupVolume = 0;
                }
                // Surface Area
                try
                {
                    if (tool.ZAxis != 0)
                    {
                        double shortsides = (tool.ZAxis * tool.YAxis) * 2;
                        double longSides = (tool.ZAxis * tool.XAxis) * 2;
                        tool.SurfaceArea = SigFig(longSides + shortsides + tool.CrossSectionalArea);
                    }
                    else
                    {
                        tool.SurfaceArea = tool.CrossSectionalArea;
                    }
                }
                catch
                {
                    tool.SurfaceArea = 0;
                }
                // Perimeter
                try
                {
                    tool.Perimeter = SigFig((tool.XAxis * 2) + (tool.YAxis * 2));
                }
                catch
                {
                    tool.Perimeter = 0;
                }

            }
            // concate remaining feilds to 0000.000 ?
            tool.XAxis = SigFig3(tool.XAxis);
            tool.YAxis = SigFig3(tool.YAxis);
            tool.ZAxis = SigFig3(tool.ZAxis);

            return tool;
        }

        #endregion

        #region Post Compaction Calcs

        #region Individual Calc Methods

        public double RunDensityCalc(Tablet tablet, Tool upperTool, Tool lowerTool)
        {
            double OrigDensity = tablet.Density;

            if (tablet.Thickness != 0 && tablet.Weight != 0)
            {
                double bellyband = tablet.Thickness - (upperTool.ZAxis + lowerTool.ZAxis);
                double bellybandVolume = bellyband * upperTool.CrossSectionalArea;

                double tabletVolume = (upperTool.CupVolume + lowerTool.CupVolume) + bellybandVolume;
                try
                {
                    tablet.Density = Math.Round((tablet.Weight) / tabletVolume, 3, MidpointRounding.AwayFromZero);

                    return tablet.Density;
                }
                catch
                {
                    tablet.Density = 0;
                }
            }
            return OrigDensity;
        }

        public double RunSolidFractionCalc(Tablet tablet, Formula formula)
        {
            double originalSolidFrac = tablet.SolidFraction;
            // solid fraction
            if (tablet.Density != 0 && formula.TrueDensity != 0)
            {
                try
                {

                    tablet.SolidFraction = Math.Round((tablet.Density / formula.TrueDensity), 3, MidpointRounding.AwayFromZero);

                    return tablet.SolidFraction;
                }
                catch
                {
                    tablet.SolidFraction = 0;
                }
            }
            return originalSolidFrac;
        }

        public double RunTensileStrCalc(Tablet tablet, Tool uppertool, Tool lowerTool)
        {
            double originalTensileStr = tablet.TensileStrength;

            // tensileStr
            if (tablet.BreakingForce != 0 && tablet.Thickness != 0)
            {
                if (uppertool.ToolGeoTypeValue == 1) // is round
                {
                    if (uppertool.ZAxis == 0 && lowerTool.ZAxis == 0) // is flat tool
                    {
                        try
                        {
                            double fracArea = tablet.Thickness * uppertool.XAxis;

                            tablet.TensileStrength = Math.Round(((tablet.BreakingForce * 2) / (Math.PI * fracArea)), 3, MidpointRounding.AwayFromZero);

                            return tablet.TensileStrength;
                        }
                        catch
                        {
                            tablet.TensileStrength = 0;
                        }
                    }
                    else // is not flat tool
                    {
                        try
                        {
                            double BellyBand = (tablet.Thickness - (uppertool.ZAxis + lowerTool.ZAxis));

                            double LeftSide = ((10 * tablet.BreakingForce) / (Math.PI * Math.Pow(uppertool.XAxis, 2)));

                            double rDenomA = ((2.84 * tablet.Thickness) / uppertool.XAxis);
                            double rDenomB = ((0.126 * tablet.Thickness) / BellyBand);
                            double rDenomC = ((3.15 * BellyBand) / uppertool.XAxis);

                            double RightSide = ((Math.Pow((rDenomA - rDenomB + rDenomC + 0.01), -1)));

                            tablet.TensileStrength = Math.Round((LeftSide * RightSide), 3, MidpointRounding.AwayFromZero);

                            return tablet.TensileStrength;
                        }
                        catch
                        {
                            tablet.TensileStrength = 0;
                        }
                    }
                }

                else if (uppertool.ToolGeoTypeValue == 2) // is capsule
                {
                    // same calcs as oval - req'd non flat calcs
                    if (uppertool.ZAxis == 0 && lowerTool.ZAxis == 0) // is flat tool
                    {
                        try
                        {
                            double fracArea = tablet.Thickness * uppertool.YAxis;
                            tablet.TensileStrength = Math.Round(((tablet.BreakingForce * 2) / (Math.PI * fracArea)) * (.667), 3, MidpointRounding.AwayFromZero);

                            return tablet.TensileStrength;
                        }
                        catch
                        {
                            tablet.TensileStrength = 0;
                        }
                    }
                    else // is domed tool
                    {
                        try
                        {
                            double BellyBand = (tablet.Thickness - (uppertool.ZAxis + lowerTool.ZAxis));

                            double LeftSide = ((10 * tablet.BreakingForce) / (Math.PI * Math.Pow(uppertool.YAxis, 2)));

                            double rDenomA = ((2.84 * tablet.Thickness) / uppertool.YAxis);
                            double rDenomB = ((0.126 * tablet.Thickness) / BellyBand);
                            double rDenomC = ((3.15 * BellyBand) / uppertool.YAxis);

                            double RightSide = ((Math.Pow((rDenomA - rDenomB + rDenomC + 0.01), -1)));

                            tablet.TensileStrength = Math.Round((LeftSide * RightSide) * (0.667), 3, MidpointRounding.AwayFromZero);

                            return tablet.TensileStrength;
                        }
                        catch
                        {
                            tablet.TensileStrength = 0;
                        }

                    }
                }

                else if (uppertool.ToolGeoTypeValue == 4) // is oval
                {
                    if (uppertool.ZAxis == 0 && lowerTool.ZAxis == 0) // is flat tool
                    {
                        try
                        {
                            double fracArea = tablet.Thickness * uppertool.YAxis;
                            tablet.TensileStrength = Math.Round(((tablet.BreakingForce * 2) / (Math.PI * fracArea)) * (.667), 3, MidpointRounding.AwayFromZero);

                            return tablet.TensileStrength;
                        }
                        catch
                        {
                            tablet.TensileStrength = 0;
                        }
                    }
                    else // is domed tool
                    {
                        try
                        {
                            double BellyBand = (tablet.Thickness - (uppertool.ZAxis + lowerTool.ZAxis));

                            double LeftSide = ((10 * tablet.BreakingForce) / (Math.PI * Math.Pow(uppertool.YAxis, 2)));

                            double rDenomA = ((2.84 * tablet.Thickness) / uppertool.YAxis);
                            double rDenomB = ((0.126 * tablet.Thickness) / BellyBand);
                            double rDenomC = ((3.15 * BellyBand) / uppertool.YAxis);

                            double RightSide = ((Math.Pow((rDenomA - rDenomB + rDenomC + 0.01), -1)));

                            tablet.TensileStrength = Math.Round((LeftSide * RightSide) * (0.667), 3, MidpointRounding.AwayFromZero);

                            return tablet.TensileStrength;
                        }
                        catch
                        {
                            tablet.TensileStrength = 0;
                        }

                    }
                }
                else // 
                {
                    // calculations for this data type are currently not availible 
                }
            }

            return originalTensileStr;
        }

        #endregion

        #region Combined Calculations

        /// <summary>
        /// Runs Tablet Post Compaction Formulas
        /// </summary>
        /// <param name="uppertool"></param>
        /// <param name="lowerTool"></param>
        /// <param name="formula"></param>
        /// <param name="tablet"></param>
        public void RunPostCompactionCalcs(Tool uppertool, Tool lowerTool, Formula formula, Tablet tablet)
        {
            // density calculation
            if (tablet.Thickness != 0 && tablet.Weight != 0)
            {
                double bellyband = tablet.Thickness - (uppertool.ZAxis + lowerTool.ZAxis);
                double bellybandVolume = bellyband * uppertool.CrossSectionalArea;

                double tabletVolume = (uppertool.CupVolume + lowerTool.CupVolume) + bellybandVolume;
                try
                {
                    tablet.Density = Math.Round((tablet.Weight) / tabletVolume, 3, MidpointRounding.AwayFromZero);
                }
                catch
                {
                    tablet.Density = 0;
                }

            }
            // solid fraction
            if (tablet.Density != 0 && formula.TrueDensity != 0)
            {
                try
                {

                    tablet.SolidFraction = Math.Round((tablet.Density / formula.TrueDensity), 3, MidpointRounding.AwayFromZero);
                }
                catch
                {
                    tablet.SolidFraction = 0;
                }
            }
            // tensileStr
            if (tablet.BreakingForce != 0 && tablet.Thickness != 0)
            {
                if (uppertool.ToolGeoTypeValue == 1) // is round
                {
                    if (uppertool.ZAxis == 0 && lowerTool.ZAxis == 0) // is flat tool
                    {
                        try
                        {
                            double fracArea = tablet.Thickness * uppertool.XAxis;

                            tablet.TensileStrength = Math.Round(((tablet.BreakingForce * 2) / (Math.PI * fracArea)), 3, MidpointRounding.AwayFromZero);
                        }
                        catch
                        {
                            tablet.TensileStrength = 0;
                        }
                    }
                    else // is not flat tool
                    {
                        try
                        {
                            double BellyBand = (tablet.Thickness - (uppertool.ZAxis + lowerTool.ZAxis));

                            double LeftSide = ((10 * tablet.BreakingForce) / (Math.PI * Math.Pow(uppertool.XAxis, 2)));

                            double rDenomA = ((2.84 * tablet.Thickness) / uppertool.XAxis);
                            double rDenomB = ((0.126 * tablet.Thickness) / BellyBand);
                            double rDenomC = ((3.15 * BellyBand) / uppertool.XAxis);

                            double RightSide = ((Math.Pow((rDenomA - rDenomB + rDenomC + 0.01), -1)));

                            tablet.TensileStrength = Math.Round((LeftSide * RightSide), 3, MidpointRounding.AwayFromZero);
                        }
                        catch
                        {
                            tablet.TensileStrength = 0;
                        }
                    }
                }

                else if (uppertool.ToolGeoTypeValue == 2) // is capsule
                {
                    // same calcs as oval - req'd non flat calcs
                    if (uppertool.ZAxis == 0 && lowerTool.ZAxis == 0) // is flat tool
                    {
                        try
                        {
                            double fracArea = tablet.Thickness * uppertool.YAxis;
                            tablet.TensileStrength = Math.Round(((tablet.BreakingForce * 2) / (Math.PI * fracArea)) * (.667), 3, MidpointRounding.AwayFromZero);
                        }
                        catch
                        {
                            tablet.TensileStrength = 0;
                        }
                    }
                    else // is domed tool
                    {
                        try
                        {
                            double BellyBand = (tablet.Thickness - (uppertool.ZAxis + lowerTool.ZAxis));

                            double LeftSide = ((10 * tablet.BreakingForce) / (Math.PI * Math.Pow(uppertool.YAxis, 2)));

                            double rDenomA = ((2.84 * tablet.Thickness) / uppertool.YAxis);
                            double rDenomB = ((0.126 * tablet.Thickness) / BellyBand);
                            double rDenomC = ((3.15 * BellyBand) / uppertool.YAxis);

                            double RightSide = ((Math.Pow((rDenomA - rDenomB + rDenomC + 0.01), -1)));

                            tablet.TensileStrength = Math.Round((LeftSide * RightSide) * (0.667), 3, MidpointRounding.AwayFromZero);
                        }
                        catch
                        {
                            tablet.TensileStrength = 0;
                        }

                    }
                }

                else if (uppertool.ToolGeoTypeValue == 4) // is oval
                {
                    if (uppertool.ZAxis == 0 && lowerTool.ZAxis == 0) // is flat tool
                    {
                        try
                        {
                            double fracArea = tablet.Thickness * uppertool.YAxis;
                            tablet.TensileStrength = Math.Round(((tablet.BreakingForce * 2) / (Math.PI * fracArea)) * (.667), 3, MidpointRounding.AwayFromZero);
                        }
                        catch
                        {
                            tablet.TensileStrength = 0;
                        }
                    }
                    else // is domed tool
                    {
                        try
                        {
                            double BellyBand = (tablet.Thickness - (uppertool.ZAxis + lowerTool.ZAxis));

                            double LeftSide = ((10 * tablet.BreakingForce) / (Math.PI * Math.Pow(uppertool.YAxis, 2)));

                            double rDenomA = ((2.84 * tablet.Thickness) / uppertool.YAxis);
                            double rDenomB = ((0.126 * tablet.Thickness) / BellyBand);
                            double rDenomC = ((3.15 * BellyBand) / uppertool.YAxis);

                            double RightSide = ((Math.Pow((rDenomA - rDenomB + rDenomC + 0.01), -1)));

                            tablet.TensileStrength = Math.Round((LeftSide * RightSide) * (0.667), 3, MidpointRounding.AwayFromZero);
                        }
                        catch
                        {
                            tablet.TensileStrength = 0;
                        }

                    }
                }
                else // 
                {
                    // calculations for this data type are currently not availible 
                }
            }
        }

        #endregion

        #endregion

        #region DataAverage               
        /// <summary>
        /// Averages all data within a study returns StudyAverage object
        /// </summary>
        /// <param name="studyId"></param>
        /// <returns></returns>
        public Task<StudyAverage> CaluclateAverage(int studyId)
        {
            RunAverage runAverage = new RunAverage();
            // calculates the average of data within a study, returns the StudyAverage object
            // fetch sample data
            //foreach (Sample sample in this._sampleService.GetAllSamplesInStudyByID((studyId)))
            //{
            //    this.Samples.Add(sample);
            //}

            //List<double> sampleSetPoints = new List<double>();
            //List<double> sampleForceMax = new List<double>();
            //List<double> sampleEjectMax = new List<double>();
            //List<double> sampleDosing = new List<double>();
            //List<double> sampleDeviation = new List<double>();
            //List<double> sampleCompactionPressure = new List<double>();
            //List<double> sampleEjectionPressure = new List<double>();

            //foreach (Sample sample in this.Samples)
            //{
            //    sampleSetPoints.Add(sample.ForceSP);
            //    sampleForceMax.Add(sample.ForceMax);
            //    sampleEjectMax.Add(sample.EjectMax);
            //    sampleDosing.Add(sample.Dosing);
            //    sampleDeviation.Add(sample.DevPercent);
            //    sampleCompactionPressure.Add(sample.CompactionPressure);
            //    sampleEjectionPressure.Add(sample.EjectionPressure);
            //}
            //// make sure Samples contains data - if yes, generate average
            //if (this.Samples.Count > 0)
            //{
            //    this.StudyAverage.AvgForceSp = SigFig(sampleSetPoints.Average());
            //    this.StudyAverage.AvgForceMax = SigFig(sampleForceMax.Average());
            //    this.StudyAverage.AvgEjectMax = SigFig(sampleEjectMax.Average());
            //    this.StudyAverage.AvgDosing = SigFig(sampleDosing.Average());
            //    this.StudyAverage.AvgDeviation = SigFig(sampleDeviation.Average());
            //    this.StudyAverage.AvgCompactionPress = SigFig(sampleCompactionPressure.Average());
            //    this.StudyAverage.AvgEjectionPress = SigFig(sampleEjectionPressure.Average());
            //}
            //// if not, set to zero
            //else
            //{
            //    this.StudyAverage.AvgForceSp = 0;
            //    this.StudyAverage.AvgForceMax = 0;
            //    this.StudyAverage.AvgEjectMax = 0;
            //    this.StudyAverage.AvgDosing = 0;
            //    this.StudyAverage.AvgDeviation = 0;
            //    this.StudyAverage.AvgCompactionPress = 0;
            //    this.StudyAverage.AvgEjectionPress = 0;
            //}
            //// assign study ID
            //this.StudyAverage.StudyId = studyId;

            //// clear lists
            //sampleSetPoints.Clear();
            //sampleForceMax.Clear();
            //sampleEjectMax.Clear();
            //sampleDosing.Clear();
            //sampleDeviation.Clear();
            //sampleCompactionPressure.Clear();
            //sampleEjectionPressure.Clear();

            //this.Samples.Clear();

            //// Run Tablet Averages
            //// fetch Tablet Data
            //foreach (Tablet tablet in (_tabletService.GetAllTabletsInStudyByID(studyId)))
            //{
            //    this.Tablets.Add(tablet);
            //}
            //// calculate Tablet Data Averages

            //double AvgWeight = 0;
            //double AvgThickness = 0;
            //double AvgHardness = 0;
            //double AvgTensileStr = 0;
            //double AvgSolidFrac = 0;
            //double AvgDensity = 0;
            //double AvgFriability = 0;
            //double AvgDissolution = 0;
            //double AvgDisintegration = 0;

            ////List<double> tabletCompactionPress = new List<double>();
            //List<double> tabletWeights = new List<double>();
            //List<double> tabletThicknesses = new List<double>();
            //List<double> tabletHardnesses = new List<double>();
            //List<double> tabletTensileStr = new List<double>();
            //List<double> tabletSolidFrac = new List<double>();
            //List<double> tabletDensity = new List<double>();
            //List<double> tabletFriability = new List<double>();
            //List<double> tabletDissolution = new List<double>();
            //List<double> tabletDisintegration = new List<double>();

            //foreach (Tablet tablet in this.Tablets)
            //{
            //    //if (tablet.CompactionPress != 0)
            //    //{
            //    //    tabletCompactionPress.Add(tablet.CompactionPress);
            //    //}
            //    if (tablet.Weight != 0)
            //    {
            //        tabletWeights.Add(tablet.Weight);
            //    }
            //    if (tablet.Thickness != 0)
            //    {
            //        tabletThicknesses.Add(tablet.Thickness);
            //    }
            //    if (tablet.BreakingForce != 0)
            //    {
            //        tabletHardnesses.Add(tablet.BreakingForce);
            //    }
            //    if (tablet.TensileStrength != 0)
            //    {
            //        tabletTensileStr.Add(tablet.TensileStrength);
            //    }
            //    if (tablet.SolidFraction != 0)
            //    {
            //        tabletSolidFrac.Add(tablet.SolidFraction);
            //    }
            //    if (tablet.Density != 0)
            //    {
            //        tabletDensity.Add(tablet.Density);
            //    }
            //    if (tablet.Friability != 0)
            //    {
            //        tabletFriability.Add(tablet.Friability);
            //    }
            //    if (tablet.Dissolution != 0)
            //    {
            //        tabletDissolution.Add(tablet.Dissolution);
            //    }
            //    if (tablet.Disintegration != 0)
            //    {
            //        tabletDisintegration.Add(tablet.Disintegration);
            //    }
            //}

            //if (tabletWeights.Count > 0)
            //{
            //    AvgWeight = tabletWeights.Average();
            //}
            //if (tabletThicknesses.Count > 0)
            //{
            //    AvgThickness = tabletThicknesses.Average();
            //}
            //if (tabletHardnesses.Count > 0)
            //{
            //    AvgHardness = tabletHardnesses.Average();
            //}
            //if (tabletTensileStr.Count > 0)
            //{
            //    AvgTensileStr = tabletTensileStr.Average();
            //}
            //if (tabletSolidFrac.Count > 0)
            //{
            //    AvgSolidFrac = tabletSolidFrac.Average();
            //}
            //if (tabletDensity.Count > 0)
            //{
            //    AvgDensity = tabletDensity.Average();
            //}
            //if (tabletFriability.Count > 0)
            //{
            //    AvgFriability = tabletFriability.Average();
            //}
            //if (tabletDissolution.Count > 0)
            //{
            //    AvgDissolution = tabletDissolution.Average();
            //}
            //if (tabletDisintegration.Count > 0)
            //{
            //    AvgDisintegration = tabletDisintegration.Average();
            //}

            //this.StudyAverage.AvgWeight = SigFig(AvgWeight);
            //this.StudyAverage.AvgThickness = SigFig(AvgThickness);
            //this.StudyAverage.AvgBreakingForce = SigFig(AvgHardness);
            //this.StudyAverage.AvgTensileStr = SigFig(AvgTensileStr);
            //this.StudyAverage.AvgSolidFrac = SigFig(AvgSolidFrac);
            //this.StudyAverage.AvgDensity = SigFig(AvgDensity);
            //this.StudyAverage.AvgFriability = SigFig(AvgFriability);
            //this.StudyAverage.AvgDissolution = SigFig(AvgDissolution);
            //this.StudyAverage.AvgDisintegration = SigFig(AvgDisintegration);

            //this.StudyAverage.LastModified = DateTime.Now;

            //// clear lists
            //tabletWeights.Clear();
            //tabletThicknesses.Clear();
            //tabletHardnesses.Clear();
            //tabletTensileStr.Clear();
            //tabletSolidFrac.Clear();
            //tabletDensity.Clear();
            //tabletFriability.Clear();
            //tabletDissolution.Clear();
            //tabletDisintegration.Clear();

            //this.Tablets.Clear();

            return Task.FromResult(this.StudyAverage);
        }

        /// <summary>
        /// Generates Initial Average on Study Completion
        /// Returns Study Average Object
        /// </summary>
        /// <param name="studyId"></param>
        /// <returns></returns>
        public StudyAverage InitCaluclateAverage(int studyId)
        {
            this.StudyAverage = new StudyAverage();
            // calculates the average of data within a study, returns the StudyAverage object
            // fetch sample data
            //foreach (Sample sample in (_sampleService.GetAllSamplesInStudyByID(studyId)))
            //{
            //    this.Samples.Add(sample);
            //}
            //// calculate sample averages
            //List<double> sampleSetPoints = new List<double>();
            //List<double> sampleForceMax = new List<double>();
            //List<double> sampleEjectMax = new List<double>();
            //List<double> sampleDosing = new List<double>();
            //List<double> sampleDeviation = new List<double>();
            //List<double> sampleCompactionPressure = new List<double>();
            //List<double> sampleEjectionPressure = new List<double>();


            //double AvgSetPoint = 0;
            //double AvgForceMax = 0;
            //double AvgEjectMax = 0;
            //double AvgDosing = 0;
            //double AvgDeviation = 0;
            //double AvgCompactionPressure = 0;
            //double AvgEjectionPressure = 0;


            //foreach (Sample sample in this.Samples)
            //{
            //    sampleSetPoints.Add(sample.ForceSP);
            //    sampleForceMax.Add(sample.ForceMax);
            //    sampleEjectMax.Add(sample.EjectMax);
            //    sampleDosing.Add(sample.Dosing);
            //    sampleDeviation.Add(sample.DevPercent);
            //    sampleCompactionPressure.Add(sample.CompactionPressure);
            //    sampleEjectionPressure.Add(sample.EjectionPressure);
            //}

            //foreach (double fsp in sampleSetPoints)
            //{
            //    AvgSetPoint += fsp;
            //}
            //foreach (double forceMax in sampleForceMax)
            //{
            //    AvgForceMax += forceMax;
            //}
            //foreach (double ejectMax in sampleEjectMax)
            //{
            //    AvgEjectMax += ejectMax;
            //}
            //foreach (double dosing in sampleDosing)
            //{
            //    AvgDosing += dosing;
            //}
            //foreach (double deviation in sampleDeviation)
            //{
            //    AvgDeviation += deviation;
            //}
            //foreach (double compactionPressure in sampleCompactionPressure)
            //{
            //    AvgCompactionPressure += compactionPressure;
            //}
            //foreach (double ejectionPressure in sampleEjectionPressure)
            //{
            //    AvgEjectionPressure += ejectionPressure;
            //}

            //AvgSetPoint = (AvgSetPoint / (sampleSetPoints.Count));
            //AvgForceMax = (AvgForceMax / (sampleForceMax.Count));
            //AvgEjectMax = (AvgEjectMax / (sampleEjectMax.Count));
            //AvgDosing = (AvgDosing / (sampleDosing.Count));
            //AvgDeviation = (AvgDeviation / (sampleDeviation.Count));
            //AvgCompactionPressure = (AvgCompactionPressure / (sampleCompactionPressure.Count));
            //AvgEjectionPressure = (AvgEjectionPressure / (sampleEjectionPressure.Count));

            //this.StudyAverage.AvgForceSp = SigFig(AvgSetPoint);
            //this.StudyAverage.AvgForceMax = SigFig(AvgForceMax);
            //this.StudyAverage.AvgEjectMax = SigFig(AvgEjectMax);
            //this.StudyAverage.AvgDosing = SigFig(AvgDosing);
            //this.StudyAverage.AvgDeviation = SigFig(AvgDeviation);
            //this.StudyAverage.AvgCompactionPress = SigFig(AvgCompactionPressure);
            //this.StudyAverage.AvgEjectionPress = SigFig(AvgEjectionPressure);
            //this.StudyAverage.StudyId = studyId;

            //// clear lists
            //sampleSetPoints.Clear();
            //sampleForceMax.Clear();
            //sampleEjectMax.Clear();
            //sampleDosing.Clear();
            //sampleDeviation.Clear();
            //sampleCompactionPressure.Clear();
            //sampleEjectionPressure.Clear();

            //this.Samples.Clear();

            //// fetch Tablet Data
            //foreach (Tablet tablet in (_tabletService.GetAllTabletsInStudyByID(studyId)))
            //{
            //    this.Tablets.Add(tablet);
            //}
            //// calculate Tablet Data Averages

            //double AvgWeight = 0;
            //double AvgThickness = 0;
            //double AvgHardness = 0;
            //double AvgTensileStr = 0;
            //double AvgSolidFrac = 0;
            //double AvgDensity = 0;
            //double AvgFriability = 0;
            //double AvgDissolution = 0;
            //double AvgDisintegration = 0;


            //List<double> tabletWeights = new List<double>();
            //List<double> tabletThicknesses = new List<double>();
            //List<double> tabletHardnesses = new List<double>();
            //List<double> tabletTensileStr = new List<double>();
            //List<double> tabletSolidFrac = new List<double>();
            //List<double> tabletDensity = new List<double>();
            //List<double> tabletFriability = new List<double>();
            //List<double> tabletDissolution = new List<double>();
            //List<double> tabletDisintegration = new List<double>();

            //foreach (Tablet tablet in this.Tablets)
            //{
            //    if (tablet.Weight != 0)
            //    {
            //        tabletWeights.Add(tablet.Weight);
            //    }
            //    if (tablet.Thickness != 0)
            //    {
            //        tabletThicknesses.Add(tablet.Thickness);
            //    }
            //    if (tablet.BreakingForce != 0)
            //    {
            //        tabletHardnesses.Add(tablet.BreakingForce);
            //    }
            //    if (tablet.TensileStrength != 0)
            //    {
            //        tabletTensileStr.Add(tablet.TensileStrength);
            //    }
            //    if (tablet.SolidFraction != 0)
            //    {
            //        tabletSolidFrac.Add(tablet.SolidFraction);
            //    }
            //    if (tablet.Density != 0)
            //    {
            //        tabletDensity.Add(tablet.Density);
            //    }
            //    if (tablet.Friability != 0)
            //    {
            //        tabletFriability.Add(tablet.Friability);
            //    }
            //    if (tablet.Dissolution != 0)
            //    {
            //        tabletDissolution.Add(tablet.Dissolution);
            //    }
            //    if (tablet.Disintegration != 0)
            //    {
            //        tabletDisintegration.Add(tablet.Disintegration);
            //    }
            //}

            //if (tabletWeights.Count > 0)
            //{
            //    AvgWeight = tabletWeights.Average();
            //}
            //if (tabletThicknesses.Count > 0)
            //{
            //    AvgThickness = tabletThicknesses.Average();
            //}
            //if (tabletHardnesses.Count > 0)
            //{
            //    AvgHardness = tabletHardnesses.Average();
            //}
            //if (tabletTensileStr.Count > 0)
            //{
            //    AvgTensileStr = tabletTensileStr.Average();
            //}
            //if (tabletSolidFrac.Count > 0)
            //{
            //    AvgSolidFrac = tabletSolidFrac.Average();
            //}
            //if (tabletDensity.Count > 0)
            //{
            //    AvgDensity = tabletDensity.Average();
            //}
            //if (tabletFriability.Count > 0)
            //{
            //    AvgFriability = tabletFriability.Average();
            //}

            //if (tabletDissolution.Count > 0)
            //{
            //    AvgDissolution = tabletDissolution.Average();
            //}
            //if (tabletDisintegration.Count > 0)
            //{
            //    AvgDisintegration = tabletDisintegration.Average();
            //}


            //this.StudyAverage.AvgWeight = SigFig(AvgWeight);
            //this.StudyAverage.AvgThickness = SigFig(AvgThickness);
            //this.StudyAverage.AvgBreakingForce = SigFig(AvgHardness);
            //this.StudyAverage.AvgTensileStr = SigFig(AvgTensileStr);
            //this.StudyAverage.AvgSolidFrac = SigFig(AvgSolidFrac);
            //this.StudyAverage.AvgDensity = SigFig(AvgDensity);
            //this.StudyAverage.AvgFriability = SigFig(AvgFriability);
            //this.StudyAverage.AvgDissolution = SigFig(AvgDissolution);
            //this.StudyAverage.AvgDisintegration = SigFig(AvgDisintegration);
            //this.StudyAverage.LastModified = DateTime.Now;

            //// clear lists
            //tabletWeights.Clear();
            //tabletThicknesses.Clear();
            //tabletHardnesses.Clear();
            //tabletTensileStr.Clear();
            //tabletSolidFrac.Clear();
            //tabletDensity.Clear();
            //tabletFriability.Clear();
            //tabletDissolution.Clear();
            //tabletDisintegration.Clear();

            //this.Tablets.Clear();

            return this.StudyAverage;
        }

        public RunAverage CalculateAverageRun(int runId, int studyId)
        {
            RunAverage runAverage = new RunAverage();
            // calculates the average of data within a study, returns the StudyAverage object
            // fetch sample data
            //foreach (Sample sample in (this._runService.GetAllSamplesInRunByID(studyId, runId)))
            //{
            //    this.Samples.Add(sample);
            //}
            //// calculate sample averages
            //List<double> sampleSetPoints = new List<double>();
            //List<double> sampleForceMax = new List<double>();
            //List<double> sampleEjectMax = new List<double>();
            //List<double> sampleDosing = new List<double>();
            //List<double> sampleDeviation = new List<double>();
            //List<double> sampleCompactionPressure = new List<double>();
            //List<double> sampleEjectionPressure = new List<double>();


            //double AvgSetPoint = 0;
            //double AvgForceMax = 0;
            //double AvgEjectMax = 0;
            //double AvgDosing = 0;
            //double AvgDeviation = 0;
            //double AvgCompactionPressure = 0;
            //double AvgEjectionPressure = 0;


            //foreach (Sample sample in this.Samples)
            //{
            //    sampleSetPoints.Add(sample.ForceSP);
            //    sampleForceMax.Add(sample.ForceMax);
            //    sampleEjectMax.Add(sample.EjectMax);
            //    sampleDosing.Add(sample.Dosing);
            //    sampleDeviation.Add(sample.DevPercent);
            //    sampleCompactionPressure.Add(sample.CompactionPressure);
            //    sampleEjectionPressure.Add(sample.EjectionPressure);
            //}

            //foreach (double fsp in sampleSetPoints)
            //{
            //    AvgSetPoint += fsp;
            //}
            //foreach (double forceMax in sampleForceMax)
            //{
            //    AvgForceMax += forceMax;
            //}
            //foreach (double ejectMax in sampleEjectMax)
            //{
            //    AvgEjectMax += ejectMax;
            //}
            //foreach (double dosing in sampleDosing)
            //{
            //    AvgDosing += dosing;
            //}
            //foreach (double deviation in sampleDeviation)
            //{
            //    AvgDeviation += deviation;
            //}
            //foreach (double compactionPressure in sampleCompactionPressure)
            //{
            //    AvgCompactionPressure += compactionPressure;
            //}
            //foreach (double ejectionPressure in sampleEjectionPressure)
            //{
            //    AvgEjectionPressure += ejectionPressure;
            //}

            //AvgSetPoint = (AvgSetPoint / (sampleSetPoints.Count));
            //AvgForceMax = (AvgForceMax / (sampleForceMax.Count));
            //AvgEjectMax = (AvgEjectMax / (sampleEjectMax.Count));
            //AvgDosing = (AvgDosing / (sampleDosing.Count));
            //AvgDeviation = (AvgDeviation / (sampleDeviation.Count));
            //AvgCompactionPressure = (AvgCompactionPressure / (sampleCompactionPressure.Count));
            //AvgEjectionPressure = (AvgEjectionPressure / (sampleEjectionPressure.Count));

            //runAverage.AvgForceSp = SigFig(AvgSetPoint);
            //runAverage.AvgForceMax = SigFig(AvgForceMax);
            //runAverage.AvgEjectMax = SigFig(AvgEjectMax);
            //runAverage.AvgDosing = SigFig(AvgDosing);
            //runAverage.AvgDeviation = SigFig(AvgDeviation);
            //runAverage.AvgCompactionPress = SigFig(AvgCompactionPressure);
            //runAverage.AvgEjectionPress = SigFig(AvgEjectionPressure);
            ////runAverage.Run.StudyId = studyId;
            //runAverage.Run = this._runService.GetRunByRunID(runId);
            //runAverage.RunId = runId;

            //// clear lists
            //sampleSetPoints.Clear();
            //sampleForceMax.Clear();
            //sampleEjectMax.Clear();
            //sampleDosing.Clear();
            //sampleDeviation.Clear();
            //sampleCompactionPressure.Clear();
            //sampleEjectionPressure.Clear();

            //this.Samples.Clear();

            //// fetch Tablet Data
            //foreach (Tablet tablet in (this._runService.GetAllTabletsInRunByID(studyId, runId)))
            //{
            //    this.Tablets.Add(tablet);
            //}
            //// calculate Tablet Data Averages

            //double AvgWeight = 0;
            //double AvgThickness = 0;
            //double AvgHardness = 0;
            //double AvgTensileStr = 0;
            //double AvgSolidFrac = 0;
            //double AvgDensity = 0;
            //double AvgFriability = 0;
            //double AvgDissolution = 0;
            //double AvgDisintegration = 0;


            //List<double> tabletWeights = new List<double>();
            //List<double> tabletThicknesses = new List<double>();
            //List<double> tabletHardnesses = new List<double>();
            //List<double> tabletTensileStr = new List<double>();
            //List<double> tabletSolidFrac = new List<double>();
            //List<double> tabletDensity = new List<double>();
            //List<double> tabletFriability = new List<double>();
            //List<double> tabletDissolution = new List<double>();
            //List<double> tabletDisintegration = new List<double>();

            //foreach (Tablet tablet in this.Tablets)
            //{
            //    if (tablet.Weight != 0)
            //    {
            //        tabletWeights.Add(tablet.Weight);
            //    }
            //    if (tablet.Thickness != 0)
            //    {
            //        tabletThicknesses.Add(tablet.Thickness);
            //    }
            //    if (tablet.BreakingForce != 0)
            //    {
            //        tabletHardnesses.Add(tablet.BreakingForce);
            //    }
            //    if (tablet.TensileStrength != 0)
            //    {
            //        tabletTensileStr.Add(tablet.TensileStrength);
            //    }
            //    if (tablet.SolidFraction != 0)
            //    {
            //        tabletSolidFrac.Add(tablet.SolidFraction);
            //    }
            //    if (tablet.Density != 0)
            //    {
            //        tabletDensity.Add(tablet.Density);
            //    }
            //    if (tablet.Friability != 0)
            //    {
            //        tabletFriability.Add(tablet.Friability);
            //    }
            //    if (tablet.Dissolution != 0)
            //    {
            //        tabletDissolution.Add(tablet.Dissolution);
            //    }
            //    if (tablet.Disintegration != 0)
            //    {
            //        tabletDisintegration.Add(tablet.Disintegration);
            //    }
            //}

            //if (tabletWeights.Count > 0)
            //{
            //    AvgWeight = tabletWeights.Average();
            //}
            //if (tabletThicknesses.Count > 0)
            //{
            //    AvgThickness = tabletThicknesses.Average();
            //}
            //if (tabletHardnesses.Count > 0)
            //{
            //    AvgHardness = tabletHardnesses.Average();
            //}
            //if (tabletTensileStr.Count > 0)
            //{
            //    AvgTensileStr = tabletTensileStr.Average();
            //}
            //if (tabletSolidFrac.Count > 0)
            //{
            //    AvgSolidFrac = tabletSolidFrac.Average();
            //}
            //if (tabletDensity.Count > 0)
            //{
            //    AvgDensity = tabletDensity.Average();
            //}
            //if (tabletFriability.Count > 0)
            //{
            //    AvgFriability = tabletFriability.Average();
            //}

            //if (tabletDissolution.Count > 0)
            //{
            //    AvgDissolution = tabletDissolution.Average();
            //}
            //if (tabletDisintegration.Count > 0)
            //{
            //    AvgDisintegration = tabletDisintegration.Average();
            //}

            //runAverage.RunNumber = this._runService.GetRunByRunID(runId).RunNumber;
            //runAverage.AvgWeight = SigFig(AvgWeight);
            //runAverage.AvgThickness = SigFig(AvgThickness);
            //runAverage.AvgBreakingForce = SigFig(AvgHardness);
            //runAverage.AvgTensileStr = SigFig(AvgTensileStr);
            //runAverage.AvgSolidFrac = SigFig(AvgSolidFrac);
            //runAverage.AvgDensity = SigFig(AvgDensity);
            //runAverage.AvgFriability = SigFig(AvgFriability);
            //runAverage.AvgDissolution = SigFig(AvgDissolution);
            //runAverage.AvgDisintegration = SigFig(AvgDisintegration);
            //runAverage.LastModified = DateTime.Now;

            //// clear lists
            //tabletWeights.Clear();
            //tabletThicknesses.Clear();
            //tabletHardnesses.Clear();
            //tabletTensileStr.Clear();
            //tabletSolidFrac.Clear();
            //tabletDensity.Clear();
            //tabletFriability.Clear();
            //tabletDissolution.Clear();
            //tabletDisintegration.Clear();

            //this.Tablets.Clear();

            return runAverage;
        }

        #endregion

        
    }
}
