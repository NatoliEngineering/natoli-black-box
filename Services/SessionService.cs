﻿using System.Collections.Generic;
using Dmc.Wpf;
using NatoliAnalytical.Base.Data;
using NatoliAnalytical.Data.TagData;
using NatoliAnalytical.Data.SQL.Entities;


namespace NatoliBlackBox.Services
{
    public sealed class SessionService : NotifyPropertyChanged
    {

        public DataQueue<List<StationData>> DaqData { get; set; }

        public SessionService()
        {
            this._activeChannels = new List<ChannelData>();
            this.DaqData = new DataQueue<List<StationData>>();
            this.PopulateChannels();
            // load existing to active channels?
        }

        #region Popup Properties
        private bool _isBusy;
        public bool IsBusy
        {
            get
            {
                return this._isBusy;
            }
            set
            {
                this.SetProperty(ref this._isBusy, value);
                // Clear the old busy message when the busy pop up is closed so that 
                // an old message isn't accidentally shown the next time the popup opens
                if (value == false)
                {
                    this.BusyMessage = string.Empty;
                }
            }
        }

        private string _busyMessage;
        public string BusyMessage
        {
            get { return _busyMessage; }
            set { SetProperty(ref _busyMessage, value); }
        }
        #endregion

        #region Offset values

        private double _channelOneOffsetDegree;
        public double ChannelOneOffsetDegree
        {
            get { return this._channelOneOffsetDegree; }
            set { this.SetProperty(ref this._channelOneOffsetDegree, value); }
        }

        private double _channelTwoOffsetDegree;
        public double ChannelTwoOffsetDegree
        {
            get { return this._channelTwoOffsetDegree; }
            set { this.SetProperty(ref this._channelTwoOffsetDegree, value); }
        }

        private double _channelThreeOffsetDegree;
        public double ChannelThreeOffsetDegree
        {
            get { return this._channelThreeOffsetDegree; }
            set { this.SetProperty(ref this._channelThreeOffsetDegree, value); }
        }

        private double _channelFourOffsetDegree;
        public double ChannelFourOffsetDegree
        {
            get { return this._channelFourOffsetDegree; }
            set { this.SetProperty(ref this._channelFourOffsetDegree, value); }
        }

        private double _channelFiveOffsetDegree;
        public double ChannelFiveOffsetDegree
        {
            get { return this._channelFiveOffsetDegree; }
            set { this.SetProperty(ref this._channelFiveOffsetDegree, value); }
        }

        private double _channelSixOffsetDegree;
        public double ChannelSixOffsetDegree
        {
            get { return this._channelSixOffsetDegree; }
            set { this.SetProperty(ref this._channelSixOffsetDegree, value); }
        }

        private double _channelSevenOffsetDegree;
        public double ChannelSevenOffsetDegree
        {
            get { return this._channelSevenOffsetDegree; }
            set { this.SetProperty(ref this._channelSevenOffsetDegree, value); }
        }

        private double _channelEightOffsetDegree;
        public double ChannelEightOffsetDegree
        {
            get { return this._channelEightOffsetDegree; }
            set { this.SetProperty(ref this._channelEightOffsetDegree, value); }
        }

        private double _channelNineOffsetDegree;
        public double ChannelNineOffsetDegree
        {
            get { return this._channelNineOffsetDegree; }
            set { this.SetProperty(ref this._channelNineOffsetDegree, value); }
        }

        private double _channelTenOffsetDegree;
        public double ChannelTenOffsetDegree
        {
            get { return this._channelTenOffsetDegree; }
            set { this.SetProperty(ref this._channelTenOffsetDegree, value); }
        }

        #endregion

        #region ActiveChannels

        private List<ChannelData> _activeChannels;
        public List<ChannelData> ActiveChannels
        {
            get { return this._activeChannels; }
            set { this.SetProperty(ref this._activeChannels, value); }
        }

        #endregion

        private void PopulateChannels()
        {
            if (Properties.Settings.Default.PmxChannelOneEnable)
            {
                ChannelData chan1 = new ChannelData(Properties.Settings.Default.PmxChannelOneName, 1, Properties.Settings.Default.ChannelOneOffsetDegree);

                ActiveChannels.Add(chan1);

            }
            if (Properties.Settings.Default.PmxChannelTwoEnable)
            {
                ChannelData chan2 = new ChannelData(Properties.Settings.Default.PmxChannelTwoName, 2, Properties.Settings.Default.ChannelTwoOffsetDegree);

                ActiveChannels.Add(chan2);

            }
            if (Properties.Settings.Default.PmxChannelThreeEnable)
            {
                ChannelData chan3 = new ChannelData(Properties.Settings.Default.PmxChannelThreeName, 3, Properties.Settings.Default.ChannelThreeOffsetDegree);

                ActiveChannels.Add(chan3);

            }
            if (Properties.Settings.Default.PmxChannelFourEnable)
            {
                ChannelData chan4 = new ChannelData(Properties.Settings.Default.PmxChannelFourName, 4, Properties.Settings.Default.ChannelFourOffsetDegree);

                ActiveChannels.Add(chan4);

            }
            if (Properties.Settings.Default.PmxChannelFiveEnable)
            {
                ChannelData chan5 = new ChannelData(Properties.Settings.Default.PmxChannelFiveName, 5, Properties.Settings.Default.ChannelFiveOffsetDegree);

                ActiveChannels.Add(chan5);

            }
            if (Properties.Settings.Default.PmxChannelSixEnable)
            {
                ChannelData chan6 = new ChannelData(Properties.Settings.Default.PmxChannelSixName, 6, Properties.Settings.Default.ChannelSixOffsetDegree);

                ActiveChannels.Add(chan6);

            }
            if (Properties.Settings.Default.PmxChannelSevenEnable)
            {
                ChannelData chan7 = new ChannelData(Properties.Settings.Default.PmxChannelSevenName, 7, Properties.Settings.Default.ChannelSevenOffsetDegree);

                ActiveChannels.Add(chan7);

            }
            if (Properties.Settings.Default.PmxChannelEightEnable)
            {
                ChannelData chan8 = new ChannelData(Properties.Settings.Default.PmxChannelEightName, 8, Properties.Settings.Default.ChannelEightOffsetDegree);

                ActiveChannels.Add(chan8);

            }
            if (Properties.Settings.Default.PmxChannelNineEnable)
            {
                ChannelData chan9 = new ChannelData(Properties.Settings.Default.PmxChannelNineName, 9, Properties.Settings.Default.ChannelNineOffsetDegree);

                ActiveChannels.Add(chan9);

            }
            if (Properties.Settings.Default.PmxChannelTenEnable)
            {
                ChannelData chan10 = new ChannelData(Properties.Settings.Default.PmxChannelTenName, 10, Properties.Settings.Default.ChannelTenOffsetDegree);

                ActiveChannels.Add(chan10);

            }
        }
    }
}
