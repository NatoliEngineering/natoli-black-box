﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Autofac;
using Autofac.Features.OwnedInstances;
using Dmc.Collections;
using Dmc.Wpf;
using NatoliBlackBox.IoC;
using NatoliBlackBox.Popups;
using NatoliBlackBox.Screens;
using NatoliBlackBox.Popups.Models;
using NatoliBlackBox.Screens.Models;

namespace NatoliBlackBox.Services
{
    public sealed class NavigationService : NotifyPropertyChanged
    {

        private const int HistoryCount = 10;

        private readonly DropoutStack<(ScreenControlBase screen, object loadConfiguration)> _screenHistory = new DropoutStack<(ScreenControlBase, object)>(HistoryCount);
        private readonly DropoutStack<(ScreenControlBase screen, object loadConfiguration)> _screenFuture = new DropoutStack<(ScreenControlBase, object)>(HistoryCount);

        private object _activeLoadConfiguration;

        public ObservableCollection<PopupControlBase> ActivePopups { get; } = new ObservableCollection<PopupControlBase>();

        private ScreenControlBase _activeScreen;
        public ScreenControlBase ActiveScreen
        {
            get => this._activeScreen;
            set => this.SetProperty(ref this._activeScreen, value);
        }

        public void NavigateTo<TScreen>(object loadConfiguration = null, object unloadConfiguration = null)
            where TScreen : ScreenControlBase
        {
            this.InternalNavigateTo(typeof(TScreen), loadConfiguration, unloadConfiguration);
        }

        public void NavigateTo(Type screenType, object loadConfiguration = null, object unloadConfiguration = null)
        {
            if (!typeof(ScreenControlBase).IsAssignableFrom(screenType))
                throw new ArgumentException(nameof(screenType), "View models for screens must inherit from ScreenViewModel");

            this.InternalNavigateTo(screenType, loadConfiguration, unloadConfiguration);
        }

        public void InternalNavigateTo(Type screenType, object loadConfiguration = null, object unloadConfiguration = null)
        {
            // if there is currently an active view model, unload it
            if (!(this.ActiveScreen?.TypedDataContext is null))
            {
                this.ActiveScreen.TypedDataContext.Unload(unloadConfiguration);
                this._screenHistory.Push((this.ActiveScreen, this._activeLoadConfiguration));
            }

            this.InternalNavigate(screenType, loadConfiguration);
        }

        private void InternalNavigate(Type screenType, object loadConfiguration = null)
        {
            // set the active view model to be the specified screen and initialize it
            if (IoCContainer.Resolve(screenType) is ScreenControlBase nextScreen)
            {
                this._activeLoadConfiguration = loadConfiguration;
                var nextViewModel = nextScreen.DataContext as ScreenViewModel;
                nextViewModel?.Load(loadConfiguration);
                this.ActiveScreen = nextScreen;
            }
            else
            {
                this.ActiveScreen = null;
                this._activeLoadConfiguration = null;
            }
        }

        public void NavigateBack(object unloadConfiguration = null)
        {
            if (!this._screenHistory.TryPop(out var lastScreen))
                return;
            if (!(this.ActiveScreen is null))
            {
                this.ActiveScreen.TypedDataContext?.Unload(unloadConfiguration);
                this._screenFuture.Push((this.ActiveScreen, this._activeLoadConfiguration));
            }

            this.InternalNavigate(lastScreen.screen.GetType(), lastScreen.loadConfiguration);
        }

        public void NavigateForward(object unloadConfiguration = null)
        {
            if (!this._screenFuture.TryPop(out var futureScreen))
                return;
            if (!(this.ActiveScreen is null))
            {
                this.ActiveScreen.TypedDataContext?.Unload(unloadConfiguration);
                this._screenHistory.Push((this.ActiveScreen, this._activeLoadConfiguration));
            }

            this.InternalNavigate(futureScreen.screen.GetType(), futureScreen.loadConfiguration);
        }

        public Task<IPopupResult> OpenPopupAsync<TPopup>(object loadConfiguration = null) where TPopup : PopupControlBase =>
            this.OpenPopupAsync<TPopup, IPopupResult>(loadConfiguration);

        public Task<TResult> OpenPopupAsync<TPopup, TResult>(object loadConfiguration = null)
            where TPopup : PopupControlBase
            where TResult : IPopupResult
        {
            var popupTaskCompletionSource = new TaskCompletionSource<TResult>();

            var lifetime = IoCContainer.BeginLifetimeScope();
            var nextPopup = lifetime.Resolve<TPopup>();
            if (nextPopup is null || nextPopup.DataContext is null || !typeof(PopupViewModel<TResult>).IsAssignableFrom(nextPopup.DataContext.GetType()))
                return Task.FromResult(default(TResult));

            var nextViewModel = nextPopup.DataContext as PopupViewModel<TResult>;
            nextViewModel.Load(r =>
            {
                this.ActivePopups.Remove(nextPopup);
                nextViewModel.Unload();
                popupTaskCompletionSource.TrySetResult(r);
                lifetime.Dispose();
            }, loadConfiguration);

            this.ActivePopups.Add(nextPopup);

            return popupTaskCompletionSource.Task;
        }
    }
}
