﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dmc.Wpf;
using NatoliAnalytical.Base.Data;
using NatoliAnalytical.Data.TagData;
using NatoliAnalytical.Data.Enums;

namespace NatoliBlackBox.Services
{
    public sealed class UiObjectService : NotifyPropertyChanged
    {
        #region Navigation Buttons

        // home screen
        private bool _homeVisible;
        public bool HomeVisible
        {
            get { return this._homeVisible; }
            set { this.SetProperty(ref this._homeVisible, value); }
        }

        private bool _homeEnable;
        public bool HomeEnable
        {
            get { return this._homeEnable; }
            set { this.SetProperty(ref this._homeEnable, value); }
        }

        // reports screen
        private bool _reportsVisible;
        public bool ReportsVisible
        {
            get { return this._reportsVisible; }
            set { this.SetProperty(ref this._reportsVisible, value); }
        }

        private bool _reportsEnable;
        public bool ReportsEnable
        {
            get { return this._reportsEnable; }
            set { this.SetProperty(ref this._reportsEnable, value); }
        }

        // projects screen
        private bool _projectVisible;
        public bool ProjectVisible
        {
            get { return this._projectVisible; }
            set { this.SetProperty(ref this._projectVisible, value); }
        }

        private bool _projectEnable;
        public bool ProjectEnable
        {
            get { return this._projectEnable; }
            set { this.SetProperty(ref this._projectEnable, value); }
        }

        // formulas screen
        private bool _formulaVisible;
        public bool FormulaVisible
        {
            get { return this._formulaVisible; }
            set { this.SetProperty(ref this._formulaVisible, value); }
        }

        private bool _formulaEnable;
        public bool FormulaEnable
        {
            get { return this._formulaEnable; }
            set { this.SetProperty(ref this._formulaEnable, value); }
        }

        // tools screen
        private bool _toolVisible;
        public bool ToolVisible
        {
            get { return this._toolVisible; }
            set { this.SetProperty(ref this._toolVisible, value); }
        }

        private bool _toolEnable;
        public bool ToolEnable
        {
            get { return this._toolEnable; }
            set { this.SetProperty(ref this._toolEnable, value); }
        }

        // setup screen
        private bool _setupVisible;
        public bool SetupVisible
        {
            get { return this._setupVisible; }
            set { this.SetProperty(ref this._setupVisible, value); }
        }

        private bool _setupEnable;
        public bool SetupEnable
        {
            get { return this._setupEnable; }
            set { this.SetProperty(ref this._setupEnable, value); }
        }

        // users screen
        private bool _usersVisible;
        public bool UsersVisible
        {
            get { return this._usersVisible; }
            set { this.SetProperty(ref this._usersVisible, value); }
        }

        private bool _usersEnable;
        public bool UsersEnable
        {
            get { return this._usersEnable; }
            set { this.SetProperty(ref this._usersEnable, value); }
        }

        // change log screen
        private bool _changeLogVisible;
        public bool ChangeLogVisible
        {
            get { return this._changeLogVisible; }
            set { this.SetProperty(ref this._changeLogVisible, value); }
        }

        private bool _changeLogEnable;
        public bool ChangeLogEnable
        {
            get { return this._changeLogEnable; }
            set { this.SetProperty(ref this._changeLogEnable, value); }
        }

        #endregion

        #region Channel Setup

        private bool _pmxChannelOneEnable;
        public bool PmxChannelOneEnable
        {
            get { return this._pmxChannelOneEnable; }
            set { this.SetProperty(ref this._pmxChannelOneEnable, value); }
        }

        private string _pmxChannelOneName;
        public string PmxChannelOneName
        {
            get { return this._pmxChannelOneName; }
            set { this.SetProperty(ref this._pmxChannelOneName, value); }
        }

        private bool _pmxChannelTwoEnable;
        public bool PmxChannelTwoEnable
        {
            get { return this._pmxChannelTwoEnable; }
            set { this.SetProperty(ref this._pmxChannelTwoEnable, value); }
        }

        private string _pmxChannelTwoName;
        public string PmxChannelTwoName
        {
            get { return this._pmxChannelTwoName; }
            set { this.SetProperty(ref this._pmxChannelTwoName, value); }
        }

        private bool _pmxChannelThreeEnable;
        public bool PmxChannelThreeEnable
        {
            get { return this._pmxChannelThreeEnable; }
            set { this.SetProperty(ref this._pmxChannelThreeEnable, value); }
        }

        private string _pmxChannelThreeName;
        public string PmxChannelThreeName
        {
            get { return this._pmxChannelThreeName; }
            set { this.SetProperty(ref this._pmxChannelThreeName, value); }
        }

        private bool _pmxChannelFourEnable;
        public bool PmxChannelFourEnable
        {
            get { return this._pmxChannelFourEnable; }
            set { this.SetProperty(ref this._pmxChannelFourEnable, value); }
        }

        private string _pmxChannelFourName;
        public string PmxChannelFourName
        {
            get { return this._pmxChannelFourName; }
            set { this.SetProperty(ref this._pmxChannelFourName, value); }
        }

        private bool _pmxChannelFiveEnable;
        public bool PmxChannelFiveEnable
        {
            get { return this._pmxChannelFiveEnable; }
            set { this.SetProperty(ref this._pmxChannelFiveEnable, value); }
        }

        private string _pmxChannelFiveName;
        public string PmxChannelFiveName
        {
            get { return this._pmxChannelFiveName; }
            set { this.SetProperty(ref this._pmxChannelFiveName, value); }
        }

        private bool _pmxChannelSixEnable;
        public bool PmxChannelSixEnable
        {
            get { return this._pmxChannelSixEnable; }
            set { this.SetProperty(ref this._pmxChannelSixEnable, value); }
        }

        private string _pmxChannelSixName;
        public string PmxChannelSixName
        {
            get { return this._pmxChannelSixName; }
            set { this.SetProperty(ref this._pmxChannelSixName, value); }
        }

        private bool _pmxChannelSevenEnable;
        public bool PmxChannelSevenEnable
        {
            get { return this._pmxChannelSevenEnable; }
            set { this.SetProperty(ref this._pmxChannelSevenEnable, value); }
        }

        private string _pmxChannelSevenName;
        public string PmxChannelSevenName
        {
            get { return this._pmxChannelSevenName; }
            set { this.SetProperty(ref this._pmxChannelSevenName, value); }
        }

        private bool _pmxChannelEightEnable;
        public bool PmxChannelEightEnable
        {
            get { return this._pmxChannelEightEnable; }
            set { this.SetProperty(ref this._pmxChannelEightEnable, value); }
        }

        private string _pmxChannelEightName;
        public string PmxChannelEightName
        {
            get { return this._pmxChannelEightName; }
            set { this.SetProperty(ref this._pmxChannelEightName, value); }
        }

        private bool _pmxChannelNineEnable;
        public bool PmxChannelNineEnable
        {
            get { return this._pmxChannelNineEnable; }
            set { this.SetProperty(ref this._pmxChannelNineEnable, value); }
        }

        private string _pmxChannelNineName;
        public string PmxChannelNineName
        {
            get { return this._pmxChannelNineName; }
            set { this.SetProperty(ref this._pmxChannelNineName, value); }
        }

        private bool _pmxChannelTenEnable;
        public bool PmxChannelTenEnable
        {
            get { return this._pmxChannelTenEnable; }
            set { this.SetProperty(ref this._pmxChannelTenEnable, value); }
        }

        private string _pmxChannelTenName;
        public string PmxChannelTenName
        {
            get { return this._pmxChannelTenName; }
            set { this.SetProperty(ref this._pmxChannelTenName, value); }
        }

        #endregion

        #region Machine Setup

        private bool _isDoubleSided;
        public bool IsDoubleSided
        {
            get { return this._isDoubleSided; }
            set { this.SetProperty(ref this._isDoubleSided, value); }
        }

        private int _totalStationCount;
        public int TotalStationCount
        {
            get { return this._totalStationCount; }
            set { this.SetProperty(ref this._totalStationCount, value); }
        }

        private int _activeStationCount;
        public int ActiveStationCount
        {
            get { return this._activeStationCount; }
            set { this.SetProperty(ref this._activeStationCount, value); }
        }

        #endregion

        #region Study

        private bool _studyCreateVisible;
        public bool StudyCreateVisible
        {
            get { return this._studyCreateVisible; }
            set { this.SetProperty(ref this._studyCreateVisible, value); }
        }

        private bool _studyCreateEnable;
        public bool StudyCreateEnable
        {
            get { return this._studyCreateEnable; }
            set { this.SetProperty(ref this._studyCreateEnable, value); }
        }

        private bool _studyCloseVisible;
        public bool StudyCloseVisible
        {
            get { return this._studyCloseVisible; }
            set { this.SetProperty(ref this._studyCloseVisible, value); }
        }

        private bool _studyCloseEnable;
        public bool StudyCloseEnable
        {
            get { return this._studyCloseEnable; }
            set { this.SetProperty(ref this._studyCloseEnable, value); }
        }

        private bool _studyLoadVisible;
        public bool StudyLoadVisible
        {
            get { return this._studyLoadVisible; }
            set { this.SetProperty(ref this._studyLoadVisible, value); }
        }

        private bool _studyLoadEnable;
        public bool StudyLoadEnable
        {
            get { return this._studyLoadEnable; }
            set { this.SetProperty(ref this._studyLoadEnable, value); }
        }

        #endregion

        #region Home Screen Chart

        private string _channelOneDisplayColor;
        public string ChannelOneDisplayColor
        {
            get { return this._channelOneDisplayColor; }
            set { this.SetProperty(ref this._channelOneDisplayColor, value); }
        }

        private string _channelTwoDisplayColor;
        public string ChannelTwoDisplayColor
        {
            get { return this._channelTwoDisplayColor; }
            set { this.SetProperty(ref this._channelTwoDisplayColor, value); }
        }

        private string _channelThreeDisplayColor;
        public string ChannelThreeDisplayColor
        {
            get { return this._channelThreeDisplayColor; }
            set { this.SetProperty(ref this._channelThreeDisplayColor, value); }
        }

        private string _channelFourDisplayColor;
        public string ChannelFourDisplayColor
        {
            get { return this._channelFourDisplayColor; }
            set { this.SetProperty(ref this._channelFourDisplayColor, value); }
        }

        private string _channelFiveDisplayColor;
        public string ChannelFiveDisplayColor
        {
            get { return this._channelFiveDisplayColor; }
            set { this.SetProperty(ref this._channelFiveDisplayColor, value); }
        }

        private string _channelSixDisplayColor;
        public string ChannelSixDisplayColor
        {
            get { return this._channelSixDisplayColor; }
            set { this.SetProperty(ref this._channelSixDisplayColor, value); }
        }

        private string _channelSevenDisplayColor;
        public string ChannelSevenDisplayColor
        {
            get { return this._channelSevenDisplayColor; }
            set { this.SetProperty(ref this._channelSevenDisplayColor, value); }
        }

        private string _channelEightDisplayColor;
        public string ChannelEightDisplayColor
        {
            get { return this._channelEightDisplayColor; }
            set { this.SetProperty(ref this._channelEightDisplayColor, value); }
        }

        private string _channelNineDisplayColor;
        public string ChannelNineDisplayColor
        {
            get { return this._channelNineDisplayColor; }
            set { this.SetProperty(ref this._channelNineDisplayColor, value); }
        }

        private string _channelTenDisplayColor;
        public string ChannelTenDisplayColor
        {
            get { return this._channelTenDisplayColor; }
            set { this.SetProperty(ref this._channelTenDisplayColor, value); }
        }

        private bool _upperChartVis;
        public bool UpperChartVis
        {
            get { return this._upperChartVis; }
            set { this.SetProperty(ref this._upperChartVis, value); }
        }

        private bool _lowerChartVis;
        public bool LowerChartVis
        {
            get { return this._lowerChartVis; }
            set { this.SetProperty(ref this._lowerChartVis, value); }
        }

        private int _upperChartHeight;
        public int UpperChartHeight
        {
            get { return this._upperChartHeight; }
            set { this.SetProperty(ref this._upperChartHeight, value); }
        }

        private int _lowerChartHeight;
        public int LowerChartHeight
        {
            get { return this._lowerChartHeight; }
            set { this.SetProperty(ref this._lowerChartHeight, value); }
        }

        #endregion

        public UiObjectService()
        {

        }


        public void SetVisibilityStates()
        {
            if(this.PmxChannelOneEnable || this.PmxChannelTwoEnable || this.PmxChannelThreeEnable || this.PmxChannelFourEnable || this.PmxChannelFiveEnable)
            {
                this.UpperChartVis = true;
            }
            else
            {
                this.UpperChartVis = false;
            }
            if(this.PmxChannelSixEnable || this.PmxChannelSevenEnable || this.PmxChannelEightEnable || this.PmxChannelNineEnable || this.PmxChannelTenEnable)
            {
                this.LowerChartVis = true;
            }
            else
            {
                this.LowerChartVis = false;
            }
            if(UpperChartVis && LowerChartVis)
            {
                this.UpperChartHeight = 250;
                this.LowerChartHeight = 250;
            }
            else if(UpperChartVis && !LowerChartVis)
            {
                this.UpperChartHeight = 500;
                this.LowerChartHeight = 0;
            }
            else if(!LowerChartVis && UpperChartVis)
            {
                this.UpperChartHeight = 0;
                this.LowerChartHeight = 500;
            }
            else
            {
                // no channels activated - is error
                this.UpperChartHeight = 150;
                this.LowerChartHeight = 150;
            }
        }
    }
}
