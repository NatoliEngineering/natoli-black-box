﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

//Newton
using Newtonsoft.Json;

//DMC
using EventLog = Dmc.Logging.EventLog;

//Custom
using NatoliAnalytical.Base.Interfaces;
using NatoliAnalytical.Data.TagData;
using NatoliAnalytical.DaqModule;

namespace NatoliBlackBox.Services
{
    public sealed class PmxDataService
    {
        #region Class Members
        //Engines
        private readonly SessionService _sessionService;
        private readonly IDaqEngine _daqEngine;

        //Polling Task Handling
        private CancellationTokenSource _tokensource;
        private CancellationToken _stoptoken;
        private Task _daqtask;
        #endregion

        public PmxDataService(SessionService sessionService)
        {
            //Engine local registration
            this._sessionService = sessionService;


            //Construct the driver.
            //if (Properties.Settings.Default.SimulationEnabled)
            //{
            //    this._daqEngine = new SimulateDAQEngine();
            //}
            //else
            //{
            //    this._daqEngine = new PMXEngine(Properties.Settings.Default.TotalStations);
            //}

            //this._daqEngine = new PMXEngine(Properties.Settings.Default.TotalStationCount);

            this._daqEngine = new SimulateDAQEngine();
        }


        /// <summary>
        /// Start and run continuous DAQ session. 
        /// </summary>        
        public void StartDAQSession(string ipaddr, int totalStations, int startingRev)
        {
            try
            {
                EventLog.LogDebug("DAQ Session Start Requested...");
                //Thread management
                this._tokensource = new CancellationTokenSource();
                this._stoptoken = this._tokensource.Token;

                //Clear Queue for Run
                this._sessionService.DaqData.Flush();

                //Connect and Run PMX Unit                    
                this._daqEngine.Connect(ipaddr);
                this._daqEngine.Configure(totalStations, _sessionService.ActiveChannels);
                //TODO: Change this when we add in connectivity to PLC.
                this._daqEngine.Start(startingRev, _sessionService.ActiveChannels, this._sessionService.DaqData);

                //Launch the Poll Read task, and move on.
                this._daqtask = Task.Factory.StartNew(() => this.DaqTask(this._stoptoken), this._stoptoken);

                EventLog.LogDebug("DAQ Session Start Sucessful.");
            }
            catch (Exception ex)
            {
                EventLog.LogFatal("Failed to run PMX streaming session. Error: " + ex.Message);
            }

        }

        /// <summary>
        /// Stop DAQ session, and disconnect PMX device.
        /// </summary>
        public void StopDAQSession()
        {
            try
            {
                EventLog.LogDebug("DAQ Session Stop Requested...");
                //Stop Implementation
                this._tokensource.Cancel();
                Task.WaitAll(this._daqtask);
                EventLog.LogDebug("The DAQ Task Stopped. Task Status is now: {0}", this._daqtask.Status);
            }
            catch (Exception ex)
            {
                EventLog.LogFatal("DAQ Session Stop Failed. Error: " + ex.Message);
            }
            finally
            {
                //Always clean up, even on task failure.
                this._daqEngine.Stop();
                this._daqEngine.Disconnect();
                EventLog.LogDebug("DAQ Stop Completed.");
            }
        }

        #region Internal Read Task
        private async Task DaqTask(CancellationToken stopToken)
        {
            while (!stopToken.IsCancellationRequested)
            {
                try
                {
                    await Task.Run(() =>
                    {
                        //Read DAQ Data
                        this._daqEngine.Read(_sessionService.ActiveChannels);
                    });

                }
                catch (Exception ex)
                {
                    EventLog.LogFatal("Failed to read DAQ device. Error: " + ex.Message);
                }
            }
        }
        #endregion
    }
}
