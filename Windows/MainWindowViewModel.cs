﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Threading;
using System.Threading.Tasks;

//DMC
using Dmc.Wpf.Commands;
using EventLog = Dmc.Logging.EventLog;

//Custom
using NatoliBlackBox.Screens;
using NatoliBlackBox.Screens.Views;
using NatoliBlackBox.Services;
using NatoliAnalytical.Base.Interfaces;
using NatoliAnalytical.Data.SQL.Entities;
using NatoliBlackBox.Popups;
//using NatoliBlackBox.Popups.PopupResults;
//using NatoliBlackBox.Popups.Views;
using NatoliBlackBox.Popups.Models;


namespace NatoliBlackBox.Windows
{
    public class MainWindowViewModel : ViewModelBase
    {
        //System Services
        private NavigationService _NavigationService;
        

        public NavigationService NavigationService
        {
            get
            {
                return this._NavigationService;
            }
            private set
            {
                this.SetProperty(ref this._NavigationService, value);
            }
        }

        //Class Members
        private Version _appVersion;
        public Version AppVersion
        {
            get
            {
                if (this._appVersion == null)
                    this._appVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
                return this._appVersion;
            }
        }


        private bool _loginActive;
        public bool LoginActive
        {
            get { return this._loginActive; }
            set { this.SetProperty(ref this._loginActive, value); }
        }

        private bool _logoutActive;
        public bool LogoutActive
        {
            get { return this._logoutActive; }
            set { this.SetProperty(ref this._logoutActive, value); }
        }

        public ICommand NavigateToScreenCommand { get; set; }

        public MainWindowViewModel(NavigationService navigationService, SessionService session, UiObjectService uiService) : base(session, uiService)
        {
            this.NavigationService = navigationService;

            this.NavigateToScreenCommand = new RelayCommand(this.NavigateToScreen);

            this.Load();
        }

        protected internal override bool Load(object configuration = null)
        {
            this.NavigationService.NavigateTo<HomeScreen>();
            return true;
        }

        private void NavigateToScreen(object param)
        {
            var viewModelType = param as Type;
            if (viewModelType != null)
            {
                this.NavigationService.NavigateTo(viewModelType);
            }
        }
    }
}
