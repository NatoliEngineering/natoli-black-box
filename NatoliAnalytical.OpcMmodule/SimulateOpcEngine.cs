﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

//DMC
using EventLog = Dmc.Logging.EventLog;

//Custom
using NatoliAnalytical.Base.Interfaces;
using TitaniumAS.Opc.Client.Da;

namespace NatoliAnalytical.OpcModule
{
    public class SimulateOpcEngine : IOPCEngine
    {
        //Class Members
        //private OPCSimGroup _press;
        private Random _random;
        
        //Constructor
        public SimulateOpcEngine()
        {
            // Init
            this._random = new Random();
        }

        //Interface Methods
        public void Connect()
        {
            //Setup OPC Connection
            EventLog.LogDebug("Connected to Simulated OPC Server at C# 4Eva");
        }

        public void Configure(List<string> tags)
        {
            //Configure tags            
            EventLog.LogDebug("Configured Simulated Tags and OPC Groups");
            foreach (string tag in tags)
            {
                EventLog.LogTrace("Added Tag: {0}", tag);
            }
        }

        public void Disconnect()
        {
            //Simulate Disconnect
            EventLog.LogDebug("Successful Simulate Server Disconnect");
        }

        #region ReadTags
        public object ReadTag(string tagName)
        {
            //Read Bool Tag from Server
            try
            {
                EventLog.LogTrace("Attempting to read tag: " + tagName);
                bool value;
                if (tagName != "[Shortc]Alarms_Present")
                {
                    
                    if (this._random.Next(0, 100) > 49)
                    {
                        value = true;
                    }
                    else
                    {
                        value = false;
                    }
                }
                else
                {
                    value = false;
                }

                EventLog.LogTrace("Read Success, Value: " + value.ToString());                                       
                
                return value;
            }
            catch (Exception ex)
            {
                EventLog.LogFatal(ex.Message);
                throw new Exception("Tag Read Failure. ID: " + tagName, ex);
            }
        }

        public object[] ReadGroup(string groupName)
        {
            //Read entire group of tags, bulk operation.
            return null;
        }

        #endregion

        #region WriteTags
        //Write Tags
        public void WriteTag(string tagName, object value)
        {
            //Write Bool Tag to OPC Server
            try
            {
                EventLog.LogTrace("Attempting OPC Tag Write for object: " + tagName);
                EventLog.LogTrace("Successfully wrote OPC Value: " + value.ToString());
            }
            catch (Exception ex)
            {
                EventLog.LogFatal(ex.Message);
                throw new Exception("Tag Write Failure. ID: " + tagName, ex);
            }

        }
        #endregion

    }

    #region Future
    internal class OPCSimGroup
    {
        public string GroupName { get; set; }

        public List<OPCSimTag> Tags { get; set; }

        public OPCSimGroup(string groupName)
        {
            this.GroupName = groupName;
            this.Tags = new List<OPCSimTag>();
        }
    }

    internal class OPCSimTag
    {
        public string Name { get; set; }
        public object Value { get; set; }

        public OPCSimTag(string name, object value)
        {
            this.Name = name;
            this.Value = value;
        }
    }
    #endregion

}
