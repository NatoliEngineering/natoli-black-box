﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;

//Newton
using Newtonsoft.Json;


//DMC
using EventLog = Dmc.Logging.EventLog;

//Titanium OPC
using TitaniumAS.Opc.Client;
using TitaniumAS.Opc.Client.Da;
using TitaniumAS.Opc.Client.Common;

//Custom
using NatoliAnalytical.Base.Interfaces;
using NatoliAnalytical.Base.Data;

namespace NatoliAnalytical.OpcModule
{
    public class OpcEngine : ITagEngine
    {
        //Class Members
        private readonly OpcDaServer _opcserver;

        //Read Tag Data Support
        private OpcDaGroup _group;

        //Constructor
        public OpcEngine()
        {
            // Init
            // Custom CLSID out of the local client registry,  could not get name identification to work with OPC Service
            var guid = new Guid("3109D532-F9E7-4aa6-B3BB-8D1C5BE480F1");
            this._opcserver = new OpcDaServer(guid);
        }

        //Interface Methods
        public bool Connect()
        {
            //Setup OPC Connection
            try
            {
                EventLog.LogDebug("Connecting to OPC Server at {0} ...", this._opcserver.Uri.ToString());

                this._opcserver.Connect();

                EventLog.LogDebug("Successful connection to OPC Server.");
                return true;
            }
            catch (Exception ex)
            {
               
                EventLog.LogFatal(ex.Message);
                throw new Exception("Failed to connect to OPC Server", ex);
            }


        }

        public void Configure(List<TagConfiguration> tagConfigurations)
        {
            //Configure tags
            try
            {
                EventLog.LogDebug("Configuring tags on OPC service");

                // Create a group for the items.
                this._group = _opcserver.AddGroup("Press");
                this._group.IsActive = true;
                this._group.UpdateRate = TimeSpan.FromMilliseconds(250);

                //Iterate and add items to the OPC Server
                List<OpcDaItemDefinition> tagDefinitions = new List<OpcDaItemDefinition>();
                foreach (TagConfiguration tag in tagConfigurations)
                {
                    tagDefinitions.Add(new OpcDaItemDefinition
                    {
                        ItemId = tag.TagName,
                        IsActive = true
                    });
                }

                //OpcDaItemDefinition[] definitions = { definition1, definition2 };
                OpcDaItemResult[] results = this._group.AddItems(tagDefinitions.ToArray());

                // Handle adding results.
                foreach (OpcDaItemResult result in results)
                {
                    if (result.Error.Failed)
                        EventLog.LogError("Error adding items: {0}", result.Error);
                }
            }
            catch (Exception ex)
            {
                EventLog.LogFatal(ex.Message);
                throw new Exception("Failed to configure OPC tags.", ex);
            }

        }

        public bool Disconnect()
        {
            //Disconnect from OPC Connection
            try
            {
                EventLog.LogDebug("Disconnecting from OPC Server");

                this._opcserver.Disconnect();
                EventLog.LogDebug("Successful Server Disconnect");
                return true;
            }
            catch (Exception ex)
            {
                EventLog.LogFatal(ex.Message);
                throw new Exception("Failed to close connection to OPC Server.", ex);
            }

        }

        //Read Tags
        public object ReadTag(string tagName)
        {
            //Read Bool Tag from Server
            try
            {
                EventLog.LogTrace("Attempting to read tag: " + tagName);

                OpcDaItem item = this._group.Items.FirstOrDefault(i => i.ItemId == tagName);
                OpcDaItem[] items = { item };

                OpcDaItemValue[] values = this._group.Read(items, OpcDaDataSource.Device);

                EventLog.LogTrace("Read Success, Value: " + values[0].Value.ToString());
                return values[0].Value;
            }
            catch (Exception ex)
            {
                EventLog.LogFatal(ex.Message);
                throw new Exception("Tag Read Failure. ID: " + tagName, ex);
            }
        }

        //Write Tags
        public void WriteTag(string tagName, object value)
        {
            //Write Bool Tag to OPC Server
            try
            {
                EventLog.LogTrace("Attempting OPC Tag Write for object: " + tagName);
                //Prepare the tag
                OpcDaItem tag = this._group.Items.FirstOrDefault(i => i.ItemId == tagName);
                OpcDaItem[] tags = { tag };

                //Set the value
                object[] values = { value };

                //Write the Server (Synchronous)
                HRESULT[] results = this._group.Write(tags, values);

                foreach (HRESULT r in results)
                {
                    if (r.Failed)
                    {
                        EventLog.LogFatal("Failed to write OPC Tag: " + tagName);
                    }
                    else
                    {
                        EventLog.LogTrace("Successfully wrote OPC object: " + tagName);
                    }

                }
            }
            catch (Exception ex)
            {
                EventLog.LogFatal(ex.Message);
                throw new Exception("Tag Write Failure. ID: " + tagName, ex);
            }

        }
    }
}
