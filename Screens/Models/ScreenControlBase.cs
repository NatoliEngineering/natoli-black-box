﻿using System.Windows.Controls;

namespace NatoliBlackBox.Screens.Models
{
    public abstract class ScreenControlBase : UserControl
    {

        internal ScreenViewModel TypedDataContext
        {
            get => this.DataContext as ScreenViewModel;
            set => this.DataContext = value;
        }


    }
}
