﻿using NatoliBlackBox.Services;

namespace NatoliBlackBox.Screens.Models
{
    public abstract class ScreenViewModel : ViewModelBase
    {

        protected NavigationService NavigationService { get; }

        public ScreenViewModel(SessionService session, UiObjectService uiService, NavigationService navigationService) : base(session, uiService)
        {
            this.NavigationService = navigationService;
        }
    }
}
