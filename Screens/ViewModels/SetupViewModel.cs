﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NatoliBlackBox.Services;
using NatoliBlackBox.Screens.Models;
using NatoliAnalytical.Data.SQL.Entities;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Dmc.Wpf.Commands;
using NatoliAnalytical.Base.Data;

namespace NatoliBlackBox.Screens.ViewModels
{
    public partial class SetupViewModel : ScreenViewModel
    {
        #region Channel Properties

        private ObservableCollection<string> _channelNameList;
        public ObservableCollection<string> ChannelNameList
        {
            get { return this._channelNameList; }
            set { this.SetProperty(ref this._channelNameList, value); }
        }

        #region Channel Names

        private string _selectedNameChannelOne;
        public string SelectedNameChannelOne
        {
            get { return this._selectedNameChannelOne; }
            set { this.SetProperty(ref this._selectedNameChannelOne, value); }
        }

        private string _selectedNameChannelTwo;
        public string SelectedNameChannelTwo
        {
            get { return this._selectedNameChannelTwo; }
            set { this.SetProperty(ref this._selectedNameChannelTwo, value); }
        }

        private string _selectedNameChannelThree;
        public string SelectedNameChannelThree
        {
            get { return this._selectedNameChannelThree; }
            set { this.SetProperty(ref this._selectedNameChannelThree, value); }
        }

        private string _selectedNameChannelFour;
        public string SelectedNameChannelFour
        {
            get { return this._selectedNameChannelFour; }
            set { this.SetProperty(ref this._selectedNameChannelFour, value); }
        }

        private string _selectedNameChannelFive;
        public string SelectedNameChannelFive
        {
            get { return this._selectedNameChannelFive; }
            set { this.SetProperty(ref this._selectedNameChannelFive, value); }
        }

        private string _selectedNameChannelSix;
        public string SelectedNameChannelSix
        {
            get { return this._selectedNameChannelSix; }
            set { this.SetProperty(ref this._selectedNameChannelSix, value); }
        }

        private string _selectedNameChannelSeven;
        public string SelectedNameChannelSeven
        {
            get { return this._selectedNameChannelSeven; }
            set { this.SetProperty(ref this._selectedNameChannelSeven, value); }
        }

        private string _selectedNameChannelEight;
        public string SelectedNameChannelEight
        {
            get { return this._selectedNameChannelEight; }
            set { this.SetProperty(ref this._selectedNameChannelEight, value); }
        }

        private string _selectedNameChannelNine;
        public string SelectedNameChannelNine
        {
            get { return this._selectedNameChannelNine; }
            set { this.SetProperty(ref this._selectedNameChannelNine, value); }
        }

        private string _selectedNameChannelTen;
        public string SelectedNameChannelTen
        {
            get { return this._selectedNameChannelTen; }
            set { this.SetProperty(ref this._selectedNameChannelTen, value); }
        }

        #endregion

        #region Channel Enables

        private bool _pmxChannelOneEnable;
        public bool PmxChannelOneEnable
        {
            get { return this._pmxChannelOneEnable; }
            set { this.SetProperty(ref this._pmxChannelOneEnable, value); }
        }

        private bool _pmxChannelTwoEnable;
        public bool PmxChannelTwoEnable
        {
            get { return this._pmxChannelTwoEnable; }
            set { this.SetProperty(ref this._pmxChannelTwoEnable, value); }
        }

        private bool _pmxChannelThreeEnable;
        public bool PmxChannelThreeEnable
        {
            get { return this._pmxChannelThreeEnable; }
            set { this.SetProperty(ref this._pmxChannelThreeEnable, value); }
        }

        private bool _pmxChannelFourEnable;
        public bool PmxChannelFourEnable
        {
            get { return this._pmxChannelFourEnable; }
            set { this.SetProperty(ref this._pmxChannelFourEnable, value); }
        }

        private bool _pmxChannelFiveEnable;
        public bool PmxChannelFiveEnable
        {
            get { return this._pmxChannelFiveEnable; }
            set { this.SetProperty(ref this._pmxChannelFiveEnable, value); }
        }

        private bool _pmxChannelSixEnable;
        public bool PmxChannelSixEnable
        {
            get { return this._pmxChannelSixEnable; }
            set { this.SetProperty(ref this._pmxChannelSixEnable, value); }
        }

        private bool _pmxChannelSevenEnable;
        public bool PmxChannelSevenEnable
        {
            get { return this._pmxChannelSevenEnable; }
            set { this.SetProperty(ref this._pmxChannelSevenEnable, value); }
        }

        private bool _pmxChannelEightEnable;
        public bool PmxChannelEightEnable
        {
            get { return this._pmxChannelEightEnable; }
            set { this.SetProperty(ref this._pmxChannelEightEnable, value); }
        }

        private bool _pmxChannelNineEnable;
        public bool PmxChannelNineEnable
        {
            get { return this._pmxChannelNineEnable; }
            set { this.SetProperty(ref this._pmxChannelNineEnable, value); }
        }

        private bool _pmxChannelTenEnable;
        public bool PmxChannelTenEnable
        {
            get { return this._pmxChannelTenEnable; }
            set { this.SetProperty(ref this._pmxChannelTenEnable, value); }
        }

        #endregion

        #endregion

        #region Machine Setup

        private bool _isDoubleSided;
        public bool IsDoubleSided
        {
            get { return this._isDoubleSided; }
            set { this.SetProperty(ref this._isDoubleSided, value); }
        }

        private int _totalStationCount;
        public int TotalStationCount
        {
            get { return this._totalStationCount; }
            set { this.SetProperty(ref this._totalStationCount, value); }
        }

        private int _activeStationCount;
        public int ActiveStationCount
        {
            get { return this._activeStationCount; }
            set { this.SetProperty(ref this._activeStationCount, value); }
        }

        private double _turretCircumfrence;
        public double TurretCircumfrence
        {
            get { return this._turretCircumfrence; }
            set { this.SetProperty(ref this._turretCircumfrence, value); }
        }

        private double _turretRadius;
        public double TurretRadius
        {
            get { return this._turretRadius; }
            set { this.SetProperty(ref this._turretRadius, value); }
        }

        #endregion

        #region Screen Properties

        private string _errorMessage;
        public string ErrorMessage
        {
            get { return this._errorMessage; }
            set { this.SetProperty(ref this._errorMessage, value); }
        }

        private bool _displayErrorMessage;
        public bool DisplayErrorMessage
        {
            get { return this._displayErrorMessage; }
            set { this.SetProperty(ref this._displayErrorMessage, value); }
        }

        #endregion

        #region Commands

        public ICommand ChannelOneNameChangeCommand { get; private set; }
        public ICommand ChannelTwoNameChangeCommand { get; private set; }
        public ICommand ChannelThreeNameChangeCommand { get; private set; }
        public ICommand ChannelFourNameChangeCommand { get; private set; }
        public ICommand ChannelFiveNameChangeCommand { get; private set; }
        public ICommand ChannelSixNameChangeCommand { get; private set; }
        public ICommand ChannelSevenNameChangeCommand { get; private set; }
        public ICommand ChannelEightNameChangeCommand { get; private set; }
        public ICommand ChannelNineNameChangeCommand { get; private set; }
        public ICommand ChannelTenNameChangeCommand { get; private set; }

        public ICommand ChannelOneToggleCommand { get; private set; }
        public ICommand ChannelTwoToggleCommand { get; private set; }
        public ICommand ChannelThreeToggleCommand { get; private set; }
        public ICommand ChannelFourToggleCommand { get; private set; }
        public ICommand ChannelFiveToggleCommand { get; private set; }
        public ICommand ChannelSixToggleCommand { get; private set; }
        public ICommand ChannelSevenToggleCommand { get; private set; }
        public ICommand ChannelEightToggleCommand { get; private set; }
        public ICommand ChannelNineToggleCommand { get; private set; }
        public ICommand ChannelTenToggleCommand { get; private set; }

        public ICommand SaveChangesCommand { get; private set; }
        public ICommand ToggleDoubleSidedCommand { get; private set; }
        public ICommand TotalStationCountChangedCommand { get; private set; }

        public ICommand ChannelOffsetChangedCommand { get; private set; }

        #endregion

        public SetupViewModel(NavigationService navigationService, SessionService session, UiObjectService uiService) : base(session, uiService, navigationService)
        {

            this._channelNameList = new ObservableCollection<string>();

            this.ChannelOneNameChangeCommand = new RelayCommand(a => this.ChannelOneNameChanged());
            this.ChannelTwoNameChangeCommand = new RelayCommand(a => this.ChannelTwoNameChanged());
            this.ChannelThreeNameChangeCommand = new RelayCommand(a => this.ChannelThreeNameChanged());
            this.ChannelFourNameChangeCommand = new RelayCommand(a => this.ChannelFourNameChanged());
            this.ChannelFiveNameChangeCommand = new RelayCommand(a => this.ChannelFiveNameChanged());
            this.ChannelSixNameChangeCommand = new RelayCommand(a => this.ChannelSixNameChanged());
            this.ChannelSevenNameChangeCommand = new RelayCommand(a => this.ChannelSevenNameChanged());
            this.ChannelEightNameChangeCommand = new RelayCommand(a => this.ChannelEightNameChanged());
            this.ChannelNineNameChangeCommand = new RelayCommand(a => this.ChannelNineNameChanged());
            this.ChannelTenNameChangeCommand = new RelayCommand(a => this.ChannelTenNameChanged());

            this.ChannelOneToggleCommand = new RelayCommand(a => this.ToggleChannelOne());
            this.ChannelTwoToggleCommand = new RelayCommand(a => this.ToggleChannelTwo());
            this.ChannelThreeToggleCommand = new RelayCommand(a => this.ToggleChannelThree());
            this.ChannelFourToggleCommand = new RelayCommand(a => this.ToggleChannelFour());
            this.ChannelFiveToggleCommand = new RelayCommand(a => this.ToggleChannelFive());
            this.ChannelSixToggleCommand = new RelayCommand(a => this.ToggleChannelSix());
            this.ChannelSevenToggleCommand = new RelayCommand(a => this.ToggleChannelSeven());
            this.ChannelEightToggleCommand = new RelayCommand(a => this.ToggleChannelEight());
            this.ChannelNineToggleCommand = new RelayCommand(a => this.ToggleChannelNine());
            this.ChannelTenToggleCommand = new RelayCommand(a => this.ToggleChannelTen());

            this.ToggleDoubleSidedCommand = new RelayCommand(a => this.ToggleDoubleSided());
            this.TotalStationCountChangedCommand = new RelayCommand(a => this.TotalStationCountChanged());

            this.ChannelOffsetChangedCommand = new RelayCommand(a => this.SetOffsetValues());

            this.SaveChangesCommand = new RelayCommand(a => this.SaveChanges());

        }

        protected internal override bool Load(object param)
        {
            // load current settings
            this.TurretCircumfrence = Properties.Settings.Default.TurretCircumfrence;
            this.TurretRadius = Properties.Settings.Default.TurretRadius;
            this.ActiveStationCount = Properties.Settings.Default.ActiveStationCount;
            this.TotalStationCount = Properties.Settings.Default.TotalStationCount;
            this.IsDoubleSided = Properties.Settings.Default.IsDoubleSIded;

            this.PmxChannelOneEnable = Properties.Settings.Default.PmxChannelOneEnable;
            this.PmxChannelTwoEnable = Properties.Settings.Default.PmxChannelTwoEnable;
            this.PmxChannelThreeEnable = Properties.Settings.Default.PmxChannelThreeEnable;
            this.PmxChannelFourEnable = Properties.Settings.Default.PmxChannelFourEnable;
            this.PmxChannelFiveEnable = Properties.Settings.Default.PmxChannelFiveEnable;
            this.PmxChannelSixEnable = Properties.Settings.Default.PmxChannelSixEnable;
            this.PmxChannelSevenEnable = Properties.Settings.Default.PmxChannelSevenEnable;
            this.PmxChannelEightEnable = Properties.Settings.Default.PmxChannelEightEnable;
            this.PmxChannelNineEnable = Properties.Settings.Default.PmxChannelNineEnable;
            this.PmxChannelTenEnable = Properties.Settings.Default.PmxChannelTenEnable;

            this.SelectedNameChannelOne = Properties.Settings.Default.PmxChannelOneName;
            this.SelectedNameChannelTwo = Properties.Settings.Default.PmxChannelTwoName;
            this.SelectedNameChannelThree = Properties.Settings.Default.PmxChannelThreeName;
            this.SelectedNameChannelFour = Properties.Settings.Default.PmxChannelFourName;
            this.SelectedNameChannelFive = Properties.Settings.Default.PmxChannelFiveName;
            this.SelectedNameChannelSix = Properties.Settings.Default.PmxChannelSixName;
            this.SelectedNameChannelSeven = Properties.Settings.Default.PmxChannelSevenName;
            this.SelectedNameChannelEight = Properties.Settings.Default.PmxChannelEightName;
            this.SelectedNameChannelNine = Properties.Settings.Default.PmxChannelNineName;
            this.SelectedNameChannelTen = Properties.Settings.Default.PmxChannelTenName;

            this.Session.ChannelOneOffsetDegree = Properties.Settings.Default.ChannelOneOffsetDegree;
            this.Session.ChannelTwoOffsetDegree = Properties.Settings.Default.ChannelTwoOffsetDegree;
            this.Session.ChannelThreeOffsetDegree = Properties.Settings.Default.ChannelThreeOffsetDegree;
            this.Session.ChannelFourOffsetDegree = Properties.Settings.Default.ChannelFourOffsetDegree;
            this.Session.ChannelFiveOffsetDegree = Properties.Settings.Default.ChannelFiveOffsetDegree;
            this.Session.ChannelSixOffsetDegree = Properties.Settings.Default.ChannelSixOffsetDegree;
            this.Session.ChannelSevenOffsetDegree = Properties.Settings.Default.ChannelSevenOffsetDegree;
            this.Session.ChannelEightOffsetDegree = Properties.Settings.Default.ChannelEightOffsetDegree;
            this.Session.ChannelNineOffsetDegree = Properties.Settings.Default.ChannelNineOffsetDegree;
            this.Session.ChannelTenOffsetDegree = Properties.Settings.Default.ChannelTenOffsetDegree;

            // load channel selections
            if (this.IsDoubleSided)
            {
                this.ChannelNameList.Add("Side One Main Compression");
                this.ChannelNameList.Add("Side One Pre Compression");
                this.ChannelNameList.Add("Side One Ejection");
                this.ChannelNameList.Add("Side One Tablet Take Off");

                this.ChannelNameList.Add("Side Two Main Compression");
                this.ChannelNameList.Add("Side Two Pre Compression");
                this.ChannelNameList.Add("Side Two Ejection");
                this.ChannelNameList.Add("Side Two Tablet Take Off");

                this.ChannelNameList.Add("Prox Switch");
            }
            else
            {
                this.ChannelNameList.Add("Main Compression");
                this.ChannelNameList.Add("Pre Compression");
                this.ChannelNameList.Add("Ejection");
                this.ChannelNameList.Add("Tablet Take Off");

                this.ChannelNameList.Add("Prox Switch");
            }
            

            return true;
        }

        protected internal override void Unload(object param)
        {
            this.UiService.SetVisibilityStates();
        }

        #region Channel On/Off Toggles

        private void ToggleChannelOne()
        {
            this.SelectedNameChannelOne = String.Empty;
            Properties.Settings.Default.PmxChannelOneName = String.Empty;

            Properties.Settings.Default.PmxChannelOneEnable = this.UiService.PmxChannelOneEnable;
            Properties.Settings.Default.Save();
        }

        private void ToggleChannelTwo()
        {
            this.SelectedNameChannelTwo = String.Empty;
            Properties.Settings.Default.PmxChannelTwoName = String.Empty;

            Properties.Settings.Default.PmxChannelTwoEnable = this.UiService.PmxChannelTwoEnable;
            Properties.Settings.Default.Save();            
        }

        private void ToggleChannelThree()
        {
            this.SelectedNameChannelThree = String.Empty;
            Properties.Settings.Default.PmxChannelThreeName = String.Empty;

            Properties.Settings.Default.PmxChannelThreeEnable = this.UiService.PmxChannelThreeEnable;
            Properties.Settings.Default.Save();            
        }

        private void ToggleChannelFour()
        {
            this.SelectedNameChannelFour = String.Empty;
            Properties.Settings.Default.PmxChannelFourName = String.Empty;

            Properties.Settings.Default.PmxChannelFourEnable = this.UiService.PmxChannelFourEnable;
            Properties.Settings.Default.Save();
        }

        private void ToggleChannelFive()
        {
            this.SelectedNameChannelFive = String.Empty;
            Properties.Settings.Default.PmxChannelFiveName = String.Empty;

            Properties.Settings.Default.PmxChannelFiveEnable = this.UiService.PmxChannelFiveEnable;
            Properties.Settings.Default.Save();            
        }

        private void ToggleChannelSix()
        {
            this.SelectedNameChannelSix = String.Empty;
            Properties.Settings.Default.PmxChannelSixName = String.Empty;

            Properties.Settings.Default.PmxChannelSixEnable = this.UiService.PmxChannelSixEnable;
            Properties.Settings.Default.Save();
        }

        private void ToggleChannelSeven()
        {
            this.SelectedNameChannelSeven = String.Empty;
            Properties.Settings.Default.PmxChannelSevenName = String.Empty;

            Properties.Settings.Default.PmxChannelSevenEnable = this.UiService.PmxChannelSevenEnable;
            Properties.Settings.Default.Save();
        }

        private void ToggleChannelEight()
        {
            this.SelectedNameChannelEight = String.Empty;
            Properties.Settings.Default.PmxChannelEightName = String.Empty;

            Properties.Settings.Default.PmxChannelEightEnable = this.UiService.PmxChannelEightEnable;
            Properties.Settings.Default.Save();
        }

        private void ToggleChannelNine()
        {
            this.SelectedNameChannelNine = String.Empty;
            Properties.Settings.Default.PmxChannelNineName = String.Empty;

            Properties.Settings.Default.PmxChannelNineEnable = this.UiService.PmxChannelNineEnable;
            Properties.Settings.Default.Save();
        }

        private void ToggleChannelTen()
        {
            this.SelectedNameChannelTen = String.Empty;
            Properties.Settings.Default.PmxChannelTenName = String.Empty;

            Properties.Settings.Default.PmxChannelTenEnable = this.UiService.PmxChannelTenEnable;
            Properties.Settings.Default.Save();

        }

        #endregion

        #region Channel Assignments

        private void ChannelOneNameChanged()
        {
            this.UiService.PmxChannelOneName = this.SelectedNameChannelOne;
            Properties.Settings.Default.PmxChannelOneName = this.SelectedNameChannelOne;
            Properties.Settings.Default.Save();
        }

        private void ChannelTwoNameChanged()
        {
            this.UiService.PmxChannelTwoName = this.SelectedNameChannelTwo;
            Properties.Settings.Default.PmxChannelTwoName = this.SelectedNameChannelTwo;
            Properties.Settings.Default.Save();
        }

        private void ChannelThreeNameChanged()
        {
            this.UiService.PmxChannelThreeName = this.SelectedNameChannelThree;
            Properties.Settings.Default.PmxChannelThreeName = this.SelectedNameChannelThree;
            Properties.Settings.Default.Save();
        }

        private void ChannelFourNameChanged()
        {
            this.UiService.PmxChannelFourName = this.SelectedNameChannelFour;
            Properties.Settings.Default.PmxChannelFourName = this.SelectedNameChannelFour;
            Properties.Settings.Default.Save();
        }

        private void ChannelFiveNameChanged()
        {
            this.UiService.PmxChannelFiveName = this.SelectedNameChannelFive;
            Properties.Settings.Default.PmxChannelFiveName = this.SelectedNameChannelFive;
            Properties.Settings.Default.Save();
        }

        private void ChannelSixNameChanged()
        {
            this.UiService.PmxChannelSixName = this.SelectedNameChannelSix;
            Properties.Settings.Default.PmxChannelSixName = this.SelectedNameChannelSix;
            Properties.Settings.Default.Save();
        }

        private void ChannelSevenNameChanged()
        {
            this.UiService.PmxChannelSevenName = this.SelectedNameChannelSeven;
            Properties.Settings.Default.PmxChannelSevenName = this.SelectedNameChannelSeven;
            Properties.Settings.Default.Save();
        }

        private void ChannelEightNameChanged()
        {
            this.UiService.PmxChannelEightName = this.SelectedNameChannelEight;
            Properties.Settings.Default.PmxChannelEightName = this.SelectedNameChannelEight;
            Properties.Settings.Default.Save();
        }

        private void ChannelNineNameChanged()
        {
            this.UiService.PmxChannelNineName = this.SelectedNameChannelNine;
            Properties.Settings.Default.PmxChannelNineName = this.SelectedNameChannelNine;
            Properties.Settings.Default.Save();
        }

        private void ChannelTenNameChanged()
        {
            this.UiService.PmxChannelTenName = this.SelectedNameChannelTen;
            Properties.Settings.Default.PmxChannelTenName = this.SelectedNameChannelTen;
            Properties.Settings.Default.Save();
        }

        #endregion

        #region machine Settings

        private void TotalStationCountChanged()
        {
            this.DisplayErrorMessage = false;

            if(this.TotalStationCount > 0)
            {
                this.UiService.TotalStationCount = this.TotalStationCount;

                Properties.Settings.Default.TotalStationCount = this.TotalStationCount;
                Properties.Settings.Default.Save();
            }
            else
            {
                this.ErrorMessage = "Station Count Must be Greater Than Zero";
                this.DisplayErrorMessage = true;
            }
            
        }

        private void ToggleDoubleSided()
        {
            Properties.Settings.Default.IsDoubleSIded = this.UiService.IsDoubleSided;
            Properties.Settings.Default.Save();

            // reset all assigned channels

            this.SelectedNameChannelOne = String.Empty;
            this.SelectedNameChannelTwo = String.Empty;
            this.SelectedNameChannelThree = String.Empty;
            this.SelectedNameChannelFour = String.Empty;
            this.SelectedNameChannelFive = String.Empty;
            this.SelectedNameChannelSix = String.Empty;
            this.SelectedNameChannelSeven = String.Empty;
            this.SelectedNameChannelEight = String.Empty;
            this.SelectedNameChannelNine = String.Empty;
            this.SelectedNameChannelTen = String.Empty;

            this.UiService.PmxChannelOneName = this.SelectedNameChannelOne;
            Properties.Settings.Default.PmxChannelOneName = this.SelectedNameChannelOne;

            this.UiService.PmxChannelTwoName = this.SelectedNameChannelTwo;
            Properties.Settings.Default.PmxChannelTwoName = this.SelectedNameChannelTwo;

            this.UiService.PmxChannelThreeName = this.SelectedNameChannelThree;
            Properties.Settings.Default.PmxChannelThreeName = this.SelectedNameChannelThree;

            this.UiService.PmxChannelFourName = this.SelectedNameChannelFour;
            Properties.Settings.Default.PmxChannelFourName = this.SelectedNameChannelFour;

            this.UiService.PmxChannelFiveName = this.SelectedNameChannelFive;
            Properties.Settings.Default.PmxChannelFiveName = this.SelectedNameChannelFive;

            this.UiService.PmxChannelSixName = this.SelectedNameChannelSix;
            Properties.Settings.Default.PmxChannelSixName = this.SelectedNameChannelSix;

            this.UiService.PmxChannelSevenName = this.SelectedNameChannelSeven;
            Properties.Settings.Default.PmxChannelSevenName = this.SelectedNameChannelSeven;

            this.UiService.PmxChannelEightName = this.SelectedNameChannelEight;
            Properties.Settings.Default.PmxChannelEightName = this.SelectedNameChannelEight;

            this.UiService.PmxChannelNineName = this.SelectedNameChannelNine;
            Properties.Settings.Default.PmxChannelNineName = this.SelectedNameChannelNine;

            this.UiService.PmxChannelTenName = this.SelectedNameChannelTen;
            Properties.Settings.Default.PmxChannelTenName = this.SelectedNameChannelTen;

            Properties.Settings.Default.Save();

            this.ChannelNameList.Clear();
            if (UiService.IsDoubleSided)
            {
                this.ChannelNameList.Add("Side One Main Compression");
                this.ChannelNameList.Add("Side One Pre Compression");
                this.ChannelNameList.Add("Side One Ejection");
                this.ChannelNameList.Add("Side One Tablet Take Off");

                this.ChannelNameList.Add("Side Two Main Compression");
                this.ChannelNameList.Add("Side Two Pre Compression");
                this.ChannelNameList.Add("Side Two Ejection");
                this.ChannelNameList.Add("Side Two Tablet Take Off");

                this.ChannelNameList.Add("Prox Switch");
            }
            else
            {
                this.ChannelNameList.Add("Main Compression");
                this.ChannelNameList.Add("Pre Compression");
                this.ChannelNameList.Add("Ejection");
                this.ChannelNameList.Add("Tablet Take Off");

                this.ChannelNameList.Add("Prox Switch");
            }
        }

        #endregion

        private void SaveChanges()
        {
            // clear existing channel list
            this.Session.ActiveChannels.Clear();
            string ErrorString = "";

            this.DisplayErrorMessage = false;

            #region Channel Setup

            if (this.UiService.PmxChannelOneEnable)
            {
                if(!String.IsNullOrEmpty( this.SelectedNameChannelOne) && Session.ChannelOneOffsetDegree != 0)
                {
                    ChannelData chan1 = new ChannelData(this.SelectedNameChannelOne, 1, this.Session.ChannelOneOffsetDegree);

                    this.Session.ActiveChannels.Add(chan1);

                    Properties.Settings.Default.PmxChannelOneName = this.SelectedNameChannelOne;
                    Properties.Settings.Default.ChannelOneOffsetDegree = this.Session.ChannelOneOffsetDegree;
                    Properties.Settings.Default.Save();

                }
                else
                {
                    ErrorString += "\n Channel One Could Not be Configured ";
                }
            }
            if (this.UiService.PmxChannelTwoEnable)
            {
                if (!String.IsNullOrEmpty(this.SelectedNameChannelTwo) && Session.ChannelTwoOffsetDegree != 0)
                {
                    ChannelData chan2 = new ChannelData(this.SelectedNameChannelTwo, 2, this.Session.ChannelTwoOffsetDegree);

                    this.Session.ActiveChannels.Add(chan2);

                    Properties.Settings.Default.PmxChannelTwoName = this.SelectedNameChannelTwo;
                    Properties.Settings.Default.ChannelTwoOffsetDegree = this.Session.ChannelTwoOffsetDegree;
                    Properties.Settings.Default.Save();

                }
                else
                {
                    ErrorString += "\n Channel Two Could Not be Configured ";
                }
            }
            if (this.UiService.PmxChannelThreeEnable)
            {
                if (!String.IsNullOrEmpty(this.SelectedNameChannelThree) && Session.ChannelThreeOffsetDegree != 0)
                {
                    ChannelData chan3 = new ChannelData(this.SelectedNameChannelThree, 3, this.Session.ChannelThreeOffsetDegree);

                    this.Session.ActiveChannels.Add(chan3);

                    Properties.Settings.Default.PmxChannelThreeName = this.SelectedNameChannelThree;
                    Properties.Settings.Default.ChannelThreeOffsetDegree = this.Session.ChannelThreeOffsetDegree;
                    Properties.Settings.Default.Save();

                }
                else
                {
                    ErrorString += "\n Channel Three Could Not be Configured ";
                }
            }
            if (this.UiService.PmxChannelFourEnable)
            {
                if (!String.IsNullOrEmpty(this.SelectedNameChannelFour) && Session.ChannelFourOffsetDegree != 0)
                {
                    ChannelData chan4 = new ChannelData(this.SelectedNameChannelFour, 4, this.Session.ChannelFourOffsetDegree);

                    this.Session.ActiveChannels.Add(chan4);

                    Properties.Settings.Default.PmxChannelFourName = this.SelectedNameChannelFour;
                    Properties.Settings.Default.ChannelFourOffsetDegree = this.Session.ChannelFourOffsetDegree;
                    Properties.Settings.Default.Save();

                }
                else
                {
                    ErrorString += "\n Channel Four Could Not be Configured ";
                }
            }
            if (this.UiService.PmxChannelFiveEnable)
            {
                if (!String.IsNullOrEmpty(this.SelectedNameChannelFive) && Session.ChannelFiveOffsetDegree != 0)
                {
                    ChannelData chan5 = new ChannelData(this.SelectedNameChannelFive, 5, this.Session.ChannelFiveOffsetDegree);

                    this.Session.ActiveChannels.Add(chan5);

                    Properties.Settings.Default.PmxChannelFiveName = this.SelectedNameChannelFive;
                    Properties.Settings.Default.ChannelFiveOffsetDegree = this.Session.ChannelFiveOffsetDegree;
                    Properties.Settings.Default.Save();

                }
                else
                {
                    ErrorString += "\n Channel Five Could Not be Configured ";
                }
            }
            if (this.UiService.PmxChannelSixEnable)
            {
                if (!String.IsNullOrEmpty(this.SelectedNameChannelSix) && Session.ChannelSixOffsetDegree != 0)
                {
                    ChannelData chan6 = new ChannelData(this.SelectedNameChannelSix, 6, this.Session.ChannelSixOffsetDegree);

                    this.Session.ActiveChannels.Add(chan6);

                    Properties.Settings.Default.PmxChannelSixName = this.SelectedNameChannelSix;
                    Properties.Settings.Default.ChannelSixOffsetDegree = this.Session.ChannelSixOffsetDegree;
                    Properties.Settings.Default.Save();

                }
                else
                {
                    ErrorString += "\n Channel Six Could Not be Configured ";
                }
            }
            if (this.UiService.PmxChannelSevenEnable)
            {
                if (!String.IsNullOrEmpty(this.SelectedNameChannelSeven) && Session.ChannelSevenOffsetDegree != 0)
                {
                    ChannelData chan7 = new ChannelData(this.SelectedNameChannelSeven, 7, this.Session.ChannelSevenOffsetDegree);

                    this.Session.ActiveChannels.Add(chan7);

                    Properties.Settings.Default.PmxChannelSevenName = this.SelectedNameChannelSeven;
                    Properties.Settings.Default.ChannelSevenOffsetDegree = this.Session.ChannelSevenOffsetDegree;
                    Properties.Settings.Default.Save();

                }
                else
                {
                    ErrorString += "\n Channel Six Could Not be Configured ";
                }
            }
            if (this.UiService.PmxChannelEightEnable)
            {
                if (!String.IsNullOrEmpty(this.SelectedNameChannelEight) && Session.ChannelEightOffsetDegree != 0)
                {
                    ChannelData chan8 = new ChannelData(this.SelectedNameChannelEight, 8, this.Session.ChannelEightOffsetDegree);

                    this.Session.ActiveChannels.Add(chan8);

                    Properties.Settings.Default.PmxChannelEightName = this.SelectedNameChannelEight;
                    Properties.Settings.Default.ChannelEightOffsetDegree = this.Session.ChannelEightOffsetDegree;
                    Properties.Settings.Default.Save();

                }
                else
                {
                    ErrorString += "\n Channel Eight Could Not be Configured ";
                }
            }
            if (this.UiService.PmxChannelNineEnable)
            {
                if (!String.IsNullOrEmpty(this.SelectedNameChannelNine) && Session.ChannelNineOffsetDegree != 0)
                {
                    ChannelData chan9 = new ChannelData(this.SelectedNameChannelNine, 9, this.Session.ChannelNineOffsetDegree);

                    this.Session.ActiveChannels.Add(chan9);

                    Properties.Settings.Default.PmxChannelNineName = this.SelectedNameChannelNine;
                    Properties.Settings.Default.ChannelNineOffsetDegree = this.Session.ChannelNineOffsetDegree;
                    Properties.Settings.Default.Save();

                }
                else
                {
                    ErrorString += "\n Channel Nine Could Not be Configured ";
                }
            }
            if (this.UiService.PmxChannelTenEnable)
            {
                if (!String.IsNullOrEmpty(this.SelectedNameChannelTen) && Session.ChannelTenOffsetDegree != 0)
                {
                    ChannelData chan10 = new ChannelData(this.SelectedNameChannelTen, 10, this.Session.ChannelTenOffsetDegree);

                    this.Session.ActiveChannels.Add(chan10);

                    Properties.Settings.Default.PmxChannelTenName = this.SelectedNameChannelTen;
                    Properties.Settings.Default.ChannelTenOffsetDegree = this.Session.ChannelTenOffsetDegree;
                    Properties.Settings.Default.Save();

                }
                else
                {
                    ErrorString += "\n Channel Ten Could Not be Configured ";
                }
            }
            

            #endregion

            #region Turret Setup

            if(this.TotalStationCount > 0)
            {
                this.UiService.TotalStationCount = this.TotalStationCount;
                Properties.Settings.Default.TotalStationCount = this.TotalStationCount;
                Properties.Settings.Default.Save();
            }
            else
            {
                ErrorString += "\n Station Count Cannot Be Zero";
            }
            if(this.TurretCircumfrence > 0)
            {
                Properties.Settings.Default.TurretCircumfrence = this.TurretCircumfrence;
                Properties.Settings.Default.Save();
            }
            else
            {
                ErrorString += "\n Circumfrence Cannot Be Zero";
            }
            if (this.TurretRadius > 0 )
            {
                Properties.Settings.Default.TurretRadius = this.TurretRadius;
                Properties.Settings.Default.Save();
            }
            else
            {
                ErrorString += "\n Radius Cannot Be Zero";
            }

            #endregion
            if (!String.IsNullOrEmpty(ErrorString))
            {
                this.ErrorMessage = ErrorString;
                this.DisplayErrorMessage = true;
            }
            // display error string if errors are found
        }

        private void SetOffsetValues()
        {
            Properties.Settings.Default.ChannelOneOffsetDegree = this.Session.ChannelOneOffsetDegree;
            Properties.Settings.Default.ChannelTwoOffsetDegree = this.Session.ChannelTwoOffsetDegree;
            Properties.Settings.Default.ChannelThreeOffsetDegree = this.Session.ChannelThreeOffsetDegree;
            Properties.Settings.Default.ChannelFourOffsetDegree = this.Session.ChannelFourOffsetDegree;
            Properties.Settings.Default.ChannelFiveOffsetDegree = this.Session.ChannelFiveOffsetDegree;
            Properties.Settings.Default.ChannelSixOffsetDegree = this.Session.ChannelSixOffsetDegree;
            Properties.Settings.Default.ChannelSevenOffsetDegree = this.Session.ChannelSevenOffsetDegree;
            Properties.Settings.Default.ChannelEightOffsetDegree = this.Session.ChannelEightOffsetDegree;
            Properties.Settings.Default.ChannelNineOffsetDegree = this.Session.ChannelNineOffsetDegree;
            Properties.Settings.Default.ChannelTenOffsetDegree = this.Session.ChannelTenOffsetDegree;
            Properties.Settings.Default.Save();
        }
    }
}
