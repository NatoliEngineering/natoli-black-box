﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NatoliBlackBox.Screens.Models;
using NatoliBlackBox.Services;
using NatoliAnalytical.Data.SQL.Entities;
using LiveCharts;
using Dmc.Wpf.Commands;
using Tablet = NatoliAnalytical.Data.SQL.Entities.Tablet;
using System.Threading;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.IO.Ports;
using NatoliAnalytical.Data;
using NatoliAnalytical.Base.Data;

namespace NatoliBlackBox.Screens.ViewModels
{
    public partial class HomeViewModel : ScreenViewModel
    {
        #region Services
        PmxDataService _pmxService;



        #endregion

        #region Tasks

        private CancellationTokenSource _CancellationTokenSource;
        private CancellationToken _StopToken;

        private Task _pmxDAQMonitoring;

        #endregion

        #region Live Data Displays

        #region PMX data
        private bool _proxVisible;
        public bool ProxVisible
        {
            get { return this._proxVisible; }
            set { this.SetProperty(ref this._proxVisible, value); }
        }

        private double _channelOneAverage;
        public double ChannelOneAverage
        {
            get { return this._channelOneAverage; }
            set { this.SetProperty(ref this._channelOneAverage, value); }
        }

        private double _channelTwoAverage;
        public double ChannelTwoAverage
        {
            get { return this._channelTwoAverage; }
            set { this.SetProperty(ref this._channelTwoAverage, value); }
        }

        private double _channelThreeAverage;
        public double ChannelThreeAverage
        {
            get { return this._channelThreeAverage; }
            set { this.SetProperty(ref this._channelThreeAverage, value); }
        }

        private double _channelFourAverage;
        public double ChannelFourAverage
        {
            get { return this._channelFourAverage; }
            set { this.SetProperty(ref this._channelFourAverage, value); }
        }

        private double _channelFiveAverage;
        public double ChannelFiveAverage
        {
            get { return this._channelFiveAverage; }
            set { this.SetProperty(ref this._channelFiveAverage, value); }
        }

        private double _channelSixAverage;
        public double ChannelSixAverage
        {
            get { return this._channelSixAverage; }
            set { this.SetProperty(ref this._channelSixAverage, value); }
        }

        private double _channelSevenAverage;
        public double ChannelSevenAverage
        {
            get { return this._channelSevenAverage; }
            set { this.SetProperty(ref this._channelSevenAverage, value); }
        }

        private double _channelEightAverage;
        public double ChannelEightAverage
        {
            get { return this._channelEightAverage; }
            set { this.SetProperty(ref this._channelEightAverage, value); }
        }

        private double _channelNineAverage;
        public double ChannelNineAverage
        {
            get { return this._channelNineAverage; }
            set { this.SetProperty(ref this._channelNineAverage, value); }
        }

        private double _channelTenAverage;
        public double ChannelTenAverage
        {
            get { return this._channelTenAverage; }
            set { this.SetProperty(ref this._channelTenAverage, value); }
        }
        #endregion
        #region Calculated Values

        private double _calculatedTurretSpeed;
        public double CalculatedTurretSpeed
        {
            get { return this._calculatedTurretSpeed; }
            set { this.SetProperty(ref this._calculatedTurretSpeed, value); }
        }

        private int _sampleCount;
        public int SampleCount
        {
            get { return this._sampleCount; }
            set { this.SetProperty(ref this._sampleCount, value); }
        }

        private int _tabletCount;
        public int TabletCount
        {
            get { return this._tabletCount; }
            set { this.SetProperty(ref this._tabletCount, value); }
        }

        #endregion
        #endregion

        #region Charts

        public ChartValues<double> ChannelOneChartValues { get; set; }
        public ChartValues<double> ChannelTwoChartValues { get; set; }
        public ChartValues<double> ChannelThreeChartValues { get; set; }
        public ChartValues<double> ChannelFourChartValues { get; set; }
        public ChartValues<double> ChannelFiveChartValues { get; set; }
        public ChartValues<double> ChannelSixChartValues { get; set; }
        public ChartValues<double> ChannelSevenChartValues { get; set; }
        public ChartValues<double> ChannelEightChartValues { get; set; }
        public ChartValues<double> ChannelNineChartValues { get; set; }
        public ChartValues<double> ChannelTenChartValues { get; set; }

        private bool _upperChartVis;
        public bool UpperChartVis
        {
            get { return this._upperChartVis; }
            set { this.SetProperty(ref this._upperChartVis, value); }
        }

        private bool _lowerChartVis;
        public bool LowerChartVis
        {
            get { return this._lowerChartVis; }
            set { this.SetProperty(ref this._lowerChartVis, value); }
        }

        private int _upperChartHeight;
        public int UpperChartHeight
        {
            get { return this._upperChartHeight; }
            set { this.SetProperty(ref this._upperChartHeight, value); }
        }

        private int _lowerChartHeight;
        public int LowerChartHeight
        {
            get { return this._lowerChartHeight; }
            set { this.SetProperty(ref this._lowerChartHeight, value); }
        }

        #endregion

        private bool SampleIsActive = true;

        #region Sample Data

        //Station Detail List
        public ObservableCollection<StationDetail> _stationdetaildata;
        public ObservableCollection<StationDetail> StationDetailData
        {
            get { return this._stationdetaildata; }
            set { this.SetProperty(ref this._stationdetaildata, value); }
        }

        private bool[] _stationMask;
        public bool[] StationMask
        {
            get { return _stationMask; }
            set { this.SetProperty(ref this._stationMask, value); }
        }


        #endregion

        #region Commands



        #endregion


        public HomeViewModel(PmxDataService pmxService ,NavigationService navigationService, SessionService session, UiObjectService uiService) : base(session, uiService, navigationService)
        {
            this._pmxService = pmxService;


            this.ChannelOneChartValues = new ChartValues<double>();
            this.ChannelTwoChartValues = new ChartValues<double>();
            this.ChannelThreeChartValues = new ChartValues<double>();
            this.ChannelFourChartValues = new ChartValues<double>();
            this.ChannelFiveChartValues = new ChartValues<double>();
            this.ChannelSixChartValues = new ChartValues<double>();
            this.ChannelSevenChartValues = new ChartValues<double>();
            this.ChannelEightChartValues = new ChartValues<double>();
            this.ChannelNineChartValues = new ChartValues<double>();
            this.ChannelTenChartValues = new ChartValues<double>();

            this._stationdetaildata = new ObservableCollection<StationDetail>();

            // load settings
            this.UiService.IsDoubleSided = Properties.Settings.Default.IsDoubleSIded;
            this.UiService.PmxChannelOneEnable = Properties.Settings.Default.PmxChannelOneEnable;
            this.UiService.PmxChannelTwoEnable = Properties.Settings.Default.PmxChannelTwoEnable;
            this.UiService.PmxChannelThreeEnable = Properties.Settings.Default.PmxChannelThreeEnable;
            this.UiService.PmxChannelFourEnable = Properties.Settings.Default.PmxChannelFourEnable;
            this.UiService.PmxChannelFiveEnable = Properties.Settings.Default.PmxChannelFiveEnable;
            this.UiService.PmxChannelSixEnable = Properties.Settings.Default.PmxChannelSixEnable;
            this.UiService.PmxChannelSevenEnable = Properties.Settings.Default.PmxChannelSevenEnable;
            this.UiService.PmxChannelEightEnable = Properties.Settings.Default.PmxChannelEightEnable;
            this.UiService.PmxChannelNineEnable = Properties.Settings.Default.PmxChannelNineEnable;
            this.UiService.PmxChannelTenEnable = Properties.Settings.Default.PmxChannelTenEnable;

            this.UiService.PmxChannelOneName = Properties.Settings.Default.PmxChannelOneName;
            this.UiService.PmxChannelTwoName = Properties.Settings.Default.PmxChannelTwoName;
            this.UiService.PmxChannelThreeName = Properties.Settings.Default.PmxChannelThreeName;
            this.UiService.PmxChannelFourName = Properties.Settings.Default.PmxChannelFourName;
            this.UiService.PmxChannelFiveName = Properties.Settings.Default.PmxChannelFiveName;
            this.UiService.PmxChannelSixName = Properties.Settings.Default.PmxChannelSixName;
            this.UiService.PmxChannelSevenName = Properties.Settings.Default.PmxChannelSevenName;
            this.UiService.PmxChannelEightName = Properties.Settings.Default.PmxChannelEightName;
            this.UiService.PmxChannelNineName = Properties.Settings.Default.PmxChannelNineName;
            this.UiService.PmxChannelTenName = Properties.Settings.Default.PmxChannelTenName;

            this.UiService.ChannelOneDisplayColor = Properties.Settings.Default.ChannelOneDisplayColor;
            this.UiService.ChannelTwoDisplayColor = Properties.Settings.Default.ChannelTwoDisplayColor;
            this.UiService.ChannelThreeDisplayColor = Properties.Settings.Default.ChannelThreeDisplayColor;
            this.UiService.ChannelFourDisplayColor = Properties.Settings.Default.ChannelFourDisplayColor;
            this.UiService.ChannelFiveDisplayColor = Properties.Settings.Default.ChannelFiveDisplayColor;
            this.UiService.ChannelSixDisplayColor = Properties.Settings.Default.ChannelSixDisplayColor;
            this.UiService.ChannelSevenDisplayColor = Properties.Settings.Default.ChannelSevenDisplayColor;
            this.UiService.ChannelEightDisplayColor = Properties.Settings.Default.ChannelEightDisplayColor;
            this.UiService.ChannelNineDisplayColor = Properties.Settings.Default.ChannelNineDisplayColor;
            this.UiService.ChannelTenDisplayColor = Properties.Settings.Default.ChannelTenDisplayColor;

            // set visibility states
            this.UiService.SetVisibilityStates();


            // testing testing

            this._pmxService.StartDAQSession("10.0.1.8", 10, 0);

            this._CancellationTokenSource = new CancellationTokenSource();
            this._StopToken = this._CancellationTokenSource.Token;

            this._pmxDAQMonitoring = Task.Factory.StartNew(() => this.PmxMonitor(this._StopToken), this._StopToken);


        }

        private async Task PmxMonitor(CancellationToken cancellationToken)
        {
            while(!cancellationToken.IsCancellationRequested)
            {
                await Task.Run(() =>
                {
                    //if (UiService.PmxChannelOneEnable)
                    //{
                    //    if(Session.ActiveChannels[0].Values.Count > 0)
                    //    {
                    //        this.ChannelOneChartValues.Clear();

                    //        this.ChannelOneChartValues.AddRange(Session.ActiveChannels[0].Values);

                    //        //this.Session.ActiveChannels.RemoveRange(0, (Session.ActiveChannels[0].Values.Count)-1);
                    //        this.Session.ActiveChannels[0].Values.Clear();
                    //    }
                        

                    //    //foreach(double s in Session.ActiveChannels[0].Values)
                    //    //{
                    //    //    this.ChannelOneChartValues.Add(s);
                    //    //}

                    //    //this.Session.ActiveChannels[0].Values.Clear();
                    //}
                    //if (UiService.PmxChannelTwoEnable)
                    //{
                    //    //this.ChannelTwoChartValues.AddRange(Session.ActiveChannels[1].Values);

                    //    if (Session.ActiveChannels[1].Values.Count > 0)
                    //    {
                    //        this.ChannelTwoChartValues.Clear();

                    //        this.ChannelTwoChartValues.AddRange(Session.ActiveChannels[1].Values);

                    //        //this.Session.ActiveChannels.RemoveRange(0, (Session.ActiveChannels[0].Values.Count)-1);
                    //        this.Session.ActiveChannels[1].Values.Clear();
                    //    }

                    //}
                    //if (UiService.PmxChannelThreeEnable)
                    //{
                    //    if (Session.ActiveChannels[2].Values.Count > 0)
                    //    {
                    //        this.ChannelThreeChartValues.Clear();

                    //        this.ChannelThreeChartValues.AddRange(Session.ActiveChannels[2].Values);

                    //        //this.Session.ActiveChannels.RemoveRange(0, (Session.ActiveChannels[0].Values.Count)-1);
                    //        this.Session.ActiveChannels[2].Values.Clear();
                    //    }
                    //}
                    //if (UiService.PmxChannelFourEnable)
                    //{
                    //    if (Session.ActiveChannels[3].Values.Count > 0)
                    //    {
                    //        this.ChannelFourChartValues.Clear();

                    //        this.ChannelFourChartValues.AddRange(Session.ActiveChannels[3].Values);

                    //        //this.Session.ActiveChannels.RemoveRange(0, (Session.ActiveChannels[0].Values.Count)-1);
                    //        this.Session.ActiveChannels[3].Values.Clear();
                    //    }
                    //}
                    //if (UiService.PmxChannelFiveEnable)
                    //{

                    //}
                    //if (UiService.PmxChannelSixEnable)
                    //{

                    //}

                    // Clear buffer


                });
            }
        }


        private async void HandleNewDataAdded(object sender, EventArgs eventArgs)
        {
            // check for valid data write // test method

            if(UiService.PmxChannelOneEnable)
            {
                this.ChannelOneChartValues.AddRange(Session.ActiveChannels[0].Values);
            }
            if (UiService.PmxChannelTwoEnable)
            {
                this.ChannelTwoChartValues.AddRange(Session.ActiveChannels[1].Values);
            }
            if (UiService.PmxChannelThreeEnable)
            {
                this.ChannelThreeChartValues.AddRange(Session.ActiveChannels[2].Values);
            }
            if (UiService.PmxChannelFourEnable)
            {

            }
            if (UiService.PmxChannelFiveEnable)
            {

            }
            if (UiService.PmxChannelSixEnable)
            {

            }

            //Dev Notes:
            //Event is generated by Enqueue method of DataQueue class, Natoli.Base.Data
            //DAQ Engine -> Post Processor enqueue whole revolutions of data for the VM to display
            //Queue is stored in the Session Service, DaqData and is the sender

            //double tempDosing = 0;
            //double tempPreThickness = 0;
            //double tempMainThickness = 0;

            //Internal Buffering
            List<double> preCompression = new List<double>();
            List<double> mainCompression = new List<double>();
            List<double> ejectionForce = new List<double>();
            List<double> tabletTakeoffForce = new List<double>();

            List<double> preCompMaxs = new List<double>();
            List<double> mainCompMaxs = new List<double>();
            List<double> ejectForceMaxs = new List<double>();
            List<double> tabletTakeoffForceMaxs = new List<double>();

            //TODO: Remove this random when we add TabletTakeOff
            //Random randomDouble = new Random();

            //Data Dequeue
            //TODO: There has to be a better way using the sender to handle this, but this seems to work for now.
            List<StationData> data = new List<StationData>();
            data = Session.DaqData.Dequeue();

            if ((data.Count() > 0) && (data != null))
            {
                //TODO: This is going to need to be changed when we actually implement the station mask.
                int index = 0;
                foreach (StationData station in data)
                {
                    if (this.StationMask[station.ID - 1] == true)
                    {
                        this.ChannelOneChartValues.AddRange(station.Channels[0].Values);
                        this.ChannelTwoChartValues.AddRange(station.Channels[1].Values);
                        this.ChannelThreeChartValues.AddRange(station.Channels[2].Values);
                        this.ChannelFourChartValues.AddRange(station.Channels[3].Values);
                        this.ChannelFiveChartValues.AddRange(station.Channels[4].Values);
                        this.ChannelSixChartValues.AddRange(station.Channels[5].Values);
                        this.ChannelSevenChartValues.AddRange(station.Channels[6].Values);
                        this.ChannelEightChartValues.AddRange(station.Channels[7].Values);
                        this.ChannelNineChartValues.AddRange(station.Channels[8].Values);
                        this.ChannelTenChartValues.AddRange(station.Channels[9].Values);

                        preCompMaxs.Add(station.Channels[0].Values.Max());
                        mainCompMaxs.Add(station.Channels[1].Values.Max());
                        ejectForceMaxs.Add(station.Channels[2].Values.Max());
                        tabletTakeoffForceMaxs.Add(station.Channels[3].Values.Max());

                        System.Windows.Application.Current.Dispatcher.Invoke((Action)delegate
                        {
                            this.StationDetailData[index].StationID = station.ID;
                            this.StationDetailData[index].MaxChannelOne = station.Channels[0].Values.Max();
                            this.StationDetailData[index].MaxChannelTwo = station.Channels[1].Values.Max();
                            this.StationDetailData[index].MaxChannelThree = station.Channels[2].Values.Max();
                            this.StationDetailData[index].MaxChannelFour = station.Channels[3].Values.Max();
                            this.StationDetailData[index].MaxChannelFive = station.Channels[4].Values.Max();
                            this.StationDetailData[index].MaxChannelSix = station.Channels[5].Values.Max();
                            this.StationDetailData[index].MaxChannelSeven = station.Channels[6].Values.Max();
                            this.StationDetailData[index].MaxChannelEight = station.Channels[7].Values.Max();
                            this.StationDetailData[index].MaxChannelNine = station.Channels[8].Values.Max();
                            this.StationDetailData[index].MaxChannelTen = station.Channels[9].Values.Max();


                        });

                       

                        if (this.SampleIsActive)
                        {
                            Sample sampleToAdd = new Sample()
                            {
                                //Station = station.ID,
                                //Revolution = station.RevCount,
                                //MaxPreComp = station.Channels[0].Values.Max(),
                                //MaxMain = station.Channels[1].Values.Max(),
                                //MaxEject = station.Channels[2].Values.Max(),
                                //MaxTabletTakeoff = station.Channels[3].Values.Max(),
                                //StudyId = this.ActiveStudy.Id,
                                //SampleDate = DateTime.UtcNow,
                                //TurretRPM = this.Session.TurretStatusSpeedPV.Value,
                                //FeederRPM = this.Session.FeederStatusSpeedPV.Value
                            };
                            // Tablet Takeoff override
                            //if (!Properties.Settings.Default.TabletTakeoffInstalled)
                            //{
                            //    sampleToAdd.MaxTabletTakeoff = 0;
                            //}
                            //// ejection data override
                            //if (!Properties.Settings.Default.EjectionMonitoringInstalled)
                            //{
                            //    sampleToAdd.MaxEject = 0;
                            //}
                            

                            

                            

                            //Add new Sample to DB
                           // this._sampleService.AddSampleToDB(sampleToAdd);


                            //this.ActiveSampleCount++;
                        }

                        index++;
                    }
                }

                //Calculate and assign the "Average" Dosing, Pre-Thickness, and Main Thickness, but only if the servos aren't enabled.
                //If the servos are enabled, then just read the direct EthernetIP value.
                

                //Update Static Display Items
                //this.PreCompAvg = (preCompression.Count() > 0) ? preCompMaxs.Average() : 0.0;
                //this.MainCompAvg = (mainCompression.Count() > 0) ? mainCompMaxs.Average() : 0.0;
                //this.EjectionAvg = (ejectionForce.Count() > 0) ? ejectForceMaxs.Average() : 0.0;
                //this.TabletTakeoffAvg = (tabletTakeoffForce.Count() > 0) ? tabletTakeoffForceMaxs.Average() : 0.0;


                //Grab the highest revolution number
                //this.ActiveRevolutionCount = data.Max(x => x.RevCount);

                //Clean
                preCompMaxs.Clear();
                mainCompMaxs.Clear();
                ejectForceMaxs.Clear();
                tabletTakeoffForceMaxs.Clear();

                //Clear Chart
                //this.PreCompressionChartData.Clear();
                //this.MainCompressionChartData.Clear();
                //this.EjectionForceChartData.Clear();

                //Fire the chart re-draw event, with decimation.            
                //this.PreCompressionChartData.AddRange(this.Downsample(preCompression.ToArray(), decimation));
                //this.MainCompressionChartData.AddRange(this.Downsample(mainCompression.ToArray(), decimation));
                //this.EjectionForceChartData.AddRange(this.Downsample(ejectionForce.ToArray(), decimation));

            }
        }

        private bool connectToPMX()
        {


            return false;
        }
    }
}
