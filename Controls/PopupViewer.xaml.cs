﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using NatoliBlackBox.Popups.Models;

namespace NatoliBlackBox.Controls
{
    public partial class PopupViewer : Grid
    {
        private readonly Dictionary<PopupControlBase, PopupHost> _hostMap = new Dictionary<PopupControlBase, PopupHost>();
        private static readonly List<(PopupViewer viewer, object popups)> _instanceMap = new List<(PopupViewer, object)>();

        public PopupViewer()
        {
            InitializeComponent();
        }

        public IList<PopupControlBase> Popups
        {
            get { return (IList<PopupControlBase>)this.GetValue(PopupsProperty); }
            set { this.SetValue(PopupsProperty, value); }
        }

        public static readonly DependencyProperty PopupsProperty =
            DependencyProperty.Register("Popups", typeof(IList<PopupControlBase>), typeof(PopupViewer), new PropertyMetadata(null, PopupViewer.CollectionInstanceChanged));

        private static void CollectionInstanceChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (!(sender is PopupViewer popupViewer))
                return;
            if (e.OldValue is INotifyCollectionChanged oldCollection && !(oldCollection is null))
                oldCollection.CollectionChanged -= PopupViewer.PopupCollectionChanged;

            if (!(e.NewValue is IList<PopupControlBase> newCollection) || newCollection is null)
                return;

            popupViewer._hostMap.Clear();
            for (var i = 0; i < newCollection.Count; i++)
            {
                popupViewer.AddPopup(newCollection[i]);
            }
            popupViewer.UpdateZIndices();

            if (!(newCollection is INotifyCollectionChanged collection))
                return;

            PopupViewer._instanceMap.Add((popupViewer, collection));
            collection.CollectionChanged += PopupViewer.PopupCollectionChanged;
        }

        private static void PopupCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            foreach (var viewer in PopupViewer._instanceMap.Where(i => object.ReferenceEquals(i.popups, sender)).Select(i => i.viewer))
            {
                switch (e.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                        var newItems = e.NewItems?.OfType<PopupControlBase>();
                        if (newItems is null) break;
                        foreach (var newPopup in newItems)
                            viewer.AddPopup(newPopup);
                        break;
                    case NotifyCollectionChangedAction.Remove:
                        var oldItems = e.OldItems?.OfType<PopupControlBase>();
                        if (oldItems is null) break;
                        foreach (var oldPopup in oldItems)
                            viewer.RemovePopup(oldPopup);
                        break;
                    case NotifyCollectionChangedAction.Replace:
                        viewer.RemovePopup(e.OldItems[0] as PopupControlBase);
                        viewer.AddPopup(e.NewItems[0] as PopupControlBase);
                        break;
                    case NotifyCollectionChangedAction.Move:
                    default:
                        break;
                }
                viewer.UpdateZIndices();
            }
        }

        private void AddPopup(PopupControlBase popup)
        {
            var host = new PopupHost(popup);
            this._hostMap.Add(popup, host);
            this.Children.Add(host);
        }

        private void RemovePopup(PopupControlBase popup)
        {
            if (this._hostMap.TryGetValue(popup, out var host))
            {
                this._hostMap.Remove(popup);
                this.Children.Remove(host);
            }
        }

        private void UpdateZIndices()
        {
            if (this.Popups is null)
                return;

            for (var i = 0; i < this.Popups.Count; i++)
            {
                if (this._hostMap.TryGetValue(this.Popups[i], out var host))
                    Panel.SetZIndex(host, i);
            }
        }
    }
}
