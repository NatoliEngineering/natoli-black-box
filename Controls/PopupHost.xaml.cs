﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NatoliBlackBox.Popups.Models;

namespace NatoliBlackBox.Controls
{
    public partial class PopupHost : Canvas
    {

        private readonly PopupControlBase _popup;

        private Point? _mouseDragStartingOffset = null;
        private Point _popupLocation;
        private volatile bool _isInflated = false;

        public PopupHost(PopupControlBase popup)
        {
            this._popup = popup;
            this.Children.Add(this._popup);

            InitializeComponent();
        }

        // IsMovable Attached Property
        public static bool GetIsMovable(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsMovableProperty);
        }

        public static void SetIsMovable(DependencyObject obj, bool value)
        {
            obj.SetValue(IsMovableProperty, value);
        }

        public static readonly DependencyProperty IsMovableProperty =
            DependencyProperty.RegisterAttached("IsMovable", typeof(bool), typeof(PopupHost), new PropertyMetadata(true));

        // IsBackgroundClosable Attached Property
        public static ICommand GetBackgroundClickCommand(DependencyObject obj)
        {
            return (ICommand)obj.GetValue(BackgroundClickCommandProperty);
        }

        public static void SetBackgroundClickCommand(DependencyObject obj, ICommand value)
        {
            obj.SetValue(BackgroundClickCommandProperty, value);
        }

        public static readonly DependencyProperty BackgroundClickCommandProperty =
            DependencyProperty.RegisterAttached("BackgroundClickCommand", typeof(ICommand), typeof(PopupHost), new PropertyMetadata(null));

        private void Canvas_MouseButton(object sender, MouseButtonEventArgs e)
        {
            if (e.ButtonState == MouseButtonState.Pressed)
            {
                if (!this._popup.IsMouseOver)
                {
                    var backgroundClickCommand = PopupHost.GetBackgroundClickCommand(this._popup);
                    if (!(backgroundClickCommand is null) && !this._popup.IsMouseOver)
                        backgroundClickCommand.Execute(null);
                    else return;
                }

                if (!PopupHost.GetIsMovable(this._popup))
                    return;

                var offset = e.GetPosition(this);
                offset.X -= this._popupLocation.X;
                offset.Y -= this._popupLocation.Y;
                this._mouseDragStartingOffset = offset;
            }
            else
            {
                this._mouseDragStartingOffset = null;
            }
        }

        private void Canvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (this._mouseDragStartingOffset.HasValue)
            {
                var newPosition = e.GetPosition(this);
                this._popupLocation.X = newPosition.X - this._mouseDragStartingOffset.Value.X;
                this._popupLocation.Y = newPosition.Y - this._mouseDragStartingOffset.Value.Y;
                if (this._popupLocation.X < 0) this._popupLocation.X = 0;
                if (this._popupLocation.Y < 0) this._popupLocation.Y = 0;
                if (this._popupLocation.X + this._popup.ActualWidth > this.ActualWidth) this._popupLocation.X = this.ActualWidth - this._popup.ActualWidth;
                if (this._popupLocation.Y + this._popup.ActualHeight > this.ActualHeight) this._popupLocation.Y = this.ActualHeight - this._popup.ActualHeight;
                Canvas.SetLeft(this._popup, this._popupLocation.X);
                Canvas.SetTop(this._popup, this._popupLocation.Y);
            }
        }

        private void Canvas_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (!this._isInflated)
            {
                this._isInflated = true;
                this.CenterPopup();
            }
        }

        private void CenterPopup()
        {
            // Center the popup initially
            this._popupLocation = new Point((this.ActualWidth - this._popup.ActualWidth) / 2.0, (this.ActualHeight - this._popup.ActualHeight) / 2.0);
            Canvas.SetLeft(this._popup, this._popupLocation.X);
            Canvas.SetTop(this._popup, this._popupLocation.Y);
        }

        private void Canvas_MouseLeave(object sender, MouseEventArgs e)
        {
            this._mouseDragStartingOffset = null;
        }
    }
}
