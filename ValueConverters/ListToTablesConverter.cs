﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Windows.Data;
using System.Linq;
using static System.Windows.Visibility;

namespace NatoliBlackBox.ValueConverters
{
    public class ListToTablesConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                var list = value as List<List<string>>;
                var headers = list.First();
                DataTable dt = new DataTable();

                foreach (string header in headers)
                {
                    dt.Columns.Add(header);
                }
                foreach (List<string> row in list.Skip(1))
                {
                    int index = 0;
                    DataRow r = dt.NewRow();
                    foreach (string col in row)
                    {
                        r[index++] = col;
                    }
                    dt.Rows.Add(r);
                }
                return dt;
            }
            catch
            {
                return value;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
