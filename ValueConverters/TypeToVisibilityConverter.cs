﻿using System;
using System.Globalization;
using System.Windows.Data;
using static System.Windows.Visibility;

namespace NatoliBlackBox.ValueConverters
{
    [ValueConversion(typeof(object), typeof(System.Windows.Visibility))]
    public class TypeToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && parameter is Type)
            {
                if (value.GetType() == (Type)parameter)
                {
                    return Visible;
                }
            }
            return Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
