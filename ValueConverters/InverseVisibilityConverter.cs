﻿using System;
using System.Globalization;
using System.Windows.Data;
using static System.Windows.Visibility;

namespace NatoliBlackBox.ValueConverters
{
    [ValueConversion(typeof(bool), typeof(System.Windows.Visibility))]
    public class InverseVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((bool)value)
            {
                return Hidden;
            }
            return Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
