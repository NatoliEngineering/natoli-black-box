﻿using NatoliBlackBox.Services;

namespace NatoliBlackBox.Popups.Models
{
    public abstract class PopupViewModelBase : ViewModelBase
    {
        protected PopupViewModelBase(SessionService session, UiObjectService uiService) : base(session, uiService)
        {
        }
    }
}
