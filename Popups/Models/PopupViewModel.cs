﻿using System;
using System.Windows.Input;
using Dmc.Wpf.Commands;
//using Dmc.Wpf.Interfaces;
using NatoliBlackBox.Services;

namespace NatoliBlackBox.Popups.Models
{
    public abstract class PopupViewModel<TResult> : PopupViewModelBase where TResult : IPopupResult
    {
        protected Action<TResult> CloseCallback;

        public ICommand ClosePopupCommand { get; }

        protected PopupViewModel(SessionService session, UiObjectService uiService) : base(session, uiService)
        {
            this.ClosePopupCommand = new RelayCommand(a => this.ClosePopup());
        }

        internal virtual void Load(Action<TResult> closeCallback, object loadConfiguration = null)
        {
            this.CloseCallback = closeCallback;
            this.Load(loadConfiguration);
        }

        protected void ClosePopup(TResult result = default(TResult))
        {
            this.CloseCallback?.Invoke(result);
        }
    }
}
