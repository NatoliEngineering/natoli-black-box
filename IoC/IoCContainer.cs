﻿using System;
using System.Linq;
using System.Reflection;
using Autofac;

using NatoliBlackBox.Services;
using NatoliBlackBox.Windows;
using NatoliAnalytical.Base.Interfaces;
using NatoliAnalytical.DaqModule;
using NatoliBlackBox.Popups.PopupResults;
//using NatoliBlackBox.Popups.Views;
using NatoliBlackBox.Popups.Models;
using NatoliBlackBox.Screens.Models;
using NatoliAnalytical.Data.SQL;


namespace NatoliBlackBox.IoC
{
    public static class IoCContainer
    {
        private static IContainer _container;

        public static void Build()
        {
            if (_container == null)
            {
                var builder = new ContainerBuilder();

                // Register components/services
               // builder.RegisterType<AuthService>().AsSelf().SingleInstance();
                builder.RegisterType<SessionService>().SingleInstance();
                builder.RegisterType<UiObjectService>().SingleInstance();
                builder.RegisterType<NavigationService>().SingleInstance();
                builder.RegisterType<PmxDataService>().AsSelf().SingleInstance();
               // builder.RegisterType<TagDataService>().AsSelf().SingleInstance();

                //builder.RegisterType<AverageService>().AsSelf().SingleInstance();
                //builder.RegisterType<FormulaService>().AsSelf().SingleInstance();
                //builder.RegisterType<ProjectService>().AsSelf().SingleInstance();
                //builder.RegisterType<SampleService>().AsSelf().SingleInstance();
                //builder.RegisterType<SeedService>().AsSelf().SingleInstance();
                //builder.RegisterType<StudyService>().AsSelf().SingleInstance();
                //builder.RegisterType<TabletService>().AsSelf().SingleInstance();
                //builder.RegisterType<ToolService>().AsSelf().SingleInstance();
                builder.RegisterType<CalculationService>().AsSelf().SingleInstance();
                //builder.RegisterType<UserService>().AsSelf().SingleInstance();

                // Register NatoliDbContext
                builder.RegisterType<NatoliDbContext>().AsSelf().SingleInstance();
                // Register windows explicitly
                builder.RegisterType<MainWindow>().SingleInstance();

                var uiAssemblyTypes = Assembly.GetAssembly(typeof(App)).GetTypes();

                // Register view models
                builder.RegisterTypes(uiAssemblyTypes.Where(t => typeof(MainWindowViewModel).IsAssignableFrom(t) && !t.IsAbstract).ToArray())
                       .SingleInstance();
                builder.RegisterTypes(uiAssemblyTypes.Where(t => typeof(ScreenViewModel).IsAssignableFrom(t) && !t.IsAbstract).ToArray())
                       .SingleInstance();
                builder.RegisterTypes(uiAssemblyTypes.Where(t => typeof(PopupViewModelBase).IsAssignableFrom(t) && !t.IsAbstract).ToArray())
                       .InstancePerDependency();

                // Register Screens
                var screenTypes = uiAssemblyTypes.Where(t => typeof(ScreenControlBase).IsAssignableFrom(t)).ToArray();
                builder.RegisterTypes(screenTypes).SingleInstance();

                // Register Popups
                builder.RegisterTypes(uiAssemblyTypes.Where(t => typeof(PopupControlBase).IsAssignableFrom(t)).ToArray())
                       .InstancePerDependency();

                _container = builder.Build();
            }
        }

        public static object Resolve(Type type)
        {
            return _container.Resolve(type);
        }

        public static T Resolve<T>()
        {
            return _container.Resolve<T>();
        }

        public static ILifetimeScope BeginLifetimeScope()
        {
            return _container.BeginLifetimeScope();
        }
    }
}
