﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NatoliAnalytical.Base.Data;

namespace NatoliAnalytical.Base.Interfaces
{
    public interface ITagEngine
    {
        //Connect to local Tag Service
        bool Connect();

        //Configure Tags, and Init Data
        void Configure(List<TagConfiguration> tagConfiguration);

        //Read Tags
        object ReadTag(string tagName);

        //Write Tags
        void WriteTag(string tagName, object value);

        //Close connection tag service
        bool Disconnect();
    }
}
