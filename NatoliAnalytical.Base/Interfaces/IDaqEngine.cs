﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NatoliAnalytical.Base.Data;

namespace NatoliAnalytical.Base.Interfaces
{
    public interface IDaqEngine
    {
        //Device API
        void Connect(string ipaddr);
        void Configure(int stationcount, List<ChannelData> activeChannels);
        void Start(int startingRev , List<ChannelData> activeChannels, DataQueue<List<StationData>> dataQueue);
        void Read(List<ChannelData> activeChannels);
        void Stop();
        void Disconnect();     

    }
}
