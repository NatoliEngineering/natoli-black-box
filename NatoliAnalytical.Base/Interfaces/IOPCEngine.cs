﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatoliAnalytical.Base.Interfaces
{
    public interface IOPCEngine
    {
        //Connect to local OPC Service
        void Connect();

        //Configure OPC Tags, and Init Data
        void Configure(List<string> tags);

        //Read Tags
        object ReadTag(string tagName);

        object[] ReadGroup(string groupName);

        //Write Tags
        void WriteTag(string tagName, object value);

        //Close connection to local OPC Server
        void Disconnect();
    }
}
