﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatoliAnalytical.Base.Data
{
    public class ChannelData:ICloneable
    {
        //Class Members
        public string Name { get; set; }
        public int ChannelNumber { get; set; }
        public double OffsetDegree { get; set; }
        public string Timestamp { get; set; }
        public List<double> Values { get; set; }
        public List<double> Positions { get; set; }

        //Constructor
        public ChannelData(string name, int channelNumber, double offset)
        {
            this.Name = name;
            this.ChannelNumber = channelNumber;
            this.OffsetDegree = offset;
            this.Timestamp = null;
            this.Values = new List<double>();
            this.Positions = new List<double>();
        }

        public object Clone()
        {
            return (ChannelData)this.MemberwiseClone();
        }

        // clear Data
        public void Clear()
        {
            this.Values.Clear();
            this.Positions.Clear();
        }
    }
}
