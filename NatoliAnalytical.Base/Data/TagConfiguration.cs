﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatoliAnalytical.Base.Data
{
    public class TagConfiguration
    {
        public string TagName { get; set; }
        public string Alias { get; set; }
        public string HMIVariableName { get; set; }
        public bool Poll { get; set; }
        public string PLCType { get; set; }
    }
}
