﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatoliAnalytical.Base.Data
{
    public class StationDetail : INotifyPropertyChanged
    {
        private int _StationID;
        private double _MaxChannelOne;
        private double _MaxChannelTwo;
        private double _MaxChannelThree;
        private double _MaxChannelFour;
        private double _MaxChannelFive;
        private double _MaxChannelSix;
        private double _MaxChannelSeven;
        private double _MaxChannelEight;
        private double _MaxChannelNine;
        private double _MaxChannelTen;

        public event PropertyChangedEventHandler PropertyChanged;

        #region Properties
        public int StationID
        {
            get
            {
                return _StationID;
            }
            set
            {
                _StationID = value;
                RaisePropertyChanged("StationID");
            }
        }
        public double MaxChannelOne
        {
            get
            {
                return _MaxChannelOne;
            }
            set
            {
                _MaxChannelOne = value;
                RaisePropertyChanged("MaxChannelOne");
            }
        }
        public double MaxChannelTwo
        {
            get
            {
                return _MaxChannelTwo;
            }
            set
            {
                _MaxChannelTwo = value;
                RaisePropertyChanged("MaxChannelTwo");
            }
        }
        public double MaxChannelThree
        {
            get
            {
                return _MaxChannelThree;
            }
            set
            {
                _MaxChannelThree = value;
                RaisePropertyChanged("MaxChannelThree");
            }
        }
        public double MaxChannelFour
        {
            get { return _MaxChannelFour; }
            set
            {
                _MaxChannelFour = value;
                RaisePropertyChanged("MaxChannelFour");
            }
        }
        public double MaxChannelFive
        {
            get
            {
                return _MaxChannelFive;
            }
            set
            {
                _MaxChannelFive = value;
                RaisePropertyChanged("MaxChannelFive");
            }
        }
        public double MaxChannelSix
        {
            get
            {
                return _MaxChannelSix;
            }
            set
            {
                _MaxChannelSix = value;
                RaisePropertyChanged("MaxChannelSix");
            }
        }
        public double MaxChannelSeven
        {
            get
            {
                return _MaxChannelSeven;
            }
            set
            {
                _MaxChannelSeven = value;
                RaisePropertyChanged("MaxChannelSeven");
            }
        }
        public double MaxChannelEight
        {
            get { return _MaxChannelEight; }
            set
            {
                _MaxChannelEight = value;
                RaisePropertyChanged("MaxChannelEight");
            }
        }

        public double MaxChannelNine
        {
            get { return _MaxChannelNine; }
            set
            {
                _MaxChannelNine = value;
                RaisePropertyChanged("MaxChannelNine");
            }
        }

        public double MaxChannelTen
        {
            get { return _MaxChannelTen; }
            set
            {
                _MaxChannelTen = value;
                RaisePropertyChanged("MaxChannelTen");
            }
        }

        #endregion

        /// Property Changed Notification      
        public void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
