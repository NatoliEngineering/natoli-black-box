﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatoliAnalytical.Base.Data
{
    public class StationData : EventArgs
    {
        //Class Members
        public int ID { get; private set; }        
        public List<ChannelData> Channels { get; set; }

        // thickness averages at rollers
        //public double Dosing;
        //public double PreThickness;
        //public double MainThickness;

        public int RevCount { get; set; }

        //Constructor
        public StationData(int index, List<ChannelData> channelDatas)
        {
            //Set Station ID
            this.ID = index + 1;

            //Add channel names to station on create.
            #region Channel Setup
            this.Channels = new List<ChannelData>();
            //string[] names = {"ChannelOne", "ChannelTwo", "ChannelThree", "ChannelFour", "ChannelFive", "ChannelSix", "ChannelSeven", "ChannelEight", "ChannelNine", "ChannelTen"};

            

            this.Channels.AddRange(channelDatas);



            #endregion

        }

        public void ClearChannels()
        {
            foreach (ChannelData Channel in this.Channels)
            {
                Channel.Clear();
            }
        }
    }

    
}
