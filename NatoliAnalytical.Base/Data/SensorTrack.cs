﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatoliAnalytical.Base.Data
{
    public class SensorTrack
    {
        public int SensorID;
        public int CurrentStation;
        public int PreviousStation;
        public double OffsetDegree;
    }
}
