﻿//System Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

//HBM Specific Using Statements
using Hbm.Api.Common.Entities.Signals;

//Custom
using NatoliAnalytical.Base.Data;

namespace NatoliAnalytical.DaqModule.DataProcessing
{
    public class PostProcessor
    {        
        // -- Class Data -- //
        private bool    _firstCall;
        private double  _currentRevCount;
        private double  _startRevCount;

        private double _sensor1_previousStationNumber;
        private double _sensor2_previousStationNumber;
        private double _sensor3_previousStationNumber;
        private double _sensor4_previousStationNumber;

        private double _sensor5_previousStationNumber;
        private double _sensor6_previousStationNumber;
        private double _sensor7_previousStationNumber;
        private double _sensor8_previousStationNumber;

        private double _sensor1_currentStationNumber;
        private double _sensor2_currentStationNumber;
        private double _sensor3_currentStationNumber;
        private double _sensor4_currentStationNumber;

        private double _sensor5_currentStationNumber;
        private double _sensor6_currentStationNumber;
        private double _sensor7_currentStationNumber;
        private double _sensor8_currentStationNumber;

        private double _offset_sensor1;
        private double _offset_sensor2;
        private double _offset_sensor3;
        private double _offset_sensor4;
        private double _offset_sensor5;
        private double _offset_sensor6;
        private double _offset_sensor7;
        private double _offset_sensor8;

        private double _stationWidth;
        private int    _numStations;
        private int _signalCount;

        private List<SensorTrack> _sensorTracks;
        private List<StationData> _buffer; // circular buffer
        private StationData _stationToAdd;

        private readonly DataQueue<List<StationData>> _dataQueue;

        // -- Constructor -- //
        public PostProcessor(int numStations, int startingRevolutionCount, List<ChannelData> activeChannels, DataQueue<List<StationData>> dataQueue)
        {
            //Data transfer queue
            this._dataQueue = dataQueue;

            //Initialize processing windows for the run, based on configuration
            this._numStations = numStations;
            this._stationWidth = (360.0 / (double) numStations);

            this._signalCount = activeChannels.Count();

            this._sensorTracks = new List<SensorTrack>();

            // These will be input by user in Degrees from 0 ( where 0 is station one at the home prox )           
            foreach (ChannelData c in activeChannels)
            {
                SensorTrack sensorTrack = new SensorTrack
                {
                    OffsetDegree = c.OffsetDegree,
                    SensorID = c.ChannelNumber
                };
            }            

            //Initialize the buffer
            this._buffer = new List<StationData>();

            //Initialize Data
            this._sensor1_previousStationNumber   = -1;
            this._sensor3_previousStationNumber     = 0;
            this._sensor4_previousStationNumber = 0;
            this._sensor1_currentStationNumber    = 0;
            this._sensor2_currentStationNumber   = 0;
            this._sensor3_currentStationNumber      = 0;
            this._sensor4_currentStationNumber = 0;
            this._firstCall = true;

            //Initialize revolution tracking
            this._startRevCount = startingRevolutionCount;
            this._currentRevCount = startingRevolutionCount;


        }
        //("Side One Main Compression");
        //("Side One Pre Compression");
        //("Side One Ejection");
        //("Side One Tablet Take Off");

        //("Side Two Main Compression");
        //("Side Two Pre Compression");
        //("Side Two Ejection");
        //("Side Two Tablet Take Off");

        //("Prox Switch");

        //("Main Compression");
        //("Pre Compression");
        //("Ejection");
        //("Tablet Take Off");

        //("Prox Switch");
       
        public void Process(List<Signal> signals, List<ChannelData> activeChannels, double elapsedTime, bool proxVisibile, bool simulation = false)
        {
            // iterate rev based on Timer Inputs
            if(elapsedTime == 0 && !this._firstCall)
            {
                this._currentRevCount++;
            }

            foreach (ChannelData c in activeChannels)
            {
                SensorTrack sensorTrack = new SensorTrack
                {
                    OffsetDegree = c.OffsetDegree,
                    SensorID = c.ChannelNumber
                };
                this._sensorTracks.Add(sensorTrack);
            }

            int lastSensor = this._sensorTracks.Count - 1;

            //Manage the buffer
            int i = 0; // incremented at bottom of foreach loop
            foreach (double s in activeChannels[0].Values)
            {
                if (!proxVisibile)
                {
                    foreach (SensorTrack t in _sensorTracks)
                    {
                        //t.PreviousStation = t.CurrentStation;
                        t.CurrentStation = CalculateStationUnderMeasurement(elapsedTime, 10000, this._numStations, t.OffsetDegree);
                    }

                    // establish first station

                    //Add a station for each new station to enter precompression.
                    if ((this._sensorTracks[0].CurrentStation != this._sensorTracks[0].PreviousStation) | (this._firstCall == true))
                    {
                        //Data acquired for rev number changes at pre-comp change
                        if ((this._sensorTracks[0].CurrentStation == 0) && (this._firstCall != true))
                        {
                            this._currentRevCount++;
                        }

                        this._stationToAdd = new StationData(Convert.ToInt32(this._sensorTracks[0].CurrentStation), activeChannels)
                        {
                            RevCount = Convert.ToInt32(this._currentRevCount)
                        };

                        this._buffer.Add(this._stationToAdd);

                    }

                    //Handle the first call to processing.
                    if (this._firstCall)
                    {
                        this._sensorTracks[lastSensor].PreviousStation = this._sensorTracks[lastSensor].CurrentStation;
                        this._sensorTracks[0].PreviousStation = this._sensorTracks[0].CurrentStation;
                        this._firstCall = false; // oneshot
                    }

                    foreach (SensorTrack sensor in _sensorTracks)
                    {
                        string stationName = activeChannels.FirstOrDefault(a => a.ChannelNumber.Equals(sensor.SensorID)).Name;

                        StationData stationData = this._buffer.Where(x => (x.ID - 1) == sensor.CurrentStation).LastOrDefault();

                        if(stationData != null)
                        {
                            stationData.Channels.FirstOrDefault(a => a.Name.Equals(activeChannels.FirstOrDefault(b => b.ChannelNumber.Equals(sensor.SensorID)).Name)).Values.Add(i);
                            stationData.Channels.FirstOrDefault(a => a.Name.Equals(activeChannels.FirstOrDefault(b => b.ChannelNumber.Equals(sensor.SensorID)).Name)).Positions.Add(s);
                        }                   
                    }
                    
                    //StationData preCompStation = this._buffer.Where(x => (x.ID - 1) == this._sensor1_currentStationNumber).LastOrDefault();
                    //if ((preCompStation != null) && (preCompStation.RevCount > this._startRevCount))
                    //{
                    //    // (from above) 0: PreCompression
                    //    preCompStation.Channels.FirstOrDefault(a => a.Name.Equals("Pre Compression")).Values.Add(i);
                    //    preCompStation.Channels.FirstOrDefault(a => a.Name.Equals("Pre Compression")).Positions.Add(s);
                    //}

                    //StationData mainCompStation = this._buffer.Where(x => (x.ID - 1) == this._sensor2_currentStationNumber).LastOrDefault();
                    //if ((mainCompStation != null) && (mainCompStation.RevCount > this._startRevCount))
                    //{
                    //    // (from above) 1: MainCompression
                    //    mainCompStation.Channels.FirstOrDefault(a => a.Name.Equals("Main Compression")).Values.Add(i);
                    //    mainCompStation.Channels.FirstOrDefault(a => a.Name.Equals("Main Compression")).Positions.Add(s);
                    //}

                    //StationData ejectStation = this._buffer.Where(x => (x.ID - 1) == this._sensor3_currentStationNumber).LastOrDefault();
                    //if ((ejectStation != null) && (ejectStation.RevCount > this._startRevCount))
                    //{
                    //    // (from above) 2: EjectionForce
                    //    ejectStation.Channels.FirstOrDefault(a => a.Name.Equals("Ejection")).Values.Add(i);
                    //    ejectStation.Channels.FirstOrDefault(a => a.Name.Equals("Ejection")).Positions.Add(s);

                    //}

                    //StationData tabletTakeoffStation = this._buffer.Where(x => (x.ID - 1) == this._sensor4_currentStationNumber).LastOrDefault();
                    //if ((tabletTakeoffStation != null) && (tabletTakeoffStation.RevCount > this._startRevCount))
                    //{
                    //    // 3: Tablet Take off
                    //    tabletTakeoffStation.Channels.FirstOrDefault(a => a.Name.Equals("Tablet Take Off")).Values.Add(i);
                    //    tabletTakeoffStation.Channels.FirstOrDefault(a => a.Name.Equals("Tablet Take Off")).Positions.Add(s);

                    //}

                    //Update previous station.
                    //this._sensor4_previousStationNumber = this._sensor4_currentStationNumber;
                    //this._sensor1_previousStationNumber = this._sensor1_currentStationNumber;

                    this._sensorTracks[0].PreviousStation = this._sensorTracks[0].CurrentStation;
                    this._sensorTracks[lastSensor].PreviousStation = this._sensorTracks[lastSensor].CurrentStation;
                    
                }
                else
                {
                    //send 'er off!
                    //NOTE: only run when all rev 0 stations are gone - no more empty stations
                    if (this._buffer.Where(x => x.RevCount == this._startRevCount).Count() == 0)
                    {
                        this.StationStats(this._buffer.GetRange(0, this._numStations));
                    }

                    // remove from buffer
                    this._buffer.RemoveRange(0, (this._buffer.FindIndex(x => x.ID == this._numStations) + 1));
                }
                i++;
            }

            this._sensorTracks.Clear();

            ////Manage the buffer
            //int i = 0; // incremented at bottom of foreach loop
            //foreach (double s in activeChannels[0].Values)
            //{

            //    foreach(SensorTrack t in _sensorTracks)
            //    {
            //        t.PreviousStation = t.CurrentStation;
            //        t.CurrentStation = CalculateStationUnderMeasurement(elapsedTime, 10000, this._numStations, t.OffsetDegree);
            //    }

                               
            //    // establish first station

            //    //Add a station for each new station to enter precompression.
            //    if ((this._sensorTracks[0].CurrentStation != this._sensorTracks[0].PreviousStation) | (this._firstCall == true))
            //    {
            //        //Data acquired for rev number changes at pre-comp change
            //        if ((this._sensorTracks[0].CurrentStation == 0) && (this._firstCall != true))
            //        {
            //            this._currentRevCount++;
            //        }

            //        this._stationToAdd = new StationData(Convert.ToInt32(this._sensorTracks[0].CurrentStation), activeChannels)
            //        {
            //            RevCount = Convert.ToInt32(this._currentRevCount)
            //        };

            //        this._buffer.Add(this._stationToAdd);

            //    }

            //    //Handle the first call to processing.
            //    if (this._firstCall)
            //    {
            //        this._sensorTracks[lastSensor].PreviousStation = this._sensorTracks[lastSensor].CurrentStation;
            //        this._sensorTracks[0].PreviousStation = this._sensorTracks[0].CurrentStation;
            //        this._firstCall = false; // oneshot
            //    }


            //    //ONLY SEND DATA OUT WHEN: station index [n-1] finishes Tablet Take off 
            //    if ((this._sensorTracks[lastSensor].CurrentStation == 0) && (this._sensorTracks[lastSensor].CurrentStation != this._sensorTracks[lastSensor].PreviousStation))
            //    {
            //        //send 'er off!
            //        //NOTE: only run when all rev 0 stations are gone - no more empty stations
            //        if (this._buffer.Where(x => x.RevCount == this._startRevCount).Count() == 0)
            //        {
            //            this.StationStats(this._buffer.GetRange(0, this._numStations));
            //        }

            //        // remove from buffer
            //        this._buffer.RemoveRange(0, (this._buffer.FindIndex(x => x.ID == this._numStations) + 1));
            //    }




            //    StationData preCompStation = this._buffer.Where(x => (x.ID - 1) == this._sensor1_currentStationNumber).LastOrDefault();
            //    if ((preCompStation != null) && (preCompStation.RevCount > this._startRevCount))
            //    {
            //        // (from above) 0: PreCompression
            //        preCompStation.Channels.FirstOrDefault(a => a.Name.Equals("Pre Compression")).Values.Add(i);
            //        preCompStation.Channels.FirstOrDefault(a => a.Name.Equals("Pre Compression")).Positions.Add(s);
            //    }

            //    StationData mainCompStation = this._buffer.Where(x => (x.ID - 1) == this._sensor2_currentStationNumber).LastOrDefault();
            //    if ((mainCompStation != null) && (mainCompStation.RevCount > this._startRevCount))
            //    {
            //        // (from above) 1: MainCompression
            //        mainCompStation.Channels.FirstOrDefault(a => a.Name.Equals("Main Compression")).Values.Add(i);
            //        mainCompStation.Channels.FirstOrDefault(a => a.Name.Equals("Main Compression")).Positions.Add(s);
            //    }

            //    StationData ejectStation = this._buffer.Where(x => (x.ID - 1) == this._sensor3_currentStationNumber).LastOrDefault();
            //    if ((ejectStation != null) && (ejectStation.RevCount > this._startRevCount))
            //    {
            //        // (from above) 2: EjectionForce
            //        ejectStation.Channels.FirstOrDefault(a => a.Name.Equals("Ejection")).Values.Add(i);
            //        ejectStation.Channels.FirstOrDefault(a => a.Name.Equals("Ejection")).Positions.Add(s);

            //    }

            //    StationData tabletTakeoffStation = this._buffer.Where(x => (x.ID - 1) == this._sensor4_currentStationNumber).LastOrDefault();
            //    if ((tabletTakeoffStation != null) && (tabletTakeoffStation.RevCount > this._startRevCount))
            //    {
            //        // 3: Tablet Take off
            //        tabletTakeoffStation.Channels.FirstOrDefault(a => a.Name.Equals("Tablet Take Off")).Values.Add(i);
            //        tabletTakeoffStation.Channels.FirstOrDefault(a => a.Name.Equals("Tablet Take Off")).Positions.Add(s);

            //    }

            //    //Update previous station.
            //    this._sensor4_previousStationNumber = this._sensor4_currentStationNumber;
            //    this._sensor1_previousStationNumber = this._sensor1_currentStationNumber;

            //    this._sensorTracks[0].PreviousStation = this._sensorTracks[0].CurrentStation;
            //    this._sensorTracks[lastSensor].PreviousStation = this._sensorTracks[lastSensor].CurrentStation;
            //    i++;
            //}
            //this._sensorTracks.Clear();
        }



        private void StationStats(List<StationData> completedRevolution)
        {
            //Calculate the full station data, for the provided buffer and send to graphing/file IO/Database

            //Send data to display, via queue.
            this._dataQueue.Enqueue(completedRevolution);
        }

        private int GetStationUnderMeasurement(double encoderPosition, double offset)
        {
            //Given any encoder signal: 
            //Calculate which station is under the measurement zone corresponding to the offset provided            
            //find the number of whole stations between current encoder signal and measurement position
            int stationSpan = (int)Math.Round( ((encoderPosition - ((double)offset + 0.5 * this._stationWidth))) / (double)this._stationWidth);

            //If negative span, subtract from total number of stations. 
            //This handles rollover conditions AND negative directions
            int stationUnderMeasurement = (stationSpan >= 0) ? (stationSpan) : (this._numStations + stationSpan);
            
            return stationUnderMeasurement;
        }

        private int CalculateStationUnderMeasurement(double elapsedTime, double lastCompleteRevTime, int totalStationCount, double offset)
        {
            int stationUnderMeasurement = 0;

            double turretSpeed = 360.00 / (lastCompleteRevTime / 1000 );

            double stationwidth = 360.00 / totalStationCount;

            double offsetLow = offset - (0.5 * stationwidth);
            //if(0 > offsetLow)
            //{
            //    offsetLow = 360 + offsetLow;
            //}
            // need to check for negative values
            double offsetHigh = offset + (0.5 * stationwidth);
            if(offsetHigh > 360 )
            {
                offsetHigh = offsetHigh - 360;
            }
            // need to check for values higher than 360

            double turretMotion = (elapsedTime / 1000) * turretSpeed; // position of station 1 


            for(int i = 0; i < totalStationCount; i++)
            {
                double stationPosition = ( i + 1 ) * stationwidth + turretMotion;
                if(stationPosition > 360)
                {
                    stationPosition = stationPosition - 360;
                }

                if(stationPosition >= offsetLow && offsetHigh >= stationPosition)
                {
                    stationUnderMeasurement = i;
                    return stationUnderMeasurement;
                }
            }

            //If negative span, subtract from total number of stations. 
            //This handles rollover conditions AND negative directions
            //stationUnderMeasurement = (stationSpan >= 0) ? (stationSpan) : (this._numStations + stationSpan);

            return -1;
        }

    }
}
