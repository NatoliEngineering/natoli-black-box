﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NatoliAnalytical.DaqModule.DataProcessing;

//HBM
using Hbm.Api.Common.Entities.Signals;
using Hbm.Api.Pmx.Signals;
//Logging
using EventLog = Dmc.Logging.EventLog;

//Custom
using NatoliAnalytical.Base.Interfaces;
using NatoliAnalytical.Base.Data;
using Hbm.Api.Common.Enums;

namespace NatoliAnalytical.DaqModule
{
    public class SimulateDAQEngine:IDaqEngine
    {
        //Class Members
        private List<StationData> _stations;
        private List<Signal> _signals;

        // rand num generator
        private Random rand = new Random(5);
        private double elapsedTimeMS;

        //Post Processor
        private PostProcessor _processor;
        private int _offset = 0;

        //spoof prox sensor
        private bool proxVisible;

        //Constructor
        public SimulateDAQEngine()
        {
            //this._offset = rand.Next(0, 2047);
            this._offset = 1252; // this breaks it!

            this._stations = new List<StationData>();
        }

        #region API Interface
        //Implementing the IDaqEngine interface methods
        public void Connect(string ipaddr)
        {
            //Simulate Connecting to Hardware
            EventLog.LogDebug("Connected to Simulated DAQ Target.");
                      
        }

        public void Configure(int stationCount, List<ChannelData> activeChannels)
        {
            //Simulate setting up signal data
            EventLog.LogDebug("Configured Simulated Stations.");

            //Station Setup
            //this._stations = activeChannels;
            for (int i = 0; i < stationCount; i++)
            {
                this._stations.Add(new StationData(i, activeChannels));
            }

            //Signal Setup
            this._signals = new List<Signal>();
            
            foreach(ChannelData channel in activeChannels)
            {
                this._signals.Add(new PmxAnalogInSignal());
            }
            //this._signals.Add(new PmxAnalogInSignal());
            //this._signals.Add(new PmxAnalogInSignal());
            //this._signals.Add(new PmxAnalogInSignal());
            //this._signals.Add(new PmxAnalogInSignal());
            //this._signals.Add(new PmxAnalogInSignal());
            //this._signals.Add(new PmxAnalogInSignal());
            //this._signals.Add(new PmxAnalogInSignal());
            //this._signals.Add(new PmxAnalogInSignal());
            //this._signals.Add(new PmxAnalogInSignal());
            //this._signals.Add(new PmxAnalogInSignal());

        }

        public void Start(int startingRevolutionCount, List<ChannelData> activeChannels, DataQueue<List<StationData>> dataQueue)
        {
            //Simulate Start Implementation
            //this._processor = new PostProcessor(this._stations.Count(), startingRevolutionCount, preCompMeasurementOffset, mainCompOffset, ejectForceOffset, tabletTakeoffEncoderOffset, dataQueue);
            this._processor = new PostProcessor(this._stations.Count(), startingRevolutionCount, activeChannels, dataQueue);
            EventLog.LogDebug("Simulated PMX started sucessfully.");
            EventLog.LogDebug("Offset Value: {0}", this._offset);
        }

        // simulate Prox Sensor Timing - 
        public double RevTimer(double elaspsedTime)
        {
            // run with reset
            if (elaspsedTime > 10000 )
            {
                elaspsedTime = 0;
                this.proxVisible = true;
            }
            if(elaspsedTime > 0)
            {
                this.proxVisible = false;
            }

            return elaspsedTime;
        }


        public void Read(List<ChannelData> activeChannels)
        {
            //Simulate Read and Process data from the DAQ device
            try
            {                
                double[] channelOneData = new double[250];
                double[] channelTwoData = new double[250];
                double[] channelThreeData = new double[250];
                double[] channelFourData = new double[250];
                double[] channelFiveData = new double[250];
                double[] channelSixData = new double[250];
                double[] channelSevenData = new double[250];
                double[] channelEightData = new double[250];
                double[] channelNineData = new double[250];
                double[] channelTenData = new double[250];

                for (int i = 0; i < channelOneData.Length; i++)
                {
                    // Generate Cyclic Simulated Prox Data               
                    if (this._offset > 359)
                    {
                        this._offset = 0;
                        
                    }
                   
                    channelOneData[i] = this._offset;
                    //Generate singal data, simulated
                    channelTwoData[i] = this._offset + 2; //rand.NextDouble() * 250 * (double)i / 250; //Sim 0-2kN signal
                    channelThreeData[i] = this._offset - 5; //rand.NextDouble() * 12000 * (double)i / 250; //Sim 0-12N signal
                    channelFourData[i] = this._offset + 10; //rand.NextDouble() * 200 * (double)i / 250; //Sim 0-200 N singal
                    channelFiveData[i] = rand.NextDouble() * 200 * (double)i / 250; // sim signal
                    channelSixData[i] = rand.NextDouble() * 1000 * (double)i / 250; //Sim 0-1N signal
                    channelSevenData[i] = rand.NextDouble() * 2000 * (double)i / 250; //Sim 0-2N signal
                    channelEightData[i] = rand.NextDouble() * 3000 * (double)i / 250; //Sim 0-3N signal
                    channelNineData[i] = rand.NextDouble() * 3000 * (double)i / 250; //Sim 0-3N signal
                    channelTenData[i] = rand.NextDouble() * 3000 * (double)i / 250; //Sim 0-3N signal

                    // iterate degree of rotation
                    this._offset++;
                }

                foreach(ChannelData channel in activeChannels)
                {
                    if(channel.ChannelNumber == 1)
                    {
                        this._signals[0].ContinuousMeasurementValues.Values = channelOneData;

                        channel.Values.AddRange(channelOneData);

                     
                    }
                    else if(channel.ChannelNumber == 2)
                    {
                        this._signals[1].ContinuousMeasurementValues.Values = channelTwoData;

                        channel.Values.AddRange(channelTwoData);

                        
                    }
                    else if(channel.ChannelNumber == 3)
                    {
                        this._signals[2].ContinuousMeasurementValues.Values = channelThreeData;

                        channel.Values.AddRange(channelThreeData);

                       
                    }
                    else if(channel.ChannelNumber == 4)
                    {
                        this._signals[3].ContinuousMeasurementValues.Values = channelFourData;

                        channel.Values.AddRange(channelFourData);

                       
                    }
                    else if(channel.ChannelNumber == 5)
                    {
                        this._signals[4].ContinuousMeasurementValues.Values = channelFiveData;

                        channel.Values.AddRange(channelFiveData);

                        
                    }
                    else if(channel.ChannelNumber == 6)
                    {
                        this._signals[5].ContinuousMeasurementValues.Values = channelSixData;

                        channel.Values.AddRange(channelSixData);

                        
                    }
                    else if(channel.ChannelNumber == 7)
                    {
                        this._signals[6].ContinuousMeasurementValues.Values = channelSevenData;

                        channel.Values.AddRange(channelSevenData);

                        
                    }
                    else if(channel.ChannelNumber == 8)
                    {
                        this._signals[7].ContinuousMeasurementValues.Values = channelEightData;

                        channel.Values.AddRange(channelEightData);

                       
                    }
                    else if(channel.ChannelNumber == 9)
                    {
                        this._signals[8].ContinuousMeasurementValues.Values = channelNineData;

                        channel.Values.AddRange(channelNineData);

                       
                    }
                    else if(channel.ChannelNumber == 10)
                    {
                        this._signals[9].ContinuousMeasurementValues.Values = channelTenData;

                        channel.Values.AddRange(channelTenData);

                        
                    }
                }

                this._processor.Process(this._signals, activeChannels, this.elapsedTimeMS, proxVisible, true);

                EventLog.LogTrace("Success Reading Simulated PMX Device.");                
            }
            catch (Exception ex)
            {
                EventLog.LogFatal("Error Reading Simulated PMX Device: " + ex.Message);
            }
            finally
            {
                Thread.Sleep(100);
                this.elapsedTimeMS += 100;
                this.elapsedTimeMS = RevTimer(this.elapsedTimeMS);
            }
        }

        public void Stop()
        {
            //Simulate Stop Implementation           
            EventLog.LogDebug("Simulate Stop PMX Successful.");
        }

        public void Disconnect()
        {
            //Simulate Disconnect Implementation
            EventLog.LogTrace("Simulated PMX target disconnected successful.");
            this._processor = null;
            this._signals.Clear();
            //this._stations.Clear();
        }
        #endregion
       
    }
}
