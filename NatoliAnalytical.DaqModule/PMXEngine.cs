﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

//HBM Using Statements
using Hbm.Api.Common;
using Hbm.Api.Common.Exceptions;
using Hbm.Api.Common.Entities;
using Hbm.Api.Common.Entities.Problems;
using Hbm.Api.Common.Entities.Connectors;
using Hbm.Api.Common.Entities.Channels;
using Hbm.Api.Common.Entities.Signals;
using Hbm.Api.Common.Entities.Filters;
using Hbm.Api.Common.Entities.ConnectionInfos;
using Hbm.Api.Common.Enums;

//PMX Device Sepcific
using Hbm.Api.Pmx;

//Logging
using EventLog = Dmc.Logging.EventLog;

//Custom
using NatoliAnalytical.Base.Interfaces;
using NatoliAnalytical.Base.Data;
using NatoliAnalytical.DaqModule.DataProcessing;

namespace NatoliAnalytical.DaqModule
{
    public class PMXEngine:IDaqEngine
    {
        #region Properties
        //Data Processing
        private PostProcessor _processor;        

        //PMX Device Control     
        private DaqEnvironment _daq;
        private PmxDevice _pmx;
        private DaqMeasurement _session;
        private List<Signal> _signals;        

        //Data Publishing
        private List<StationData> _stations;        
        #endregion

        //Constructor
        public PMXEngine(int numOfStations)
        {
            this._stations = new List<StationData>();
            this._signals = new List<Signal>();
            //this._processor = new PostProcessor(numOfStations, 0, 0, 0, 0, 0, 0, 0, 0, 0, new DataQueue<List<StationData>>());
        }

        #region API Interface
        //Implementing the IDaqEngine interface methods
        public void Connect(string ipaddr)
        {
            //Configure PMX Device connection
            EventLog.LogDebug("Setting up connection to PMX device at: {0}", ipaddr);

            //Init Variables
            var errors = new List<Problem>();
            bool connected = false;

            try
            {
                //Obtain reference to the HBM Environment
                this._daq = DaqEnvironment.GetInstance();

                //Create a PMX Device for connection
                this._pmx = new PmxDevice(ipaddr);

                //Attempt connection
                connected = this._daq.Connect(this._pmx, out errors);                               

                //Check for error
                if (errors.Count() > 0)
                {
                    foreach (Problem err in errors)
                    {
                        EventLog.LogFatal(err.Message);
                    }
                    throw new Exception("Failed to connect to PMX, see log for detailed error.");
                }

                EventLog.LogDebug("Successfully connected to PMX.");
            }
            catch (Exception ex)
            {
                EventLog.LogFatal(ex.Message);
                throw new Exception("General connection failure to PMX Device.", ex);
            }
            
        }

        public void Configure(int stationCount, List<ChannelData> activeChannels)
        {
            //Configure PMX Signals and DAQMeasurment Session            
            try
            {
                //Configure DAQ Session
                EventLog.LogDebug("Setting up station data for {0} stations.", stationCount.ToString());

                //Init Varliables
                var errors = new List<Problem>();

                //Setup Stations                
                for (int i = 0; i < stationCount; i++)
                {
                    //this._stations.Add(new StationData(i));
                }

                //Select physical channels that will be part of sampling                
                //Index - 0 - Channel One
                this._signals.Add(this._pmx.Connectors.SelectMany(c => c.Channels).FirstOrDefault(c => c.Name == "ChannelOne").Signals[0]);
                //Index - 1 - Channel Two
                this._signals.Add(this._pmx.Connectors.SelectMany(c => c.Channels).FirstOrDefault(c => c.Name == "ChannelTwo").Signals[1]);
                //Index - 2 - Channel Three
                this._signals.Add(this._pmx.Connectors.SelectMany(c => c.Channels).FirstOrDefault(c => c.Name == "ChannelThree").Signals[2]);
                //Index - 3 - Channel Four
                this._signals.Add(this._pmx.Connectors.SelectMany(c => c.Channels).FirstOrDefault(c => c.Name == "ChannelFour").Signals[3]);
                //Index - 4 - Channel Five
                this._signals.Add(this._pmx.Connectors.SelectMany(c => c.Channels).FirstOrDefault(c => c.Name == "ChannelFive").Signals[4]);
                //Index - 5 - Channel Six
                this._signals.Add(this._pmx.Connectors.SelectMany(c => c.Channels).FirstOrDefault(c => c.Name == "ChannelSix").Signals[5]);
                //Index - 6 - Channel Seven
                this._signals.Add(this._pmx.Connectors.SelectMany(c => c.Channels).FirstOrDefault(c => c.Name == "ChannelSeven").Signals[6]);
                //Index - 7 - Channel Eight
                this._signals.Add(this._pmx.Connectors.SelectMany(c => c.Channels).FirstOrDefault(c => c.Name == "ChannelEight").Signals[7]);
                //Index - 8 - Channel Nine
                this._signals.Add(this._pmx.Connectors.SelectMany(c => c.Channels).FirstOrDefault(c => c.Name == "ChannelNine").Signals[8]);
                //Index - 9 - Channel Ten
                this._signals.Add(this._pmx.Connectors.SelectMany(c => c.Channels).FirstOrDefault(c => c.Name == "ChannelTen").Signals[9]);

                //Configure Signal Parameters and Assign to PMX Device
                foreach (Signal signal in this._signals)
                {
                    //Set sampling rate (Hz)
                    signal.SampleRate = 2400;
                    this._pmx.AssignSignal(signal, out errors);
                }

                //Add signals to DAQ Measurement Session and Prepare the Session for Run           
                //Create new DAQMeasurement session.
                this._session = new DaqMeasurement();
                //Add signals to the DAQ Session
                this._session.AddSignals(this._pmx, this._signals);
                //Prepare DAQ Session for Asynchronous Run
                this._session.PrepareDaq();
                EventLog.LogDebug("DAQ is configured and ready to run.");
            }
            catch (Exception ex)
            {
                EventLog.LogFatal(ex.Message);
                throw new Exception("Failed to configure PMX target.", ex);
            }
                      
           
        }

        public void Start(int startingRevolutionCount, List<ChannelData> activeChannels, DataQueue<List<StationData>> dataQueue)
        {
            try
            {
                //Start Implementation
                EventLog.LogDebug("Starting PMX Data Streaming...");

                //Create Post-Processor for Data
                this._processor = new PostProcessor(_stations.Count(), startingRevolutionCount, activeChannels, dataQueue);

                //Start PMX Streaming
                this._session.StartDaq(DataAcquisitionMode.Auto);
                          
                EventLog.LogTrace("PMX System Start Success.");
            }
            catch (Exception ex)
            {
                EventLog.LogFatal("Failed to start PMX device: " + ex.Message);
            }
        }

        public void Read(List<ChannelData> activeChannels)
        {
            try
            {
                //Read PMX Device
                //Adds all buffered values to Signals.ContinuousMeasurementValues
                this._session.FillMeasurementValues();

                //Process the data                   
                this._processor.Process(this._signals, activeChannels, 10000, false);

                Thread.Sleep(this._session.DataFetchingInterval);
            }
            catch(Exception ex)
            {
                EventLog.LogFatal("Error reading PMX device: " + ex.Message);
            }
            
        }

        public void Stop()
        {
                       
            try
            {
                //Stop Implementation
                EventLog.LogDebug("Requesting Stop PMX Data Streaming.");                
                this._session.StopDaq();
                this._session.RemoveSignals(this._pmx);
                this._session.Dispose();
                this._processor = null;
                this._signals.Clear();
                this._stations.Clear();
                EventLog.LogDebug("PMX Stop Request Succesful.");
            }
            catch (Exception e)
            {
                EventLog.LogFatal("Error stopping PMX Device. Message: " + e.Message);
            }            
            
        }

        public void Disconnect()
        {
            //Disconnect Implementation
            EventLog.LogDebug("Closing PMX Connections.");
            this._daq.Disconnect(this._pmx);            
            this._daq.Dispose();
        }
        #endregion
    }

}
