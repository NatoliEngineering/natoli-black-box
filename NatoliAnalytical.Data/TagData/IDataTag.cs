﻿using System;

namespace NatoliAnalytical.Data.TagData
{
    public interface IDataTag
    {

        event EventHandler<object> UserValueChanged;

        void SetValue(object value);

    }
}
