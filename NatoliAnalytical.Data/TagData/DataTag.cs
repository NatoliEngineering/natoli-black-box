﻿using System;
//DMC
using EventLog = Dmc.Logging.EventLog;
using Dmc.Wpf;

namespace NatoliAnalytical.Data.TagData
{
    public sealed class DataTag<T> : NotifyPropertyChanged, IDataTag
        where T : struct
    {

        private T _value;
        public T Value
        {
            get => this._value;
            set
            {
                if (this.SetProperty(ref this._value, value))
                    this.UserValueChanged?.Invoke(this, value);
            }

        }

        public event EventHandler<object> UserValueChanged;
        
        public void SetValue(object value)
        {
            if (value is T castValue)
            {
                this._value = castValue;
                this.FirePropertyChanged(nameof(this.Value));
            }
            else if (value is IConvertible)
            {
                this._value = (T)Convert.ChangeType(value, typeof(T));
                this.FirePropertyChanged(nameof(this.Value));
            }
            else
            {
                EventLog.LogFatal("Failed to cast value for DataTag. Please fix the code definitions.");
            }
        }
    }
}
