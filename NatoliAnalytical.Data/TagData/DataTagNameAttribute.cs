﻿using System;

namespace NatoliAnalytical.Data.TagData
{
    public sealed class DataTagNameAttribute : Attribute
    {

        public string Name { get; set; }

        public DataTagNameAttribute(string name)
        {
            this.Name = name;
        }

    }
}
