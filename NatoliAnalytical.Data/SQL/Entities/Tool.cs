﻿using NatoliAnalytical.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatoliAnalytical.Data.SQL.Entities
{
    public class Tool
    {
        public Tool()
        {

        }

        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public int ToolGeoTypeValue { get; set; }
        [NotMapped]
        public ToolGeoType ToolGeoType
        {
            get
            {
                return (ToolGeoType)this.ToolGeoTypeValue;
            }
            set
            {
                this.ToolGeoTypeValue = (int)value;
            }
        }
        public int CoatingTypeValue { get; set; }
        [NotMapped]
        public ToolCoatingType CoatingType
        {
            get
            {
                return (ToolCoatingType)this.CoatingTypeValue;
            }
            set
            {
                this.CoatingTypeValue = (int)value;
            }
        }
        public int MetalTypeValue { get; set; }
        [NotMapped]
        public ToolMetalType MetalType
        {
            get
            {
                return (ToolMetalType)this.MetalTypeValue;
            }
            set
            {
                this.MetalTypeValue = (int)value;
            }
        }
        public int ToolClassValue { get; set; }
        [NotMapped]
        public ToolClass ToolClassType
        {
            get { return (ToolClass)this.ToolClassValue; }
            set { this.ToolClassValue = (int)value; }
        }
        public double MaxTipForce { get; set; }
        public double XAxis { get; set; }
        public double YAxis { get; set; }
        public double ZAxis { get; set; }
        public double Perimeter { get; set; }
        public double CupVolume { get; set; }
        public double CrossSectionalArea { get; set; }
        public double SurfaceArea { get; set; }
        public double HeadCurveRadius { get; set; }
        public double HeadFlatDiameter { get; set; }
        public string HOBDrawingNumber { get; set; }


        public int TipCount { get; set; }
        public virtual User User { get; set; }
        public DateTime TimeStamp { get; set; }
        public bool IsActive { get; set; }

        public virtual ICollection<Study> Studies { get; set; }
    }
    
    
}
