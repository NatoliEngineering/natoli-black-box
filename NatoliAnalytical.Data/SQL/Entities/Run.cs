﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace NatoliAnalytical.Data.SQL.Entities
{
    public class Run
    {
        public Run()
        {

        }

        [Key]
        public int Id { get; set; }

        public int RunNumber { get; set; }
        public int? StudyId { get; set; }
        public virtual Study Study { get; set; }

        public int? UserId { get; set; }
        public virtual User User { get; set; }

        public DateTime Timestamp { get; set; }

        public bool IsActive { get; set; }

        public virtual ICollection<Sample> Samples { get; set; }
        public virtual ICollection<Tablet> Tablets { get; set; }
    }
}
