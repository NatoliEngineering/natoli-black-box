﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using NatoliAnalytical.Data.Enums;

namespace NatoliAnalytical.Data.SQL.Entities
{
    public class Die
    {
        public Die()
        {

        }

        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public int ToolGeoTypeValue { get; set; }
        [NotMapped]
        public ToolGeoType ToolGeoType
        {
            get
            {
                return (ToolGeoType)this.ToolGeoTypeValue;
            }
            set
            {
                this.ToolGeoTypeValue = (int)value;
            }
        }
        public int CoatingTypeValue { get; set; }
        [NotMapped]
        public ToolCoatingType CoatingType
        {
            get
            {
                return (ToolCoatingType)this.CoatingTypeValue;
            }
            set
            {
                this.CoatingTypeValue = (int)value;
            }
        }
        public int MetalTypeValue { get; set; }
        [NotMapped]
        public ToolMetalType MetalType
        {
            get
            {
                return (ToolMetalType)this.MetalTypeValue;
            }
            set
            {
                this.MetalTypeValue = (int)value;
            }
        }

        public int TipCount { get; set; }
        public double TopXAxis { get; set; }
        public double TopYAxis { get; set; }
        public double BottomXAxis { get; set; }
        public double BottomYAxis { get; set; }
        public double ZAxis { get; set; }
        public double TaperXValue { get; set; }
        public double TaperYValue { get; set; }
        public bool IsDeepFill { get; set; }
        public string HOBDrawingNumber { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public DateTime TimeStamp { get; set; }
        public bool IsActive { get; set; }

    }
}
