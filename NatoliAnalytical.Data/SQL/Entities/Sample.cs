﻿using NatoliAnalytical.Base.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatoliAnalytical.Data.SQL.Entities

{
    public class Sample
    {
        public Sample()
        {
            //Support EF Core Lazy load.
        }

        [Key]
        public int Id { get; set; }
        public DateTime SampleDate { get; set; }

        public int Station { get; set; }
        public int Revolution { get; set; }
        public double Dosing { get; set; }
        public double MaxPreCompForce { get; set; }
        public double PreCompPressure { get; set; }
        public double MaxMainCompForce { get; set; }
        public double MainCompPressure { get; set; }
        public double MaxEjectForce { get; set; }
        public double EjectPressure { get; set; }
        public double MaxTabletTakeOffForce { get; set; }
        public double PreThickness { get; set; }
        public double MainThickness { get; set; }
        public double TurretRPM { get; set; }
        public double TurretVelocity { get; set; }
        public double PreCompDwellTime { get; set; }
        public double MainCompDwellTime { get; set; }

        public int TabletCount { get; set; }

        public int RunId { get; set; }
        public virtual Run Run { get; set; }
        
        public virtual User User { get; set; }
        
    }
}
