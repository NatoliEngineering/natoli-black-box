﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatoliAnalytical.Data.SQL.Entities
{
    public class Formula
    {
        public Formula()
        {
            //Support EF Core Lazy load.
        }

        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public double TrueDensity { get; set; }
        public double BulkDensity { get; set; }
        public double LubricantConcentration { get; set; }
        public double HausnerRatio { get; set; }
        public double CompressibilityIndex { get; set; }
        public double Flowability { get; set; }
        public virtual User User { get; set; }
        public DateTime TimeStamp { get; set; }
        public virtual ICollection<Study> Studies { get; set; }
        public bool IsActive { get; set; }

    }
}
