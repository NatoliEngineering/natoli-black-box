﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatoliAnalytical.Data.SQL.Entities
{
    public class Project
    {

        public Project()
        {
            //Support EF Core Lazy Load.
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public string DocumentRef { get; set; }
        public string Objective { get; set; }
        public DateTime TimeStamp { get; set; }

        public int? UserId { get; set; }
        public virtual User User { get; set; }

        public virtual ICollection<Study> Studies { get; set; }
        public bool IsActive { get; set; }

    }
}
