﻿using NatoliAnalytical.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatoliAnalytical.Data.SQL.Entities
{
    public class User
    {
        public User()
        {

        }

        [Key]
        public int Id { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public byte[] Hash { get; set; }
        [Required]
        public byte[] Salt { get; set; }

        public int UserLevelValue { get; set; }
        [NotMapped]
        public UserLevelType UserLevelType
        {
            get
            {
                return (UserLevelType)this.UserLevelValue;
            }
            set
            {
                this.UserLevelValue = (int)value;
            }
        }

        public bool IsActive { get; set; }
        public DateTime ModifiedOn { get; set; }

        public virtual ICollection<Study> Studies { get; set; }
    }

}
