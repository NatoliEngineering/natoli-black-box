﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NatoliAnalytical.Data.SQL.Entities
{
    public class StudyAverage
    {
        public StudyAverage()
        {

        }

        [Key]
        public int Id { get; set; }

        public double AvgDosing { get; set; }
        public double AvgMaxPreCompForce { get; set; }
        public double AvgPreCompPressure { get; set; }
        public double AvgMaxMainCompForce { get; set; }
        public double AvgMainCompPressure { get; set; }
        public double AvgMaxEjectForce { get; set; }
        public double AvgEjectPressure { get; set; }
        public double AvgMaxTabletTakeOffForce { get; set; }
        public double AvgPreThickness { get; set; }
        public double AvgMainThickness { get; set; }
        public double AvgTurretRPM { get; set; }
        public double AvgTurretVelocity { get; set; }
        public double AvgPreCompDwellTime { get; set; }
        public double AvgMainCompDwellTime { get; set; }

        public double AvgWeight { get; set; }
        public double AvgThickness { get; set; }
        public double AvgDensity { get; set; }
        public double AvgBreakingForce { get; set; }

        public double AvgDissolution { get; set; }
        public double AvgDisintegration { get; set; }
        public double AvgTensileStrength { get; set; }
        public double AvgSolidFraction { get; set; }
        public double AvgFriability { get; set; }

        public DateTime LastModified { get; set; }

        public int StudyId { get; set; }
        public virtual Study Study { get; set; }
    }
}
