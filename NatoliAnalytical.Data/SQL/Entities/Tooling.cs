﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NatoliAnalytical.Data.SQL.Entities;
using System.ComponentModel.DataAnnotations;

namespace NatoliAnalytical.Data.SQL.Entities
{
    public class Tooling
    {
        public Tooling()
        {

        }

        [Key]
        public int ToolingId { get; set; }

        public int? UpperToolId { get; set; }
        public virtual Tool UpperTool { get; set; }

        public int? LowerToolId { get; set; }
        public virtual Tool LowerTool { get; set; }

        public int? DieId { get; set; }
        public virtual Die Die { get; set; }

    }
}
