﻿using NatoliAnalytical.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatoliAnalytical.Data.SQL.Entities
{
    public class Study
    {
        public Study()
        {

        }

        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public int StudyTypeValue { get; set; }
        [NotMapped]
        public StudyType StudyType
        {
            get
            {
                return (StudyType)this.StudyTypeValue;
            }
            set
            {
                this.StudyTypeValue = (int)value;
            }
        }
        public double AmbientTemp { get; set; }
        public string Objective { get; set; }
        public string Description { get; set; }

        public DateTime TimeStamp { get; set; }

        public int? FormulaId { get; set; }
        public virtual Formula Formula { get; set; }

        public int LayerCount { get; set; }

        public int? ToolingId { get; set; }
        public Tooling Tooling { get; set; }

        public int? ProjectId { get; set; }
        public virtual Project Project { get; set; }

        public int? UserId { get; set; }
        public virtual User User { get; set; }
        public bool IsActive { get; set; }

        public virtual ICollection<Sample> Samples { get; set; }
        public virtual ICollection<Tablet> Tablets { get; set; }
        public virtual ICollection<Run> Runs { get; set; }

    }
}
