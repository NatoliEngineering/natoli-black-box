﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace NatoliAnalytical.Data.SQL.Entities
{
    public class ChangeLog
    {
        public ChangeLog()
        {

        }

        [Key]
        public int Id { get; set; }
        [Required]
        public int StudyId { get; set; }
        public string StudyName { get; set; }
        public int RunId { get; set; }
        public int SampleId { get; set; }
        public int TabletNumber { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public string Change { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}
