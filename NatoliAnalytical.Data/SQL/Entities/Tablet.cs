﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatoliAnalytical.Data.SQL.Entities
{
    public class Tablet
    {
        public Tablet()
        {

        }

        [Key]
        public int Id { get; set; }
        public int TabletNumber { get; set; }

        public double Weight { get; set; }
        public double Thickness { get; set; }
        public double Density { get; set; }
        public double BreakingForce { get; set; }

        public double Dissolution { get; set; }
        public double Disintegration { get; set; }
        public double TensileStrength { get; set; }
        public double SolidFraction { get; set; }
        public double Friability { get; set; }

        //public int StudyId { get; set; }
        public int SampleId { get; set; }
        public DateTime TimeStamp { get; set; }
        public bool CanEdit { get; set; }

        public virtual User User { get; set; }
        //public virtual Study Study { get; set; }

        public int RunId { get; set; }
        public virtual Run Run { get; set; }
    }
}
