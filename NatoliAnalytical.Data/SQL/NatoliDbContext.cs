﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NatoliAnalytical.Data.SQL.Entities;

namespace NatoliAnalytical.Data.SQL
{
    public class NatoliDbContext : DbContext
    {
        public virtual DbSet<ChangeLog> ChangeLogs { get; set; }
        public virtual DbSet<Project> Projects { get; set; }
        public virtual DbSet<Study> Studies { get; set; }
        public virtual DbSet<Run> Runs { get; set; }
        public virtual DbSet<RunAverage> RunAverages { get; set; }
        public virtual DbSet<Formula> Formulas { get; set; }
        public virtual DbSet<Tool> Tools { get; set; }
        public virtual DbSet<Die> Dies { get; set; }
        public virtual DbSet<Tablet> Tablets { get; set; }
        public virtual DbSet<Sample> Samples { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Tooling> Tooling { get; set; }
        public virtual DbSet<StudyAverage> StudyAverages { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(ConfigurationManager.ConnectionStrings["NatoliAnalytical_BlackBox"].ConnectionString);
            //optionsBuilder.UseLazyLoadingProxies();
        }      

    }

    
}
