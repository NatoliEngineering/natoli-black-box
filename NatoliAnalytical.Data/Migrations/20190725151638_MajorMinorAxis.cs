﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NatoliAnalytical.Data.Migrations
{
    public partial class MajorMinorAxis : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "MajorAxis",
                table: "Tools",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "MinorAxis",
                table: "Tools",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MajorAxis",
                table: "Tools");

            migrationBuilder.DropColumn(
                name: "MinorAxis",
                table: "Tools");
        }
    }
}
