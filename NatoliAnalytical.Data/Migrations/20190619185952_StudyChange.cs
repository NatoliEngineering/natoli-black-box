﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NatoliAnalytical.Data.Migrations
{
    public partial class StudyChange : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Formulas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    TrueDensity = table.Column<double>(nullable: false),
                    BulkDensity = table.Column<double>(nullable: false),
                    LubricantConcentration = table.Column<double>(nullable: false),
                    HausnerRatio = table.Column<double>(nullable: false),
                    CompressibilityIndex = table.Column<double>(nullable: false),
                    Flowability = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Formulas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Projects",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    DocumentRef = table.Column<string>(nullable: true),
                    Objective = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projects", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Tools",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    ToolGeoTypeValue = table.Column<int>(nullable: false),
                    CoatingTypeValue = table.Column<int>(nullable: false),
                    MetalTypeValue = table.Column<int>(nullable: false),
                    MaxTipForce = table.Column<double>(nullable: false),
                    PunchTipDiameter = table.Column<double>(nullable: false),
                    CupDepth = table.Column<double>(nullable: false),
                    CupVolume = table.Column<double>(nullable: false),
                    CrossSectionalArea = table.Column<double>(nullable: false),
                    HeadCurveRadius = table.Column<double>(nullable: false),
                    HeadFlatDiameter = table.Column<double>(nullable: false),
                    HOBDrawingNumber = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tools", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Studies",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    StudyTypeValue = table.Column<int>(nullable: false),
                    AmbientTemp = table.Column<double>(nullable: false),
                    Objective = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    FormulaId = table.Column<int>(nullable: false),
                    ToolId = table.Column<int>(nullable: false),
                    ProjectId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Studies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Studies_Formulas_FormulaId",
                        column: x => x.FormulaId,
                        principalTable: "Formulas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Studies_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Studies_Tools_ToolId",
                        column: x => x.ToolId,
                        principalTable: "Tools",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Studies_FormulaId",
                table: "Studies",
                column: "FormulaId");

            migrationBuilder.CreateIndex(
                name: "IX_Studies_ProjectId",
                table: "Studies",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Studies_ToolId",
                table: "Studies",
                column: "ToolId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Studies");

            migrationBuilder.DropTable(
                name: "Formulas");

            migrationBuilder.DropTable(
                name: "Projects");

            migrationBuilder.DropTable(
                name: "Tools");
        }
    }
}
