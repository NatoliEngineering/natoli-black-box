﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NatoliAnalytical.Data.Migrations
{
    public partial class NullableFKs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Studies_Formulas_FormulaId",
                table: "Studies");

            migrationBuilder.DropForeignKey(
                name: "FK_Studies_Projects_ProjectId",
                table: "Studies");

            migrationBuilder.DropForeignKey(
                name: "FK_Studies_Tools_ToolId",
                table: "Studies");

            migrationBuilder.AlterColumn<int>(
                name: "ToolId",
                table: "Studies",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ProjectId",
                table: "Studies",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "FormulaId",
                table: "Studies",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Studies_Formulas_FormulaId",
                table: "Studies",
                column: "FormulaId",
                principalTable: "Formulas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Studies_Projects_ProjectId",
                table: "Studies",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Studies_Tools_ToolId",
                table: "Studies",
                column: "ToolId",
                principalTable: "Tools",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Studies_Formulas_FormulaId",
                table: "Studies");

            migrationBuilder.DropForeignKey(
                name: "FK_Studies_Projects_ProjectId",
                table: "Studies");

            migrationBuilder.DropForeignKey(
                name: "FK_Studies_Tools_ToolId",
                table: "Studies");

            migrationBuilder.AlterColumn<int>(
                name: "ToolId",
                table: "Studies",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ProjectId",
                table: "Studies",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "FormulaId",
                table: "Studies",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Studies_Formulas_FormulaId",
                table: "Studies",
                column: "FormulaId",
                principalTable: "Formulas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Studies_Projects_ProjectId",
                table: "Studies",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Studies_Tools_ToolId",
                table: "Studies",
                column: "ToolId",
                principalTable: "Tools",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
