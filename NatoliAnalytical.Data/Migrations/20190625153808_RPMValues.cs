﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NatoliAnalytical.Data.Migrations
{
    public partial class RPMValues : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "FeederRPM",
                table: "Samples",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "TurretRPM",
                table: "Samples",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FeederRPM",
                table: "Samples");

            migrationBuilder.DropColumn(
                name: "TurretRPM",
                table: "Samples");
        }
    }
}
