﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatoliAnalytical.Data.Enums
{
    public enum ToolCoatingType
    {
        Hard_Chrome_Plating = 1,
        Natoli_Ultracoat = 2,
        Chromium_Nitride = 3,
        Titanium_Nitride = 4,
        Diamond_Like_Carbon = 5,
        Nickel_Fluoropolymer = 6,
        Teflon = 7,
        Plasma_Nitriding = 8,
        Boron_Carbide = 9,
        Diamond_Like_Carbon_4041 = 10,
        None = 999        
    }
}
