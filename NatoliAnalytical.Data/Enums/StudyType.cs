﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatoliAnalytical.Data.Enums
{
    public enum StudyType
    {
        Scope = 1,
        Strain = 2,
        Compaction = 3,
        Scalability = 4,
        Trial = 5
    }
}
