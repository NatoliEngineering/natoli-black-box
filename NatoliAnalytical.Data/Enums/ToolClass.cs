﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatoliAnalytical.Data.Enums
{
    public enum ToolClass
    {
        TSM_D_Dome = 1,
        TSM_B_Dome = 2,
        TSM_B = 3,
        EU_B = 4,
        TSM_D = 5,
        EU_D = 6
    }
}
