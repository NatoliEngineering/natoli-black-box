﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatoliAnalytical.Data.Enums
{
    public enum ToolGeoType
    {
        Round = 1,
        Capsule = 2,
        ModifiedCapsule = 3,
        Oval = 4,
        ModifiedOval = 12,
        Bullet = 5,
        Triangle = 6,
        ArcTriangle = 7,
        Square = 8,
        Pillow = 9,
        Rectangle = 10,
        ModifiedRectangle = 11,
        Other = 99
    }
}
