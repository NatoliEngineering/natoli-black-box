﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatoliAnalytical.Data.Enums
{
    public enum ToolMetalType
    {
        Standard_408 = 1,
        Standard_S1 = 2,
        Standard_S7 = 3,
        Premium_D2 = 4,
        Premium_DC53 = 5,
        Premium_K340 = 6,
        Premium_D3 = 7,
        Premium_440C = 8,
        Premium_M340 = 9,
        Premium_A2 = 10,
        Premium_M2 = 11,
        PowderedMetal_PM15V = 12,
        PowderedMetal_PM9V = 13,
        PowderedMetal_PM3V = 14,
        PowderedMetal_PM10V = 15,
        PowderedMetal_PMM2 = 16,
        PowderedMetal_PMM4 = 17,
        PowderedMetal_PMS90V = 18,
        PowderedMetal_Hastelloy = 19,
        Specialty_CarbideDies= 20,
        Specialty_CarbideTip = 21,
        Specialty_CeramicDies = 22
    }
}
