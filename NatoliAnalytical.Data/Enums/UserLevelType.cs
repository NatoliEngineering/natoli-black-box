﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatoliAnalytical.Data.Enums
{
    public enum UserLevelType
    {
        NatoliSuperUser = 1,
        Administrator = 2,
        PressMaintenance = 3,
        PressOperator = 4
    }
}
