﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Windows.Controls;

namespace NatoliBlackBox.Validation
{
    class NullStringRule : ValidationRule
    {
        #region Class Properties
        //TBD
        #endregion

        public NullStringRule()
        {
            //Contructor
        }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            string test = "";

            try
            {
                if (!String.IsNullOrEmpty((string)value))
                    test = (string)value;
                else
                    return new ValidationResult(false, "Must enter valid name or description data.");
            }
            catch (Exception e)
            {
                return new ValidationResult(false, "Data error invalid type in form field.  " + e);
            }

            return ValidationResult.ValidResult;
        }
    }
}
