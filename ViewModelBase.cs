﻿using Dmc;
using Dmc.Wpf;
using NatoliBlackBox.Services;

namespace NatoliBlackBox
{
    public abstract class ViewModelBase : NotifyPropertyChanged
    {
        private SessionService _session;
        public SessionService Session
        {
            get => this._session;
            private set => this.SetProperty(ref this._session, value);
        }

        private UiObjectService _uiservice;
        public UiObjectService UiService
        {
            get => this._uiservice;
            private set => this.SetProperty(ref this._uiservice, value);
        }

        protected ViewModelBase(SessionService session, UiObjectService uiService)
        {
            this.Session = session;
            this.UiService = uiService;
        }

        protected internal virtual bool Load(object configuration = null)
        {
            return true;
        }

        protected internal virtual void Unload(object configuration = null) { }
    }
}
