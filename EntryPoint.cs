﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace NatoliBlackBox
{
    public class EntryPoint
    {
        public static void Main()
        {
            var actionThread = new Thread(() => (new App()).Run());
            actionThread.SetApartmentState(ApartmentState.STA);
            actionThread.Start();
            actionThread.Join();
        }
    }
}
